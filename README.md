# IronLibs

Ironpipp's useful utilities, for both NodeJs and Browsers

## How to build and test
     > npm install
     > npm npm run-script build
     > npm npm run-script test

## To build uWebSockets for ARM 32 bit

    git clone https://github.com/uNetworking/uWebSockets.js.git --recursive
    cd uWebSockets.js
    nano build.c
    make
    

## TODOS
add minimized sources:

    uglifyjs --compress --mangle --output node_modules\ironlibs\build\libs\bus.js -- node_modules\ironlibs\build\libs\bus.js
    