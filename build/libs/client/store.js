"use strict";
var window;
var isNode = window == null;
//import server-client SHARED modules
if (!isNode)
    window["require"] = function () { return { "IronLibsCommon": window["IronLibsCommon"] }; }; //module import hack
var IronLibs = require("../common");
var __ = IronLibs.IronLibsCommon;
/**
 * CommonLib library dealing with local/session storage
 * (runs in browser)
 */
var IronLibsStore;
(function (IronLibsStore) {
    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function get(store, key) {
        var val = store[key];
        if (val === undefined)
            return undefined;
        return JSON.parse(val);
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     * @param value an object that will be stored SERIALIZED
     */
    function set(store, key, value) {
        if (value === undefined)
            remove(store, key);
        else
            store[key] = JSON.stringify(value);
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function remove(store, key) {
        store.removeItem(key);
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     */
    function getAllKeys(store, prefix) {
        var ret = [];
        __.ObjKeysToArray(store).forEach(function (key) {
            if (key.indexOf(prefix) == 0)
                ret.push(key.substr(prefix.length));
        });
        return ret;
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     */
    function clear(store, prefix) {
        __.ObjKeysToArray(store).forEach(function (key) {
            if (key.indexOf(prefix) == 0)
                store.removeItem(key);
        });
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     * @param fn
     */
    function each(store, prefix, fn) {
        if (!__.IsFunction(fn))
            fn = function () { };
        __.ObjKeysToArray(store).forEach(function (key) {
            if (key.indexOf(prefix) == 0)
                fn(key.substr(prefix.length), store[key]);
        });
    }
    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function isPresent(store, key) {
        return get(store, key) !== undefined;
    }
    var Local;
    (function (Local) {
        var prefix = "App";
        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        function GetPrefix() {
            return prefix;
        }
        Local.GetPrefix = GetPrefix;
        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        function SetPrefix(Prefix) {
            prefix = __.EnsureString(Prefix);
        }
        Local.SetPrefix = SetPrefix;
        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        function Get(key) {
            return get(localStorage, prefix + __.EnsureString(key));
        }
        Local.Get = Get;
        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        function Set(key, value) {
            set(localStorage, prefix + __.EnsureString(key), value);
        }
        Local.Set = Set;
        function Remove(key) {
            remove(localStorage, prefix + __.EnsureString(key));
        }
        Local.Remove = Remove;
        function GetAllKeys() {
            return getAllKeys(localStorage, prefix);
        }
        Local.GetAllKeys = GetAllKeys;
        function Clear() {
            clear(localStorage, prefix);
        }
        Local.Clear = Clear;
        function Each(fn) {
            each(localStorage, prefix, fn);
        }
        Local.Each = Each;
        function IsPresent(key) {
            return isPresent(localStorage, prefix + __.EnsureString(key));
        }
        Local.IsPresent = IsPresent;
    })(Local = IronLibsStore.Local || (IronLibsStore.Local = {}));
    var Session;
    (function (Session) {
        var prefix = "App";
        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        function GetPrefix() {
            return prefix;
        }
        Session.GetPrefix = GetPrefix;
        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        function SetPrefix(Prefix) {
            prefix = __.EnsureString(Prefix);
        }
        Session.SetPrefix = SetPrefix;
        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        function Get(key) {
            return get(sessionStorage, prefix + __.EnsureString(key));
        }
        Session.Get = Get;
        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        function Set(key, value) {
            set(sessionStorage, prefix + __.EnsureString(key), value);
        }
        Session.Set = Set;
        function Remove(key) {
            remove(sessionStorage, prefix + __.EnsureString(key));
        }
        Session.Remove = Remove;
        function GetAllKeys() {
            return getAllKeys(sessionStorage, prefix);
        }
        Session.GetAllKeys = GetAllKeys;
        function Clear() {
            clear(sessionStorage, prefix);
        }
        Session.Clear = Clear;
        function Each(fn) {
            each(sessionStorage, prefix, fn);
        }
        Session.Each = Each;
        function IsPresent(key) {
            return isPresent(sessionStorage, prefix + __.EnsureString(key));
        }
        Session.IsPresent = IsPresent;
    })(Session = IronLibsStore.Session || (IronLibsStore.Session = {}));
})(IronLibsStore || (IronLibsStore = {}));
if (!isNode)
    IronLibs.IronLibsCommon.Store = IronLibsStore;
module.exports = IronLibsStore;
