"use strict";
var window;
var isNode = window == null;
//import server-client SHARED modules
if (!isNode)
    window["require"] = function () { return { "IronLibsCommon": window["IronLibsCommon"] }; }; //module import hack
var IronLibs = require("../common");
var __ = IronLibs.IronLibsCommon;
/**
 * CommonLib library dealing with DOM and HTML
 * (runs in browser, requires JQuery library)
 */
var IronLibsUi;
(function (IronLibsUi) {
    /*##########################################################################
     ######################   GENERAL UI RELATED METHODS   #####################
     ##########################################################################*/
    /**
     * Returns a string with escaped HTML characters
     *
     * @method EscapeHtml
     * @param html {string} The plain string to escape
     * @return {string} The string with HTML ESCAPES
     */
    function EscapeHtml(html) {
        return $("<span></span>").text(html).html();
    }
    IronLibsUi.EscapeHtml = EscapeHtml;
    /**
     * Returns a string with unescaped HTML characters
     *
     * @method UnescapeHtml
     * @param html {string} A string with HTML ESCAPES
     * @return {string} The decoded string
     */
    function UnescapeHtml(html) {
        return $("<span></span>").html(html).text();
    }
    IronLibsUi.UnescapeHtml = UnescapeHtml;
    /**
     * If the tag's value is NOT EMPTY and is NOT an URL then prepends urlPrefix (defaults to "http://") to it
     *
     * @method EnsureInputValueIsUrl
     * @param $inputTag {JQuery} a JQuery wrapped INPUT TAG
     * @param [urlPrefix] {string} the string to prepend to the input value if it's not an url
     */
    function EnsureInputValueIsUrl($inputTag, urlPrefix) {
        if (urlPrefix === void 0) { urlPrefix = "http://"; }
        var s = $inputTag.val().trim();
        if (!__.StringIsUrl(s)) {
            $inputTag.val(urlPrefix + s);
        }
    }
    IronLibsUi.EnsureInputValueIsUrl = EnsureInputValueIsUrl;
    /**
     * Returns TRUE if the DOM document is actually exposed to the user
     * (the browser tab is active)
     *
     * @method IsDocumentVisible
     * @return boolean
     */
    function IsDocumentVisible() {
        var hidden = document.visibilityState == "hidden" ||
            document["webkitVisibilityState"] == "hidden" ||
            document["webkitVisibilityState"] == "unloaded" ||
            document["webkitVisibilityState"] == "not visible";
        return !hidden;
    }
    IronLibsUi.IsDocumentVisible = IsDocumentVisible;
    /**
     * If the current browser is Internet Explorer, returns its version, NULL otherwise
     *
     * @method InternetExplorerVersion
     * @return number | NULL
     */
    function InternetExplorerVersion() {
        if (__.IsNull(navigator) || __.IsEmptyString(navigator.userAgent))
            return null;
        var ua = navigator.userAgent;
        if (ua.indexOf("Trident/7") >= 0) //special case for IE11
            return 11.0;
        var pos = ua.indexOf("MSIE ");
        if (pos < 0)
            return null;
        //it's IE: parse its version
        ua = ua.substr(pos + 5);
        pos = ua.indexOf(";");
        ua = ua.substr(0, pos);
        var ver = parseFloat(ua);
        if (isNaN(ver))
            return null;
        return ver;
    }
    IronLibsUi.InternetExplorerVersion = InternetExplorerVersion;
    /**
     * Checks if the page has been restored after a browser restart WITHOUT re-executing the request
     * to the server. This is useful to be sure that old scripts are ever executed!
     * WARNING: Requires an HIDDEN INPUT tag with id="requestId" with a random value at each rendering to work
     *
     * @method IsPageFresh
     * @return {boolean} TRUE if the page is fresh or the check can't be executed, FALSE if the page is proven to be OLD.
     */
    function IsPageFresh() {
        if (window["SpaManager"] != undefined)
            return true; //for now SPA doesn't support this
        try {
            //without using Modernizr (NOT included in PUBLIC area) check if we have localStorage support
            try {
                __.Store.Local.Set('modernizr', 'modernizr');
                __.Store.Local.Remove('modernizr');
            }
            catch (e) {
                return true;
            }
            var MAX_REQUESTS_HISTORY = 100;
            if (__.Store.Local.Get("requests") == undefined)
                __.Store.Local.Set("requests", []);
            var currRequestId = $("#requestId").val();
            var requests = __.Store.Local.Get("requests");
            //search the current ID
            for (var i = 0; i < requests.length; i++) {
                if (requests[i] == currRequestId) {
                    //The page has already been loaded with this ID, so it's OLD
                    return false;
                }
            }
            //the page is NEW, so remember its ID
            if (requests.length == MAX_REQUESTS_HISTORY)
                requests.splice(0, 1); //remove the oldest request
            requests.push(currRequestId);
            __.Store.Local.Set("requests", requests);
            return true;
        }
        catch (e) {
            return true;
        }
    }
    IronLibsUi.IsPageFresh = IsPageFresh;
    /**
     * Returns a structure describing the browsing device.
     * The detection is performed by the CSS ENGINE, so reflects the MEDIA QUERIES rules of the different devices types
     *
     * @return {IsDesktop : boolean; IsTablet : boolean; IsPhone : boolean; IsHorizontal : boolean; IsVertical : boolean}
     * @method GetBrowsingDeviceType
     */
    function GetBrowsingDeviceType() {
        var $bt = $("#BrowserType");
        var type = $bt.css("background-color").toLowerCase();
        var orientation = $bt.css("color").toLowerCase();
        return {
            IsDesktop: type.indexOf("#0000ff") >= 0 || type.indexOf("rgb(0, 0, 255)") >= 0,
            IsTablet: type.indexOf("#00ff00") >= 0 || type.indexOf("rgb(0, 255, 0)") >= 0,
            IsPhone: type.indexOf("#ff0000") >= 0 || type.indexOf("rgb(255, 0, 0)") >= 0,
            IsHorizontal: orientation.indexOf("#0000ff") >= 0 || orientation.indexOf("rgb(0, 0, 255)") >= 0,
            IsVertical: orientation.indexOf("#00ff00") >= 0 || orientation.indexOf("rgb(0, 255, 0)") >= 0
        };
    }
    IronLibsUi.GetBrowsingDeviceType = GetBrowsingDeviceType;
    /**
     * Returns the wrapped Document object of the IFRAME found under the specified selector
     *
     * @method GetIframeInnerDocument
     */
    function GetIframeInnerDocument($iframe) {
        if (__.IsNull($iframe) || $iframe.length == 0)
            return null;
        var cw = ($iframe[0]).contentWindow;
        if (__.IsNotNull(cw))
            return $(cw.document);
        //Internet Explorer case
        var id = $iframe.attr("id");
        if (__.IsEmptyString(id)) {
            id = __.Guid.New();
            $iframe.attr("id", id);
        }
        return $(document.frames[id].document);
    }
    IronLibsUi.GetIframeInnerDocument = GetIframeInnerDocument;
    /**
     * Chrome browser on MOBILE devices (at the moment) when user over-scrolls up the page
     * a RELOAD PAGE button appears and activates when user releases the touch,
     * actually reloading the page.
     * This functions checks the BODY scroll situation and prevents this.
     *
     * @method PreventPullToRefresh
     */
    function PreventPullToRefresh() {
        var lastScrollPosition = null;
        var $body = $("#content").find(" > div"); //the main page scrolling element
        $body.bind("touchstart", function (e) {
            if (e.originalEvent.changedTouches.length == 0)
                return;
            lastScrollPosition = e.originalEvent.changedTouches[0].clientY;
        });
        $body.bind("touchmove", function (e) {
            if (e.originalEvent.changedTouches.length == 0)
                return;
            var t = e.originalEvent.changedTouches[0];
            if ($body[0].scrollTop == 0) //if there's nothing more to scroll
             {
                if (t.clientY > lastScrollPosition) //...and user is scrolling UP
                 {
                    e.preventDefault(); //THIS prevents the PullToRefresh browser feature
                }
                else {
                    lastScrollPosition = t.clientY;
                }
            }
        });
    }
    IronLibsUi.PreventPullToRefresh = PreventPullToRefresh;
    function CheckBrowserVersion() {
        //CHECK the BROWSER VERSION
        var agent = navigator.userAgent.toUpperCase();
        if ((agent
            .indexOf("MSIE 7.") >=
            0 &&
            agent.indexOf("WINDOWS NT 5") < 0) || //IE 7 in NON WINDOWS XP (Compatibility mode?)
            ($.browser.msie == true && parseFloat($.browser.version) <= 8) || //IE <= 8
            ($.browser.mozilla == true && parseFloat($.browser.version) <= 4)) //FF <= 4
         {
            ShowOldBrowserMsg();
            return false;
        }
        return true;
    }
    IronLibsUi.CheckBrowserVersion = CheckBrowserVersion;
    function ShowOldBrowserMsg() {
        NotificateInfo($("#OldBrowserMsg").clone().show().wrap($d()).parent().html());
    }
    IronLibsUi.ShowOldBrowserMsg = ShowOldBrowserMsg;
    /**
     * Changes the TAG TYPE of the passed element keeping the attributes and data() values
     *
     * @method ChangeElementType
     * @param $el {JQuery} the elements to be changed
     * @param newType {string} the new tag name
     * @return the newly created elements
     */
    function ChangeElementType($el, newType) {
        var $ret = $();
        $el.each(function () {
            var el = this;
            var attrs = {};
            var data = $el.data();
            $.each(el.attributes, function (idx, attr) {
                attrs[attr.nodeName] = attr.nodeValue;
            });
            $(el).replaceWith(function () {
                var $newEl = $("<" + newType + "/>", attrs)
                    .append($el.contents())
                    .data(data);
                $ret = $ret.add($newEl);
                return $newEl;
            });
        });
        return $ret;
    }
    IronLibsUi.ChangeElementType = ChangeElementType;
    /**
     * Copy ALL the style COMPUTED properties of nodeSource to nodeDest
     *
     * @method CloneNodeStyle
     * @param nodeSource {HTMLElement} a DOM node (not jQuery object)
     * @param nodeDest  {HTMLElement} a DOM node (not jQuery object)
     */
    function CloneNodeStyle(nodeSource, nodeDest) {
        var sourceStyle = nodeSource.style;
        var jQuerySource = $(nodeSource);
        for (var s in sourceStyle) {
            if (typeof sourceStyle[s] != "string" || s == "cssText")
                continue;
            //get the COMPUTED value and SET IT
            nodeDest.style[s] = jQuerySource.css(s);
        }
    }
    IronLibsUi.CloneNodeStyle = CloneNodeStyle;
    /*##########################################################################
     #############################   RENDERERS   ###############################
     ##########################################################################*/
    /**
     * Generates pretty HTML rendering a boolean value (using Bootstrap badge)
     *
     * @method RenderBool
     * @param {boolean} boolValue
     * @return {string} the rendered HTML string
     */
    function RenderBool(boolValue) {
        if (!!boolValue)
            return '<span class="badge BoolYes">' + App.Res.Yes + '</span>';
        else
            return '<span class="badge BoolNo">' + App.Res.No + '</span>';
    }
    IronLibsUi.RenderBool = RenderBool;
    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-ONLY formatted string
     *
     * @method RenderDate
     * @return {string} the rendered HTML string
     */
    function RenderDate(date) {
        var dateObj;
        try {
            if (__.IsDate(date))
                dateObj = date;
            else if (__.Dates.IsISOFormatStringDate(date))
                dateObj = __.Dates.GetDateFromISO(date);
            else if (__.Dates.IsMsFormatStringDate(date))
                dateObj = __.Dates.GetDateFromMsFormat(date);
        }
        catch (e) {
        }
        if (!__.IsDate(dateObj))
            return "";
        return __.Dates.FormatDate(dateObj, true, true, true, false, false, false, false);
    }
    IronLibsUi.RenderDate = RenderDate;
    /**
     * Can accept a Date object or a (serialized) string in ISO or MS format,
     * Returns DATE-AND-TIME formatted string
     *
     * @method RenderDateTime
     * @return {string} the rendered HTML string
     */
    function RenderDateTime(date) {
        var dateObj;
        try {
            if (__.IsDate(date))
                dateObj = date;
            else if (__.Dates.IsISOFormatStringDate(date))
                dateObj = __.Dates.GetDateFromISO(date);
            else if (__.Dates.IsMsFormatStringDate(date))
                dateObj = __.Dates.GetDateFromMsFormat(date);
        }
        catch (e) {
        }
        if (!__.IsDate(dateObj))
            return "";
        return __.Dates.FormatDate(dateObj, true, true, true, true, true, true, false);
    }
    IronLibsUi.RenderDateTime = RenderDateTime;
    /**
     * Renders a Bootstrap toggle initialized with the passed value
     *
     * @method RenderBoolSwitcher
     * @return {string} the rendered HTML string
     */
    function RenderBoolSwitcher(boolValue, inputName, inputId) {
        if (inputName === void 0) { inputName = ""; }
        if (inputId === void 0) { inputId = ""; }
        /*
         return '<span class="onoffswitch">' +
         '<input type="checkbox" class="onoffswitch-checkbox"' +
         (__.IsNotEmptyString(inputName) ? (' name="' + inputName + '"') : '' ) +
         (__.IsNotEmptyString(inputId) ? (' id="' + inputId + '"') : '' ) +
         ((!!boolValue) ? ' checked="checked"' : '' ) + '>' +
         '<label class="onoffswitch-label">' +
         '<span class="onoffswitch-inner" data-swchon-text="' + App.Res.Yes + '"' +
         ' data-swchoff-text="' + App.Res.No + '"></span>' +
         '<span class="onoffswitch-switch"></span></label></span>';
         *
         */
        /*        return '<input type="checkbox" ' +
         (__.IsNotEmptyString(inputName) ? (' name="' + inputName + '"') : '' ) +
         (__.IsNotEmptyString(inputId) ? (' id="' + inputId + '"') : '' ) +
         ((!!boolValue) ? ' checked="checked"' : '' ) + '>';
         */
        return "<div class=\"smart-form\">\n                        <label class=\"toggle\">\n                            <input type=\"checkbox\" " +
            ((!!boolValue) ? " checked=\"checked\"" : "") +
            (__.IsNotEmptyString(inputName) ? (" name=\"" + inputName + "\"") : "") +
            (__.IsNotEmptyString(inputId) ? (" id=\"" + inputId + "\"") : "") +
            ">\n                            <i data-swchoff-text=\"" +
            App.Res.No +
            "\" data-swchon-text=\"" +
            App.Res.Yes +
            "\"></i>\n                         </label>\n                     </div>";
    }
    IronLibsUi.RenderBoolSwitcher = RenderBoolSwitcher;
    /**
     * Renders a spinner image tag
     *
     * @method RenderSpinner
     * @return {string} the rendered HTML string
     */
    function RenderSpinner() {
        return '<img alt="' + App.Res.Loading + '" src="' + IronLibsUi.DefaultSpinnerImageUrl + '" />';
    }
    IronLibsUi.RenderSpinner = RenderSpinner;
    /**
     * If the passed element is contained in a JqueryUI opened dialog, closes it
     *
     * @method CloseContainingPopup
     * @param $el {JQuery} the element to be checked
     */
    function CloseContainingPopup($el) {
        var obj = $($el).parents(".ui-dialog-content:eq(0)").data("uiDialog");
        if (__.IsNotNull(obj) && __.IsFunction(obj.close))
            obj.close();
    }
    IronLibsUi.CloseContainingPopup = CloseContainingPopup;
    function $_addTheRest(node, options) {
        if (typeof options == "string")
            node.addClass(options);
        else if (__.IsArray(options))
            node.addClass(options.join(" "));
        else if (typeof options == "object")
            node.attr(options);
    }
    /**
     * Fast helper function which returns a new SPAN element
     *
     * @method $s
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the span
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $s(content, options1, options2) {
        var ret = $("<span></span>");
        if (typeof content == "string")
            ret.text(content);
        else if (__.IsArray(content))
            for (var c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$s = $s;
    if (!isNode)
        window["$s"] = $s; //shortcut
    /**
     * Fast helper function which returns a new DIV element
     *
     * @method $d
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $d(content, options1, options2) {
        var ret = $("<div></div>");
        if (typeof content == "string")
            ret.html(content);
        else if (__.IsArray(content))
            for (var c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$d = $d;
    if (!isNode)
        window["$d"] = $d; //shortcut
    /**
     * Fast helper function which returns a new IMG element
     *
     * @method $i
     * @param src The attribute SRC of the image
     * @param alt The attribute ALT of the image
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $i(src, alt, options1, options2) {
        if (src === void 0) { src = ""; }
        if (alt === void 0) { alt = ""; }
        var ret = $("<img src='' alt='' />").attr({ "src": src, "alt": alt });
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$i = $i;
    if (!isNode)
        window["$i"] = $i; //shortcut
    /**
     * Fast helper function which returns a new A element
     *
     * @method $a
     * @param href The attribute HREF of the A tag
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the div
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $a(href, content, options1, options2) {
        if (href === void 0) { href = "javascript:void(0);"; }
        var ret = $("<a></a>").attr("href", href);
        if (typeof content == "string")
            ret.html(content);
        else if (__.IsArray(content))
            for (var c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$a = $a;
    if (!isNode)
        window["$a"] = $a; //shortcut
    /**
     * Fast helper function which returns a new SELECT element
     *
     * @method $sel
     * @param values An array of STRING or SelectListItem
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $sel(values, options1, options2) {
        var ret = $("<select></select>");
        if (__.IsArray(values)) {
            for (var i = 0; i < values.length; i++) {
                var el = values[i];
                var castedEl = values[i];
                if (typeof el == "string") {
                    ret.append($opt(el, el));
                }
                else if (typeof el == "object" &&
                    __.IsNotNull(castedEl.Text) &&
                    __.IsNotNull(castedEl.Value)) {
                    ret.append($opt(castedEl.Value, castedEl.Text, castedEl.Selected == true));
                }
            }
        }
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$sel = $sel;
    if (!isNode)
        window["$sel"] = $sel; //shortcut
    /**
     * Returns an <OPTION> element
     *
     * @method $opt
     */
    function $opt(value, text, selected, disabled) {
        if (value === void 0) { value = ""; }
        if (text === void 0) { text = ""; }
        if (selected === void 0) { selected = false; }
        if (disabled === void 0) { disabled = false; }
        var $ret = $("<option></option>").attr("value", __.EnsureString(value)).text(__.EnsureString(text));
        if (selected)
            $ret.attr("selected", "selected");
        if (disabled)
            $ret.attr("disabled", "disabled");
        return $ret;
    }
    IronLibsUi.$opt = $opt;
    if (!isNode)
        window["$opt"] = $opt; //shortcut
    /**
     * Returns a list of <OPTION> elements for an ARRAY of data
     *
     * @method $opts
     */
    function $opts(items, propForValue, propForText, propForSelected, propForDisabled) {
        if (propForValue === void 0) { propForValue = "Value"; }
        if (propForText === void 0) { propForText = "Text"; }
        if (propForSelected === void 0) { propForSelected = "Selected"; }
        if (propForDisabled === void 0) { propForDisabled = "Disabled"; }
        var $ret = $();
        if (__.IsArray(items))
            items.forEach(function (item) {
                var v = __.IsFunction(propForValue) ? propForValue(item) : __.EnsureString(__.GetObjProperty(item, propForValue));
                var t = __.IsFunction(propForText) ? propForText(item) : __.EnsureString(__.GetObjProperty(item, propForText));
                var s = __.IsFunction(propForSelected) ? propForSelected(item) : !!__.EnsureString(__.GetObjProperty(item, propForSelected));
                var d = __.IsFunction(propForDisabled) ? propForDisabled(item) : !!__.EnsureString(__.GetObjProperty(item, propForDisabled));
                $ret = $ret.add($opt(v, t, s, d));
            });
        return $ret;
    }
    IronLibsUi.$opts = $opts;
    if (!isNode)
        window["$opts"] = $opts; //shortcut
    /**
     * Fast helper function which returns a new TD element
     *
     * @method $td
     * @param [content] Can be STRING or NODE or an ARRAY of nodes: will be the content of the TD node
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $td(content, options1, options2) {
        var ret = $("<td></td>");
        if (typeof content == "string")
            ret.html(content);
        else if (__.IsArray(content))
            for (var c = 0; c < content.length; c++)
                ret.append(content[c]);
        else if (__.IsNotNull(content))
            ret.append(content);
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$td = $td;
    if (!isNode)
        window["$td"] = $td; //shortcut
    /**
     * Fast helper function which returns a new TR element
     *
     * @method $tr
     * @param [values] A simple array of object(STRING or NODE or an ARRAY of nodes) representing the content
     *                 of the respective TD subnodes to be inserted.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $tr(values, options1, options2) {
        var ret = $("<tr></tr>");
        if (__.IsArray(values)) {
            for (var i = 0; i < values.length; i++)
                ret.append($td(values[i]));
        }
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$tr = $tr;
    if (!isNode)
        window["$tr"] = $tr; //shortcut
    /**
     * Fast helper function which returns a new TABLE element
     *
     * @method $t
     * @param [content] If an array of array of object(STRING or NODE or an ARRAY of nodes) inserts all TR and TD subnodes.
     *                  If a simple array inserts only empty TR subnodes.
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $t(content, options1, options2) {
        var tb = $("<tbody></tbody>");
        var ret = $("<table></table>").append(tb);
        if (__.IsArray(content)) {
            for (var c = 0; c < content.length; c++)
                tb.append($tr(content[c]));
        }
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$t = $t;
    if (!isNode)
        window["$t"] = $t; //shortcut
    /**
     * Fast helper function which returns a new STYLE element
     * Useful to COMPUTE the CSS and add/update it in the page dynamically
     *
     * @method $style
     * @param [css] The text containing the CSS rules
     * @param [options1] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @param [options2] If is a STRING it's a CSS class name; If is an ARRAY it's a list of CSS class names; If is an OBJECT it's map of attributes
     * @return {jQuery}
     */
    function $style(css, options1, options2) {
        var ret = $("<style></style>").html(css);
        $_addTheRest(ret, options1);
        $_addTheRest(ret, options2);
        return ret;
    }
    IronLibsUi.$style = $style;
    if (!isNode)
        window["$style"] = $style; //shortcut
    /**
     * Forces the browser to load (and cache!) the passed image.
     * Callbacks are called upon success or error.
     *
     * @param {string} url
     * @param {(width: string, height: string, time: number, img: HTMLImageElement) => void} callback
     * @param {(time: number, img: HTMLImageElement) => void} callbackError
     * @param {boolean} disableCache
     * @method PreLoadImageWithClbk
     */
    function PreLoadImageWithClbk(url, callback, callbackError, disableCache) {
        if (disableCache) {
            url += (url.indexOf("?") < 0) ? "?" : "&";
            url += "fresh=" + (Math.random() * 10000000 | 0);
        }
        var start = new Date().getTime();
        var $i = __.UI.$i(url);
        var $d = __.UI.$d().appendTo("body").css({
            "position": "absolute",
            "left": "-100000px",
            "overflow": "hidden",
            "visibility": "hidden"
        });
        $i.appendTo($d).bind("load", function () {
            var w = $i.css("width");
            var h = $i.css("height");
            var end = (new Date().getTime() - start);
            $d.remove();
            if (typeof callback == "function")
                callback(w, h, end, $i.detach()[0]);
        })
            .bind("error", function (e) {
            var end = (new Date().getTime() - start);
            if (typeof callbackError == "function")
                callbackError(end, $i.detach()[0]);
        });
        //IE issue FIX (normally if image is already downloaded the callback won't be called!)
        if ($i[0].readyState == "complete") //.readyState === 4 ??
            callback($i.css("width"), $i.css("height"), (new Date().getTime() - start), $i.detach()[0]);
    }
    IronLibsUi.PreLoadImageWithClbk = PreLoadImageWithClbk;
    /**
     * Executes the passed JS script
     *
     * @param script {string}
     * @param $appendScriptTagTo {JQuery}
     * @method ExecuteJsScript
     */
    function ExecuteJsScript(script, $appendScriptTagTo) {
        if ($appendScriptTagTo === void 0) { $appendScriptTagTo = $("head"); }
        //create a NEW tag
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = false;
        try {
            // doesn't work on ie...
            s.appendChild(document.createTextNode(script));
        }
        catch (e) {
            // IE has funky script nodes
            s.text = script;
        }
        $appendScriptTagTo[0].appendChild(s);
    }
    IronLibsUi.ExecuteJsScript = ExecuteJsScript;
    /**
     * Loads from network end EXECUTES in page a SCRIPT loading the specified url
     * When loading and execution are done, calls the CALLBACK if specified
     *
     * @method LoadJsLink
     * @param src Can be an URL or a NOT already inserted HTMLScriptElement tag
     * @param async
     * @param callback
     * @param appendTagTo
     * @return {JQuery} The created and inserted tag
     */
    function LoadJsLink(src, async, callback, appendTagTo) {
        var url = src.src || src;
        if (async) {
            $.ajax({
                url: url,
                type: "get",
                dataType: "script",
                data: null,
                cache: true,
                success: callback
            });
        }
        else {
            //F.P. WARNING: the order (append tag, onload, set src) IS IMPORTANT for the callback to be called!
            var s_1 = (document.createElement('script'));
            var $s_1 = $(s_1);
            s_1.type = 'text/' + (src.type || 'javascript');
            s_1.async = false;
            // use body if available. more safe in IE
            if (__.IsJquery(appendTagTo) && appendTagTo.length > 0)
                appendTagTo[0].appendChild(s_1);
            else
                (document.body || document.head).appendChild(s_1);
            if (__.IsFunction(callback)) {
                s_1.onreadystatechange = s_1.onload = function () {
                    var state = s_1.readyState;
                    if ($s_1.data("OnLoadScriptDone") != true && (!state || /loaded|complete/.test(state))) {
                        $s_1.data("OnLoadScriptDone", true); //remember it!
                        callback($s_1);
                    }
                };
            }
            s_1.src = url;
        }
    }
    IronLibsUi.LoadJsLink = LoadJsLink;
    /**
     * Loads end APPLIES in page a LINK (css) tag loading the specified url
     * When loading and apply are done, calls the CALLBACK if specified
     *
     * @method LoadCssLink
     * @param src Can be an URL or a NOT already inserted HTMLLinkElement tag
     * @param callback
     * @return {JQuery} The created and inserted tag
     */
    function LoadCssLink(src, callback) {
        var link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = src.href || src;
        document.getElementsByTagName('head')[0].appendChild(link);
        //This is the TRICK!!!!
        var img = document.createElement('img');
        img.onerror = function () {
            if (__.IsFunction(callback))
                callback(link);
        };
        img.src = link.href;
        return $(link);
    }
    IronLibsUi.LoadCssLink = LoadCssLink;
    /**
     * Binds a callback to be called when the CSS of the specified LINK tag(s) has been loaded
     * WARNING: linkTags MUST be already inserted in page!
     *
     * @method BindOnLoadCssLink
     * @param linkTags
     * @param callback
     */
    function BindOnLoadCssLink(linkTags, callback) {
        if (__.IsJquery(linkTags) && __.IsFunction(callback)) {
            linkTags.each(function () {
                var $el = $(this);
                //This is the TRICK!!!!
                var img = document.createElement('img');
                img.onerror = function () {
                    callback($el);
                };
                img.src = $el.attr("href");
            });
        }
    }
    IronLibsUi.BindOnLoadCssLink = BindOnLoadCssLink;
    /**
     * Given a TEMPLATE string and a MODEL executes the binding and returns the result string
     * Actually uses Kendo Templates syntax
     *
     * @method RenderTemplate
     * @param {JQuery | string} template The template string or a TAG holding it
     * @param model The object holding the properties to be used for the binding
     * @return {string} The result of the template execution
     */
    function RenderTemplate(template, model) {
        var templateText = __.IsString(template) ? template : template.html();
        var templateOriFun = window["kendo"].template;
        var t = templateOriFun(templateText);
        return t(model);
    }
    IronLibsUi.RenderTemplate = RenderTemplate;
    /**
     * Replace every tag content marked with "data-localize" (encode to html) or "data-localize-html" (don't encode) attributes
     * with the App.Res localized string, or with the KEY if not found.
     *
     * @param $where {JQuery} The container to limit the search for tags to be localized
     * @method ReplaceLocalizedTagsContent
     */
    function ReplaceLocalizedTagsContent($where) {
        $($where).find("[data-localize]").each(function () {
            var $tag = $(this);
            var key = $tag.attr("data-localize");
            if (__.IsEmptyString(key)) //no key provided -> print empty string
                $tag.text("");
            else if (__.IsString(App.Res[key])) //key present in App.Res -> print its localized value (also empty strings)
                $tag.text(App.Res[key]);
            else //key NOT present in App.Res -> print the key
                $tag.text("[[" + key + "]]");
        });
        $($where).find("[data-localize-html]").each(function () {
            var $tag = $(this);
            var key = $tag.attr("data-localize-html");
            if (__.IsEmptyString(key)) //no key provided -> print empty string
                $tag.html("");
            else if (__.IsString(App.Res[key])) //key present in App.Res -> print its localized value (also empty strings)
                $tag.html(App.Res[key]);
            else //key NOT present in App.Res -> print the key
                $tag.text("[[" + key + "]]");
        });
    }
    IronLibsUi.ReplaceLocalizedTagsContent = ReplaceLocalizedTagsContent;
    /**
     * Notifies the user with a non modal alert message
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @param asError {boolean} If TRUE renders as a RED message
     * @method Notify
     */
    function Notify(message, subMessage, asError) {
        if (asError === void 0) { asError = false; }
        if (__.IsEmptyString(message) || window["GLB_UnloadingPage"] === true)
            return; //Probably User aborted this connection changing the page... don't show the error!
        var messageText = "";
        if (__.IsString(message))
            messageText = /*'<div class="MessageText">' + */ message /*+ '</div>'*/;
        else if (typeof message === "object") {
            messageText = message.MessageText;
            if (message.PropertyName !== "") {
                var $valMsg = $("#" + message.PropertyName + "_validationMessage");
                if ($valMsg.length > 0)
                    $valMsg.text(messageText).show();
            }
            if (__.IsEmptyString(messageText) && __.IsNotEmptyString(message.PropertyName))
                messageText = App.Res.AnErrorForProperty + " '" + message.PropertyName + "' ";
        }
        if (__.IsNotEmptyString(subMessage))
            messageText += '<div class="SubMessage">' + subMessage + '</div>';
        if (messageText === "")
            return;
        /*
         if (asError)
         window["NotificateErrors"](messageText);
         else
         window["NotificateInfo"](messageText);

         $.smallBox({
         title : "",
         content : messageText,
         color : asError ? "#FF7777" : "#A6B6FF",
         timeout : 5000,
         icon : "fa fa-exclamation-circle swing animated"
         });

        $.notify(
            {
                title   : messageText,
                message : "",
                icon    : "k-icon k-i-note"
            },
            {
                type : asError ? 'danger' : 'success'
            });
        */
        $.notify(messageText, asError ? 'error' : 'success');
    }
    IronLibsUi.Notify = Notify;
    /**
     *  Wraps Notify() with SUCCESS type
     *
     * @param message  {string | {MessageText : string, PropertyName : string}} ads
     * @param subMessage {string}
     * @method NotificateInfo
     */
    function NotificateInfo(message, subMessage) {
        Notify(message, subMessage, false);
    }
    IronLibsUi.NotificateInfo = NotificateInfo;
    /**
     *  Wraps Notify() with ERROR type
     *
     * @param message {string | {MessageText : string, PropertyName : string}}
     * @param subMessage {string}
     * @method NotificateErrors
     */
    function NotificateErrors(message, subMessage) {
        Notify(message, subMessage, true);
    }
    IronLibsUi.NotificateErrors = NotificateErrors;
    function CloseAllNotifications() {
        $("#divSmallBoxes *").unbind();
        $("#divSmallBoxes").empty();
    }
    IronLibsUi.CloseAllNotifications = CloseAllNotifications;
    // /**
    //  *    Listen for error thrown by javascript
    //  *
    //  *    options.SilentMode    : if TRUE user won't be NOTIFIED (with __.UI.NotificateErrors), if FALSE he will
    //  *    options.ShowFullErrorToUser : if FALSE then App.Res.JsErrorOccurred is shown. Default to __.DebugMode()
    //  *    options.SuppressError : if TRUE the error won't appear in browser console
    //  *    options.InformServer  : if TRUE a call to server is performed to log the error
    //  *
    //  * @method StartToListenForErrors
    //  */
    // export function StartToListenForErrors(options : {
    //     SilentMode? : boolean,
    //     ShowFullErrorToUser? : boolean;
    //     SuppressError? : boolean,
    //     InformServer? : boolean,
    //     FilterSilent? : (msg : string, href : string) => boolean,
    //     FilterSuppressError? : (msg : string, href : string) => boolean,
    // }) : void
    // {
    //     //MERGE OPTIONS
    //     let settings = $.extend({
    //         "SilentMode"          : false,
    //         "ShowFullErrorToUser" : __.IsDebugMode(),
    //         "SuppressError"       : false,
    //         "InformServer"        : true,
    //         "FilterSilent"        : function(args)
    //         {
    //             return settings.SilentMode;
    //         },
    //         "FilterSuppressError" : function(args)
    //         {
    //             return settings.SuppressError;
    //         }
    //     }, options || {});
    //
    //
    //     //BIND THE EVENT
    //     window.onerror = <any>function(msg : string,
    //                                    href,
    //                                    lineRow,
    //                                    lineCol,
    //                                    exception /*present in CHROME*/)
    //     {
    //         //EXCLUSIONS
    //         try
    //         {
    //             if (msg.toUpperCase().indexOf("ATTEMPT TO RUN COMPILE-AND-GO SCRIPT ON A CLEARED SCOPE") >= 0 ||
    //                 msg.toUpperCase().indexOf("NS_ERROR_NOT_AVAILABLE") >= 0)
    //                 return false;
    //         }
    //         catch (e)
    //         {/*no throw...otherwise loop! */
    //         }
    //
    //
    //         //ADD OTHER INFOS to be logged
    //         let trace : any = null;
    //         if ($.browser.msie)
    //         {
    //             try
    //             {
    //                 trace = printStackTrace({});
    //             }
    //             catch (e)
    //             {
    //             }
    //         }
    //
    //         let silent = false;
    //         try
    //         {
    //             silent = settings.FilterSilent(arguments);
    //             if (trace !== null && typeof trace === "object" && trace.length > 0)
    //             {
    //                 msg += "<br>Stack trace found: <br><br> - " + trace.join('<br> - ');
    //             }
    //             msg += "<br><br>Silent mode (kept hidden to user): " + silent;
    //             msg += "<br>Current URL: " + location.href;
    //
    //             if (__.IsNotNull(exception) && __.IsString(exception.stack))
    //                 msg += "<br>Stack trace found: <br><br> - " +
    //                     exception.stack
    //                     .replace(/ at /gi, "<br> - at ") //this if for Chrome
    //                     .replace(/@http/gi, "<br> - @http"); //this is for FireFox
    //
    //             //INFORM THE SERVER
    //             if (settings.InformServer)
    //             {
    //                 let errDescription = {
    //                     "Msg"            : msg,
    //                     "CurrentUrl"     : location.href,
    //                     "PositionInCode" : lineRow + ":" + lineCol
    //                 };
    //                 __.Net.Ajax({
    //                     url   : "/Home/LogJsError/",
    //                     data  : JSON.stringify(errDescription),
    //                     error : $.noop
    //                 });
    //             }
    //         }
    //         catch (e)
    //         {/*no throw...otherwise loop! */
    //         }
    //
    //
    //         //NOTIFICATE THE USER
    //         if (!silent)
    //         {
    //             if (settings.ShowFullErrorToUser)
    //                 NotificateErrors(App.Res.Error, msg);
    //             else
    //                 NotificateErrors(App.Res.Error);
    //         }
    //
    //         //suppress the error in console?
    //         return settings.FilterSuppressError(arguments);
    //     };
    // }
    /**
     * When user tries to change page (hitting BACK button or a link or closing the browser)
     * it's possible to PROMPT him with a message (message is NOT displayed in Firefox ver >= 4)
     *
     * @method PreventWindowExitWithMessage
     * @param msg the textual message to show NATIVELY to user
     * @param {function} [boolFunction] If passed a function to control the prevention:
     *        if returns FALSE user can exit normally, otherwise he will be prompted!
     */
    function PreventWindowExitWithMessage(msg, boolFunction) {
        window.onbeforeunload = function (e) {
            if (__.IsFunction(boolFunction) && boolFunction() == false)
                return;
            e = e || window.event;
            // For IE and Firefox ver. < 4
            if (e) {
                e.returnValue = msg;
            }
            // For Safari
            return msg;
        };
    }
    IronLibsUi.PreventWindowExitWithMessage = PreventWindowExitWithMessage;
    /**
     * Permits to exit the window without any PROMPT.
     * (Cancels registration performed with PreventWindowExitWithMessage() )
     *
     * @method AllowWindowExit
     */
    function AllowWindowExit() {
        window.onbeforeunload = null;
    }
    IronLibsUi.AllowWindowExit = AllowWindowExit;
    /**
     * Detects the current REAL visible document size
     *
     * @return {{w : number, h : number}}
     * @method GetBrowserSize
     */
    function GetBrowserSize() {
        if (window.innerHeight || window.innerWidth) {
            return { w: window.innerWidth, h: window.innerHeight };
        }
        return {
            w: document.documentElement.clientWidth,
            h: document.documentElement.clientHeight
        };
    }
    IronLibsUi.GetBrowserSize = GetBrowserSize;
    /**
     * IF What is a JQuery object returns the encoded HTML of the JQuery element itself (not only its content)
     * IF What is a string then HTML-encodes it
     *
     * @param what what to encode
     * @return {string} the encoded string
     * @method ToHtml
     */
    function ToHtml(what) {
        if (__.IsString(what))
            return $d().text(what).html();
        else
            return $d().append(what.clone()).html();
    }
    IronLibsUi.ToHtml = ToHtml;
    /**
     * The default Spinner image url
     *
     * @static
     * @property {string} DefaultSpinnerImageUrl
     */
    IronLibsUi.DefaultSpinnerImageUrl = "/Images/spinner.gif";
    /**
     * The default Spinner SMALL image url
     *
     * @static
     * @property {string} DefaultSmallSpinnerImageUrl
     */
    IronLibsUi.DefaultSmallSpinnerImageUrl = "/Images/spinner-small.gif";
    /**
     * The default settings for page block effect
     *
     * @static
     * @property DefaultPageBlockingStyle
     */
    IronLibsUi.DefaultPageBlockingStyle = {
        message: '<img alt="LOADING" src="' +
            IronLibsUi.DefaultSpinnerImageUrl +
            '" class="Spinner"><br><p>' +
            (__.IsNull(window["App.Res"]) ? "LOADING" : App.Res.Loading) +
            '</p>',
        css: { border: "none", backgroundColor: "transparent", color: "#888888" },
        overlayCSS: {
            backgroundColor: '#FFF',
            opacity: 0.5
        }
    };
    /**
     * The default settings for element block effect
     *
     * @static
     * @property DefaultElementBlockingStyle
     */
    IronLibsUi.DefaultElementBlockingStyle = {
        message: '<img alt="LOADING" src="' + IronLibsUi.DefaultSmallSpinnerImageUrl + '" class="Spinner"><br>',
        css: { border: "none", backgroundColor: "transparent", color: "#888888", width: "100%" },
        overlayCSS: {
            backgroundColor: '#FFF',
            opacity: 0.5
        }
    };
    /**
     * Helper returning an HREF address doing nothing (useful for <a> tags)
     *
     * @method EmptyHref
     * @return {string}
     */
    function EmptyHref() {
        return "javascript:void(0);";
    }
    IronLibsUi.EmptyHref = EmptyHref;
    /**
     * Modally LOCKS the entire page preventing any user interaction
     * @param options Eventual object to override default options
     * @method BlockPage
     */
    function BlockPage(options) {
        var opt = __.UI.DefaultPageBlockingStyle;
        if (__.IsNotNullObject(options))
            opt = __.MergeObj(opt, options);
        $.blockUI(opt);
    }
    IronLibsUi.BlockPage = BlockPage;
    /**
     * Unlocks the page (cancels the previous BlockPage() call)
     *
     * @method UnblockPage
     */
    function UnblockPage() {
        $.unblockUI();
    }
    IronLibsUi.UnblockPage = UnblockPage;
    /**
     * The currently locked (with BlockElement()) elements
     *
     * @static
     * @property BlockedElements
     */
    IronLibsUi.BlockedElements = [];
    /**
     * Modally LOCKS the passed elements preventing any user interaction
     *
     * @param $els {JQuery} The elements to lock
     * @param options Eventual object to override default options
     * @method BlockElement
     */
    function BlockElement($els, options) {
        if (!__.IsArray(__.UI.BlockedElements))
            __.UI.BlockedElements = [];
        var opt = __.UI.DefaultElementBlockingStyle;
        if (__.IsNotNullObject(options))
            opt = __.MergeObj(opt, options);
        $els.each(function () {
            $(this).block(opt);
            __.UI.BlockedElements.push(this);
        });
    }
    IronLibsUi.BlockElement = BlockElement;
    /**
     * Unlocks the passed elements (cancels the previous BlockElement() call)
     *
     * @param $els {JQuery} The elements to unlock
     * @method UnblockElement
     */
    function UnblockElement($els) {
        $els.each(function () {
            $(this).unblock();
            var i = $.inArray(this, __.UI.BlockedElements);
            if (i > -1) {
                __.UI.BlockedElements.splice(i, 1);
            }
        });
    }
    IronLibsUi.UnblockElement = UnblockElement;
    /**
     * Unlocks all previously locked elements (cancels the previous BlockElement() call)
     *
     * @method UnblockAllElements
     */
    function UnblockAllElements() {
        while (__.UI.BlockedElements.length > 0) {
            __.UI.UnblockElement($(__.UI.BlockedElements[0]));
        }
    }
    IronLibsUi.UnblockAllElements = UnblockAllElements;
    /**
     * Returns TRUE if the pressed key represents a NUMBER (or movement), FALSE otherwise
     *
     * @param e th Event object passed to registered callback
     * @return boolean
     * @method UserPressedANumber
     */
    function UserPressedANumber(e) {
        var evt = __.IsNotNull(e) ? e : (window.event);
        var charCode = (evt.keyCode) ? evt.keyCode : evt.which;
        return ((charCode >= 48 && charCode <= 57) ||
            (charCode >= 96 && charCode <= 105) ||
            charCode == 107 || charCode == 109 || //+ and -
            charCode == 8 || charCode == 46 || //backspace and CANC
            charCode == 37 || charCode == 38 || charCode == 39 || charCode == 40); //ARROWS
    }
    IronLibsUi.UserPressedANumber = UserPressedANumber;
    /**
     * CommonLib library dealing with MODAL windows
     * (runs in browser, requires JQuery library)
     *
     * @class __.UI.Modal
     */
    var Modal;
    (function (Modal) {
        /**
         * The HTML template for any modal
         *
         * @static
         * @property htmlTemplate
         */
        Modal.htmlTemplate = "\n                    <div class=\"modal fade\">\n                      <div class=\"modal-dialog\">\n                        <div class=\"modal-content\">\n                          <div class=\"modal-header\">\n                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                                <span aria-hidden=\"true\">&times;</span>\n                            </button>\n                            <h4 class=\"modal-title\"></h4>\n                          </div>\n                          <div class=\"modal-body\"></div>\n                          <div class=\"modal-footer\"></div>\n                        </div>\n                      </div>\n                    </div>\n                ";
        /**
         * Opens a modal popup. Supports various parameters
         * (In the callback user can call "e.stopImmediatePropagation();" to prevent modal closure)
         *
         * @param options {IShowDialogOptions}
         * @method ShowDialog
         */
        function ShowDialog(options) {
            //Merge options with DEFAULT VALUES
            var opt = __.MergeObj({
                showFooter: false,
                showHeader: true,
                closeOnEsc: false,
                closeOnOverlayClick: false,
                modalTemplate: Modal.htmlTemplate
            }, options);
            //create the wrapper
            var $main = $(opt.modalTemplate).appendTo("body");
            //add the CONTENT
            if (__.IsJquery(opt.content)) {
                $main.find(".modal-body").append(opt.content);
            }
            else {
                $main.find(".modal-body").html(__.EnsureString(opt.content, "&nbsp;"));
            }
            //add HEADER and FOOTER
            if (opt.showHeader == false)
                $main.find(".modal-header").remove();
            else {
                if (__.IsJquery(opt.titleContent))
                    $main.find(".modal-title").append(opt.titleContent);
                else
                    $main.find(".modal-title").html(__.EnsureString(opt.titleContent, "&nbsp;"));
            }
            if (opt.showFooter == false)
                $main.find(".modal-footer").remove();
            else {
                if (__.IsJquery(opt.footerContent))
                    $main.find(".modal-footer").append(opt.footerContent);
                else
                    $main.find(".modal-footer").html(__.EnsureString(opt.footerContent, "&nbsp;"));
            }
            if (__.IsNotEmptyString(opt.width)) {
                $main.find(".modal-dialog").css("width", opt.width);
            }
            if (__.IsNotEmptyString(opt.height)) {
                $main.find(".modal-body").css("height", opt.height);
            }
            if (__.IsFunction(opt.clbkOnClose))
                $main.data("on-close", opt.clbkOnClose);
            //behaviour
            if (opt.closeOnEsc)
                $main.attr("tabindex", "-1"); //this is needed for a bootstrap modal issue on ESC press
            $main.attr("data-keyboard", opt.closeOnEsc ? "true" : "false");
            if (!opt.closeOnOverlayClick)
                $main.attr("data-backdrop", "static");
            $main.modal();
            return $main;
        }
        Modal.ShowDialog = ShowDialog;
        function ShowConfirm() {
            var opt;
            if (__.IsString(arguments[0]) || __.IsJquery(arguments[0])) {
                //FIRST function signature case
                opt = {
                    title: __.EnsureString(arguments[5], App.Res.ConfirmationRequest),
                    message: arguments[0],
                    buttons: [
                        {
                            text: __.EnsureString(arguments[4], App.Res.No),
                            closeOnClick: true,
                            htmlAttributes: { "class": "btn btn-w-m btn-outline btn-default" },
                            onClick: arguments[2]
                        }, {
                            text: __.EnsureString(arguments[3], App.Res.Yes),
                            closeOnClick: true,
                            htmlAttributes: { "class": "btn btn-w-m btn-primary" },
                            onClick: arguments[1]
                        }
                    ],
                    otherDialogOptions: arguments[6]
                };
            }
            else {
                //SECOND function signature case
                opt = __.MergeObj({
                    title: App.Res.ConfirmationRequest,
                    buttons: []
                }, arguments[0]);
            }
            var $buttons = $();
            opt.buttons.forEach(function (b) {
                var $b = $("<button>" + b.text + "</button>");
                if (__.IsNotNullObject(b.htmlAttributes))
                    $b.attr(b.htmlAttributes);
                //FIRST bind the passed function (which can ABORT event bubbling!!!)
                if (__.IsFunction(b.onClick))
                    $b.click(b.onClick);
                //The our CLOSE function
                if (b.closeOnClick)
                    $b.click(function (e) {
                        HideDialog();
                    });
                $buttons = $buttons.add($b);
            });
            var dialogOpts = {
                content: opt.message,
                showHeader: true,
                showFooter: true,
                titleContent: opt.title,
                footerContent: $buttons,
                closeOnEsc: true
            };
            //merge other eventual options
            if (__.IsNotNullObject(opt.otherDialogOptions))
                dialogOpts = __.MergeObj(dialogOpts, opt.otherDialogOptions);
            setTimeout(function () {
                $(".modal-footer .btn").last().focus();
            }, 510);
            return ShowDialog(dialogOpts);
        }
        Modal.ShowConfirm = ShowConfirm;
        function ShowPrompt() {
            var opt;
            var mainArgs = arguments;
            var obscured = !!arguments[8];
            if (__.IsString(arguments[0])) {
                //FIRST function signature case
                opt = {
                    title: __.EnsureString(arguments[5], App.Res.ConfirmationRequest),
                    message: __.EnsureString(arguments[0], ""),
                    buttons: []
                };
                if (__.IsNull(arguments[6]) || arguments[6]) {
                    opt.buttons.push({
                        text: __.EnsureString(arguments[4], App.Res.Cancel),
                        closeOnClick: true,
                        htmlAttributes: { "class": "btn btn-outline pull-left" },
                        onClick: arguments[2]
                    });
                }
                opt.buttons.push({
                    text: __.EnsureString(arguments[3], "Ok"),
                    closeOnClick: true,
                    htmlAttributes: { "class": "btn btn-primary BtnOk" },
                    onClick: function (e) {
                        var $b = $(this);
                        var $m = $b.parents(".modal-content:eq(0)");
                        var $i = $m.find(".TxtPrompt");
                        //VALIDATE
                        var val = $i.val();
                        if (__.IsEmptyString(val) && __.IsNull(mainArgs[7]) || mainArgs[7]) {
                            $m.toggleClass("has-error", true);
                            e.stopImmediatePropagation();
                            return;
                        }
                        //pass the value to callback
                        if (__.IsFunction(mainArgs[1]))
                            mainArgs[1](val, e);
                    }
                });
            }
            else {
                //SECOND function signature case
                opt = __.MergeObj({
                    title: App.Res.ConfirmationRequest,
                    buttons: []
                }, arguments[0]);
            }
            opt.message = "<h4>" + opt.message + "</h4><br>\n                           <input type=\"" + (obscured ? "password" : "text") + "\" class=\"TxtPrompt form-control\">\n                           <span></span>";
            var $buttons = $();
            opt.buttons.forEach(function (b) {
                var $b = $("<button><span>" + b.text + "</span></button>");
                $b.attr(b.htmlAttributes);
                //FIRST bind the passed function (which can ABORT event bubbling!!!)
                if (__.IsFunction(b.onClick))
                    $b.click(b.onClick);
                //The our CLOSE function
                if (b.closeOnClick)
                    $b.click(function (e) {
                        HideDialog();
                    });
                $buttons = $buttons.add($b);
            });
            ShowDialog({
                content: opt.message,
                showHeader: true,
                showFooter: true,
                titleContent: opt.title,
                footerContent: $buttons,
                closeOnEsc: true
            });
            setTimeout(function () {
                $(".modal .modal-content:eq(0) .TxtPrompt").focus().keydown(function (e) {
                    if (e.keyCode == 13) {
                        var $btn = $(this).parents(".modal-content:eq(0)").find(".BtnOk");
                        $btn.click();
                    }
                });
            }, 600);
        }
        Modal.ShowPrompt = ShowPrompt;
        /**
         * Hides a modal shown with the "ShowXXX" method
         *
         * @method HideDialog
         * @return {boolean} TRUE if the modal has been FOUND and HID, FALSE otherwise
         */
        function HideDialog() {
            var $dialog = $("body > .modal");
            if ($dialog.length > 0) {
                var plugin = $dialog.data('bs.modal');
                if (__.IsNotNull(plugin) && __.IsFunction(plugin.hide)) {
                    //gracefully HIDE the dialog with animation
                    plugin.hide();
                    return true;
                }
            }
            return false;
        }
        Modal.HideDialog = HideDialog;
        /**
         * Unbinds and destroys all dialogs, also if closed (they remain in the body)
         *
         * @method DestroyAllDialogs
         */
        function DestroyAllDialogs() {
            $("body > .modal").unbind();
            $("body > .modal, body > .modal-backdrop").unbind().remove();
        }
        Modal.DestroyAllDialogs = DestroyAllDialogs;
        if (__.IsFunction($.fn.modal))
            $(document).ready(function () {
                //IMPORTANT: Bootstrap modal plugins NEVER destroys its divs appended to the body.
                // To keep things clean we destroy them after the hiding transition
                __.WrapFun(function () {
                    var $dialog = this.$dialog.parents(".modal:eq(0)");
                    var onClose = $dialog.data('on-close');
                    if (__.IsFunction(onClose))
                        onClose($dialog);
                    //fix
                    var plugin = $dialog.data('bs.modal');
                    if (__.IsNull(plugin))
                        return;
                    plugin["resetAdjustments"]();
                    plugin["resetScrollbar"]();
                    $("body").removeClass("modal-open");
                    //DESTROY IT
                    var modalClass = $.fn.modal.constructor;
                    var duration = __.IsNumber(modalClass.TRANSITION_DURATION)
                        ? modalClass.TRANSITION_DURATION + 20
                        : 550;
                    setTimeout(__.UI.Modal.DestroyAllDialogs, duration);
                }, "hide", $.fn.modal.constructor.prototype, false);
            });
    })(Modal = IronLibsUi.Modal || (IronLibsUi.Modal = {}));
})(IronLibsUi || (IronLibsUi = {}));
if (!isNode)
    IronLibs.IronLibsCommon.UI = IronLibsUi;
module.exports = IronLibsUi;
