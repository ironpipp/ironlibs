"use strict";
/**
 * Created by Ironpipp on 12/12/2015.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var window;
var isNode = window == null;
//import server-client SHARED modules
if (!isNode)
    window["require"] = function () { return window["EventEmitter"]; }; //MODULES HACK
var EE3 = require('eventemitter3');
var EventObject = /** @class */ (function (_super) {
    __extends(EventObject, _super);
    function EventObject() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return EventObject;
}(EE3));
//this object is used to attach all GLOBAL events
var globalEvents = new EventObject();
//these dictionaries are meant to be used to dynamically retrieve (at runtime) the available class names
//which can emit or react to events
var registeredListenersTypes = {};
var registeredNotifiersTypes = {};
var EventsManager;
(function (EventsManager) {
    var PARAM_TYPE;
    (function (PARAM_TYPE) {
        PARAM_TYPE[PARAM_TYPE["STRING"] = 0] = "STRING";
        PARAM_TYPE[PARAM_TYPE["NUMBER"] = 1] = "NUMBER";
        PARAM_TYPE[PARAM_TYPE["DATE"] = 2] = "DATE";
        PARAM_TYPE[PARAM_TYPE["BOOLEAN"] = 3] = "BOOLEAN";
        PARAM_TYPE[PARAM_TYPE["OBJECT"] = 4] = "OBJECT";
    })(PARAM_TYPE = EventsManager.PARAM_TYPE || (EventsManager.PARAM_TYPE = {}));
    var EventListener = /** @class */ (function () {
        /**
         * A class which implements EventListener can REACT to some GLOBAL events
         *
         * @param listenerTypeName The name of the implementing class. It's meant to be used to dynamically
         * retrieve (at runtime) the available class names which can react to GLOBAL events.
         * WARNING: equal listenerTypeName should listen to the same ListenableEventsNames
         */
        function EventListener(listenerTypeName) {
            this.listenerTypeName = listenerTypeName;
            if (typeof listenerTypeName != "string")
                throw new Error("Need to pass a valid listenerTypeName");
            registeredListenersTypes[listenerTypeName] = this; //adds the entry to the dictionary
        }
        EventListener.prototype.ListenToEvent = function (eventName, clbk) {
            globalEvents.on(eventName, clbk);
        };
        EventListener.prototype.StopListeningToEvent = function (eventName, clbk) {
            globalEvents.removeListener(eventName, clbk);
        };
        return EventListener;
    }());
    EventsManager.EventListener = EventListener;
    var EventNotifier = /** @class */ (function () {
        /**
         * A class which implements EventNotifier can FIRE some GLOBAL events
         *
         * @param notifierTypeName The name of the implementing class. It's meant to be used to dynamically
         * retrieve (at runtime) the available class names which can fire GLOBAL events
         */
        function EventNotifier(notifierTypeName) {
            this.notifierTypeName = notifierTypeName;
            if (typeof notifierTypeName != "string")
                throw new Error("Need to pass a valid notifierTypeName");
            registeredNotifiersTypes[notifierTypeName] = this; //adds the entry to the dictionary
        }
        EventNotifier.prototype.NotifyEvent = function (eventName) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            if (arguments.length == 1)
                globalEvents.emit(eventName);
            else if (arguments.length == 2)
                globalEvents.emit(eventName, arguments[1]);
            else if (arguments.length == 3)
                globalEvents.emit(eventName, arguments[1], arguments[2]);
            else if (arguments.length == 4)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3]);
            else if (arguments.length == 5)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4]);
            else if (arguments.length == 6)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            else if (arguments.length == 7)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
            else if (arguments.length == 8)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]);
            else if (arguments.length == 9)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8]);
        };
        return EventNotifier;
    }());
    EventsManager.EventNotifier = EventNotifier;
    var EventListenerAndNotifier = /** @class */ (function () {
        function EventListenerAndNotifier(listenerAndNotifierTypeName) {
            if (typeof listenerAndNotifierTypeName != "string")
                throw new Error("Need to pass a valid listenerAndNotifierTypeName");
            if (registeredNotifiersTypes[listenerAndNotifierTypeName] == null)
                registeredNotifiersTypes[listenerAndNotifierTypeName] = this; //adds the entry to the dictionary
            if (registeredListenersTypes[listenerAndNotifierTypeName] == null)
                registeredListenersTypes[listenerAndNotifierTypeName] = this; //adds the entry to the dictionary
        }
        EventListenerAndNotifier.prototype.ListenToEvent = function (eventName, clbk) {
            globalEvents.on(eventName, clbk);
        };
        EventListenerAndNotifier.prototype.StopListeningToEvent = function (eventName, clbk) {
            globalEvents.removeListener(eventName, clbk);
        };
        EventListenerAndNotifier.prototype.NotifyEvent = function (eventName) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            if (arguments.length == 1)
                globalEvents.emit(eventName);
            else if (arguments.length == 2)
                globalEvents.emit(eventName, arguments[1]);
            else if (arguments.length == 3)
                globalEvents.emit(eventName, arguments[1], arguments[2]);
            else if (arguments.length == 4)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3]);
            else if (arguments.length == 5)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4]);
            else if (arguments.length == 6)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            else if (arguments.length == 7)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
            else if (arguments.length == 8)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]);
            else if (arguments.length == 9)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8]);
        };
        return EventListenerAndNotifier;
    }());
    EventsManager.EventListenerAndNotifier = EventListenerAndNotifier;
})(EventsManager || (EventsManager = {}));
module.exports = EventsManager;
