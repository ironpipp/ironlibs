"use strict";
var window;
var isNode = window == null;
//import server-client SHARED modules
if (!isNode)
    window["require"] = function () { return { "IronLibsCommon": window["IronLibsCommon"] }; }; //module import hack
var IronLibs = require("./common");
var __ = IronLibs.IronLibsCommon;
__.LoadPlugin(__.IronLibsPlugin.nodeJs);
if (!isNode)
    window["require"] = function () { return {}; /* no worries: uWs is never used in browser*/ }; //module import hack
var uWS = require("uWebSockets.js");
var IronLibsBus;
(function (IronLibsBus) {
    var mainBus = null;
    var ___privates = {};
    var BusRemoteMessageType;
    (function (BusRemoteMessageType) {
        BusRemoteMessageType[BusRemoteMessageType["System"] = 0] = "System";
        BusRemoteMessageType[BusRemoteMessageType["EventSent"] = 1] = "EventSent";
        BusRemoteMessageType[BusRemoteMessageType["AnswerToEvent"] = 2] = "AnswerToEvent";
    })(BusRemoteMessageType || (BusRemoteMessageType = {}));
    var BusRemoteMessage = /** @class */ (function () {
        function BusRemoteMessage(Type, Data, OnAnswer) {
            this.Type = Type;
            this.Data = Data;
            this.OnAnswer = OnAnswer;
            this.Id = __.Guid.NewShort();
        }
        return BusRemoteMessage;
    }());
    //TODO: To be protected: split private class form public exposable stuff
    var ServerReceivedConnection = /** @class */ (function () {
        function ServerReceivedConnection(_ws, _peerBusName, _localBus) {
            this._ws = _ws;
            this._peerBusName = _peerBusName;
            this._localBus = _localBus;
            this._closed = false;
            this._id = __.Guid.NewShort();
            this._opt = _localBus.GetOptions();
            this._unansweredMessages = [];
            var ipBuff = this._ws.getRemoteAddress();
            this._peerIp = new Uint8Array(ipBuff.slice(ipBuff.byteLength - 4)).join('.'); //ip address
            this._opt.LoggerInfo('Accepted ServerReceivedConnection WebSocket connection from ' + this._peerIp, this._localBus);
        }
        ServerReceivedConnection.prototype.GetUnansweredMessages = function () {
            return this._unansweredMessages;
        };
        ServerReceivedConnection.prototype.GetWs = function () {
            return this._ws;
        };
        ServerReceivedConnection.prototype.GetPeerAddress = function () {
            return this._peerIp;
        };
        ServerReceivedConnection.prototype.GetPeerBusName = function () {
            return this._peerBusName;
        };
        ServerReceivedConnection.prototype.SendMessage = function (msg) {
            if (this._closed)
                return false;
            this._unansweredMessages.push(msg);
            this._ws.send(JSON.stringify(msg), false, true);
            return true;
        };
        ServerReceivedConnection.prototype.IsClosed = function () {
            return this._closed;
        };
        ServerReceivedConnection.prototype.Close = function () {
            if (this._closed)
                return false;
            this._closed = true;
            this._opt.LoggerInfo('Closed ServerReceivedConnection WebSocket with ' + this.GetPeerAddress(), this._localBus);
            try {
                this._ws.close();
            }
            catch (e) {
            }
            return true;
        };
        ServerReceivedConnection.prototype.GetId = function () {
            return this._id;
        };
        return ServerReceivedConnection;
    }());
    //TODO: To be protected: split private class form public exposable stuff
    var ClientConnectionToServer = /** @class */ (function () {
        function ClientConnectionToServer(_ws, _peerBusName, _localBus) {
            this._ws = _ws;
            this._peerBusName = _peerBusName;
            this._localBus = _localBus;
            this._closed = false;
            this._id = __.Guid.NewShort();
            this._opt = _localBus.GetOptions();
            this._unansweredMessages = [];
            this._peerUrl = _ws.url;
            this._opt.LoggerInfo('Created ClientConnectionToServer WebSocket to ' + this._peerUrl, this._localBus);
        }
        ClientConnectionToServer.prototype.GetUnansweredMessages = function () {
            return this._unansweredMessages;
        };
        ClientConnectionToServer.prototype.GetWs = function () {
            return this._ws;
        };
        ClientConnectionToServer.prototype.GetPeerAddress = function () {
            return this._peerUrl;
        };
        ClientConnectionToServer.prototype.GetPeerBusName = function () {
            return this._peerBusName;
        };
        ClientConnectionToServer.prototype.SendMessage = function (msg) {
            if (this._closed)
                return false;
            this._unansweredMessages.push(msg);
            this._ws.send(JSON.stringify(msg));
            return true;
        };
        ClientConnectionToServer.prototype.IsClosed = function () {
            return this._closed;
        };
        ClientConnectionToServer.prototype.Close = function () {
            if (this._closed)
                return false;
            this._closed = true;
            this._opt.LoggerInfo('Closed ClientConnectionToServer WebSocket with ' + this.GetPeerAddress(), this._localBus);
            try {
                this._ws.close();
            }
            catch (e) {
            }
            return true;
        };
        ClientConnectionToServer.prototype.GetId = function () {
            return this._id;
        };
        return ClientConnectionToServer;
    }());
    /*##########################################################################
    #########################   EXPORTS   #########################
    ##########################################################################*/
    var Bus = /** @class */ (function () {
        function Bus(opt) {
            var _this = this;
            var me = this;
            if (__.IsNull(opt))
                opt = {};
            this._opt = __.CloneObj(opt);
            //set DEFAULTS
            if (!__.IsString(this._opt.Name))
                this._opt.Name = "Default Bus";
            if (__.IsNullOrEmpty(this._opt.NamespacesSeparator) || this._opt.NamespacesSeparator.indexOf("*") >= 0)
                this._opt.NamespacesSeparator = ":";
            if (!__.IsFunction(this._opt.LoggerInfo))
                this._opt.LoggerInfo = function () { };
            if (!__.IsFunction(this._opt.LoggerError))
                this._opt.LoggerError = function () { };
            if (!__.IsFunction(this._opt.LoggerDebug))
                this._opt.LoggerDebug = function () { };
            if (!__.IsFunction(this._opt.OnRequestPermissionsForConnection))
                this._opt.OnRequestPermissionsForConnection = function (fromBusName, conn, setPermissions) {
                    //default rules (WARNING: they are the most permissive!)
                    setPermissions({
                        Connection: conn,
                        EventsCanForwardTo: ["**"],
                        EventsCanReceiveFrom: ["**"]
                    });
                };
            /*#############  implement methods accessing REAL private stuff  #############################################*/
            var myPrivateSymbol = Symbol();
            var myPrivateStuff = {
                Subscriptions: [],
                RemoteConnections: [],
                ConnectionsPermissions: {},
                ListeningSockets: {}
            };
            ___privates[myPrivateSymbol] = myPrivateStuff;
            this["GetSubscriptions"] = function () {
                return __.CloneObj(myPrivateStuff.Subscriptions);
            };
            this["GetRemoteConnections"] = function () {
                return myPrivateStuff.RemoteConnections;
            };
            this["Subscribe"] = function (subscription) {
                if (!Bus.IsSubscription(subscription)) {
                    _this._opt.LoggerError("Subscribe must receive an ISubscription object", _this);
                    return false;
                }
                _this._opt.LoggerDebug("Subscription made to event pattern '" + subscription.SubscribeTo + "' by '" + __.EnsureString(subscription.SubscriberName) + "'", _this);
                myPrivateStuff.Subscriptions.push(subscription);
                return true;
            };
            this["RemoveSubscription"] = function (subscription) {
                if (!Bus.IsSubscription(subscription)) {
                    _this._opt.LoggerError("RemoveSubscription must receive an ISubscription object", _this);
                    return myPrivateStuff.Subscriptions.length;
                }
                var oldLength = myPrivateStuff.Subscriptions.length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, function (s) {
                    return s.SubscribeTo == subscription.SubscribeTo && s.Callback == subscription.Callback;
                });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };
            this["RemoveSubscriptionBySubscribeTo"] = function (subscribeTo) {
                var oldLength = me.GetSubscriptions().length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, function (s) { return s.SubscribeTo == subscribeTo; });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };
            this["RemoveSubscriptionByCallback"] = function (callback) {
                var oldLength = myPrivateStuff.Subscriptions.length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, function (s) { return s.Callback == callback; });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };
            function HandleReceivedWsMsg(ws, receivedData) {
                //search for the corresponding connection
                var conns = me.GetRemoteConnections().filter(function (x) { return x.IsClosed() == false && x.GetWs() == ws; });
                if (conns.length != 1) {
                    me._opt.LoggerError("Received a message from WebSocket, but can't associate WebSocket message to an active connection", me);
                    //TODO: answer with an ERROR otherwise peer would waitttt....
                    return;
                }
                var connection = conns[0];
                var message = JSON.parse(__.IsString(receivedData) ? receivedData : __.Node.ArrayBufferToUtf8String(receivedData));
                if (message.Type == 0) //System
                 {
                }
                else if (message.Type == 1) //EventSent
                 {
                    me._opt.LoggerDebug("Received a message from WebSocket, detected to be an EventSent '" + message.Data.Name +
                        "' fired by '" + __.EnsureString(message.Data.FiredBy) + "' from remote connection '" +
                        connection.GetPeerBusName() + " (" + connection.GetPeerAddress() + ")'.", me);
                    //check permissions
                    var connPermissions = myPrivateStuff.ConnectionsPermissions[connection.GetId()];
                    var permissionAllow = __.SearchInArray(connPermissions.EventsCanReceiveFrom, function (namespace) {
                        return me.MatchesEventName(namespace, message.Data.Name);
                    });
                    if (permissionAllow < 0) {
                        me._opt.LoggerDebug("No valid permission has been found to accept this message. Discard it.", me);
                        //TODO: answer with an ERROR otherwise peer would waitttt....
                        return;
                    }
                    me._opt.LoggerDebug("Permission has been found to accept this message. Publish it here and collect results...", me);
                    //Publish it here and collect results
                    me.Publish(message.Data, null, function (firingEvent, publishResults, publishTimeout) {
                        me._opt.LoggerDebug("...sending collected results via WebSocket for event '" + firingEvent.Name + "' to '" + __.EnsureString(message.Data.FiredBy) + "'.", me);
                        connection.SendMessage({
                            Type: 2,
                            Data: publishResults,
                            Id: message.Id
                        });
                    }, null, [connection]);
                }
                else if (message.Type == 2) //AnswerToEvent
                 {
                    //search for the corresponding unanswered message
                    var msgPos = __.SearchInArray(connection.GetUnansweredMessages(), function (x) { return x.Id == message.Id; });
                    if (msgPos < 0) {
                        me._opt.LoggerError("Received a message from WebSocket, but can't associate the received IBusRemoteAnswer (Id = " + message.Id + ") to any unanswered message for this connection", me);
                        return;
                    }
                    var originalMessage = connection.GetUnansweredMessages()[msgPos];
                    __.RemoveFromArray(connection.GetUnansweredMessages(), originalMessage);
                    me._opt.LoggerDebug("Received a message from WebSocket, detected to be an AnswerToEvent '" + originalMessage.Data.Name + "'. Pass it to the original publisher '" + __.EnsureString(originalMessage.Data.FiredBy) + "'.", me);
                    originalMessage.OnAnswer(message.Data);
                }
                else {
                    //TODO: answer with an ERROR otherwise peer would waitttt....
                    return;
                }
            }
            this["RemoteConnectToBusServer"] = function (url, onEnd) {
                if (onEnd === void 0) { onEnd = null; }
                if (isNode) {
                    //TODO
                    //https://github.com/theturtle32/WebSocket-Node/blob/master/docs/WebSocketClient.md
                    // var WebSocketClient = require('websocket').client;
                    // var client = new WebSocketClient();
                    // var tunnel = require('tunnel');
                    //
                    // var tunnelingAgent = tunnel.httpOverHttp({
                    //     proxy: {
                    //         host: 'proxy.host.com',
                    //         port: 8080
                    //     }
                    // });
                    //
                    // var requestOptions = {
                    //     agent: tunnelingAgent
                    // };
                    //
                    // client.connect('ws://echo.websocket.org/', null, null, null, requestOptions);
                }
                else {
                    var browserSocket_1 = new WebSocket(url);
                    browserSocket_1.onclose = function (ev) {
                        var conn = __.SearchValInArray(myPrivateStuff.RemoteConnections, function (rc) { return rc.GetWs() == browserSocket_1; });
                        if (__.IsNotNull(conn)) {
                            conn.Close(); //need this to change its state
                            __.RemoveFromArray(myPrivateStuff.RemoteConnections, conn);
                        }
                    };
                    browserSocket_1.onerror = function (ev) {
                        //hopefully this happens only during connection
                        if (__.IsFunction(onEnd))
                            onEnd(null, new Error("Readystate=" + browserSocket_1.readyState));
                    };
                    browserSocket_1.onmessage = function (ev) {
                        HandleReceivedWsMsg(browserSocket_1, ev.data);
                    };
                    browserSocket_1.onopen = function (ev) {
                        //TODO: implement HandShake to get remote peer features
                        var newConn = new ClientConnectionToServer(browserSocket_1, "TODO: unknown bus name", me);
                        myPrivateStuff.RemoteConnections.push(newConn);
                        me._opt.OnRequestPermissionsForConnection(newConn.GetPeerBusName(), newConn, function (permissions) {
                            var permissionToSave = __.CloneObj(permissions);
                            if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                                permissionToSave.EventsCanForwardTo = [];
                            if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                                permissionToSave.EventsCanReceiveFrom = [];
                            myPrivateStuff.ConnectionsPermissions[newConn.GetId()] = permissionToSave;
                        });
                        if (__.IsFunction(onEnd))
                            onEnd(newConn, null);
                    };
                }
            };
            this["StopListening"] = function (socketId) {
                if (__.IsNotEmptyString(socketId)) {
                    try {
                        var sock = myPrivateStuff.ListeningSockets[socketId];
                        if (__.IsNotNull(sock))
                            uWS.us_listen_socket_close(sock);
                        delete myPrivateStuff.ListeningSockets[socketId];
                    }
                    catch (e) { }
                }
                else {
                    for (var sockId in myPrivateStuff.ListeningSockets) {
                        if (!myPrivateStuff.ListeningSockets.hasOwnProperty(sockId))
                            continue;
                        try {
                            uWS.us_listen_socket_close(myPrivateStuff.ListeningSockets[sockId]);
                            delete myPrivateStuff.ListeningSockets[sockId];
                        }
                        catch (e) { }
                    }
                }
            };
            this["UpdateConnectionsPermissions"] = function () {
                myPrivateStuff.RemoteConnections.forEach(function (conn) {
                    me._opt.OnRequestPermissionsForConnection(conn.GetPeerBusName(), conn, function (permissions) {
                        var permissionToSave = __.CloneObj(permissions);
                        if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                            permissionToSave.EventsCanForwardTo = [];
                        if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                            permissionToSave.EventsCanReceiveFrom = [];
                        myPrivateStuff.ConnectionsPermissions[conn.GetId()] = permissionToSave;
                    });
                });
            };
            this["GetConnectionPermissions"] = function (conn) {
                return __.CloneObj(myPrivateStuff.ConnectionsPermissions[conn.GetId()]);
            };
            this["GetAllConnectionsPermissions"] = function () {
                return __.CloneObj(myPrivateStuff.ConnectionsPermissions);
            };
            if (isNode) {
                this["ListenForRemoteConnection"] = function (port, onListening, onConnection) {
                    if (onListening === void 0) { onListening = null; }
                    if (onConnection === void 0) { onConnection = null; }
                    if (!__.IsFunction(onListening))
                        onListening = function () { };
                    if (!__.IsFunction(onConnection))
                        onConnection = function () { };
                    uWS.App({
                    /* There are tons of SSL options
                    key_file_name: 'misc/key.pem',
                    cert_file_name: 'misc/cert.pem',
*/
                    }).ws('/*', {
                        compression: 1 /*uWS.CompressOptions.SHARED_COMPRESSOR*/,
                        message: function (ws, message, isBinary) {
                            HandleReceivedWsMsg(ws, message);
                        },
                        /** Handler for new WebSocket connection. WebSocket is valid from open to close, no errors. */
                        open: function (ws, req) {
                            var newConn = new ServerReceivedConnection(ws, "TODO: unknown bus name", me);
                            myPrivateStuff.RemoteConnections.push(newConn);
                            me._opt.OnRequestPermissionsForConnection(newConn.GetPeerBusName(), newConn, function (permissions) {
                                var permissionToSave = __.CloneObj(permissions);
                                if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                                    permissionToSave.EventsCanForwardTo = [];
                                if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                                    permissionToSave.EventsCanReceiveFrom = [];
                                myPrivateStuff.ConnectionsPermissions[newConn.GetId()] = permissionToSave;
                            });
                            onConnection(newConn);
                        },
                        /** Handler for close event, no matter if error, timeout or graceful close. You may not use WebSocket after this event. */
                        close: function (ws, code, message) {
                            var conn = __.SearchValInArray(myPrivateStuff.RemoteConnections, function (rc) { return rc.GetWs() == ws; });
                            if (__.IsNotNull(conn)) {
                                conn.Close(); //need this to change its state
                                __.RemoveFromArray(myPrivateStuff.RemoteConnections, conn);
                            }
                        }
                    }).listen(port, function (listenSocket) {
                        var err = listenSocket === false;
                        var socketId = __.Guid.NewShort();
                        myPrivateStuff.ListeningSockets[socketId] = listenSocket;
                        if (err) {
                            me._opt.LoggerError('ERROR listening on port ' + port, me);
                            onListening(null, new Error('ERROR listening on port ' + port));
                        }
                        else {
                            me._opt.LoggerInfo('Listening on port ' + port, me);
                            onListening(socketId, null);
                        }
                    });
                };
            }
            this._opt.LoggerDebug('Created bus instance "' + this._opt.Name + '".', this);
        }
        ;
        /**
         *
         * @param subscriptionString The "subscribeTo" string (with eventual wildcards)
         * Examples:
         *      "**"                    matches all
         *      "*"                     matches "MySingle"
         *      "App:MainOperation"     matches none
         *      "App:MainOperation:*"   matches "App:MainOperation:Minimize"
         *      "App:MainOperation:**"  matches "App:MainOperation:Minimize" and "App:MainOperation:Exit:Fatal"
         *      "App:*:Minimize"        matches "App:MainOperation:Minimize" and "App:Project:Minimize"
         *      "**:MasterLog"          matches "App:Project:Render:MasterLog"
         *
         * @param eventName The event name to match.
         * * Examples:
         *      "App:MainOperation:Minimize"
         *      "App:MainOperation:Exit:Fatal"
         *      "App:Project:Minimize"
         *      "App:Project:Render:MasterLog"
         *      "MySingle"
         */
        Bus.prototype.MatchesEventName = function (subscriptionString, eventName) {
            if (!__.IsString(subscriptionString) || !__.IsString(eventName))
                return false;
            var me = this;
            var subscriptionNamespaces = subscriptionString.split(me._opt.NamespacesSeparator);
            var eventNamespaces = eventName.split(me._opt.NamespacesSeparator);
            var iCurrEvt = 0;
            for (var iSub = 0; iSub < subscriptionNamespaces.length; iSub++) {
                var subToken = subscriptionNamespaces[iSub];
                var evtToken = eventNamespaces[iCurrEvt];
                if (subToken == "**") {
                    if (subscriptionNamespaces[iSub + 1] == "**") //syntax error: can't
                     {
                        me._opt.LoggerError("subscriptionString syntax error: cant have two ** as siblings", this);
                        return false;
                    }
                    else if (iSub == subscriptionNamespaces.length - 1) //last token: nothing more to do
                        return true;
                    //case "**:...."
                    //like "**:asd" or "**:asd:*:qwe" or "**:*:asd"
                    //try to match the remaining subscription string with all remaining evtName possibilities
                    var remainingSubscriptionString = subscriptionNamespaces.slice(iSub + 1, subscriptionNamespaces.length).join(me._opt.NamespacesSeparator);
                    for (var iRemainingEvt = iCurrEvt; iRemainingEvt < eventNamespaces.length; iRemainingEvt++) {
                        var remainingEventTrial = eventNamespaces.slice(iRemainingEvt, eventNamespaces.length).join(me._opt.NamespacesSeparator);
                        if (me.MatchesEventName(remainingSubscriptionString, remainingEventTrial))
                            return true;
                    }
                    return false;
                }
                else if (iCurrEvt >= eventNamespaces.length) {
                    //the current subToken requires the presence of at least one more event namespace which is not present
                    return false;
                }
                else if (subToken == "*") {
                    //go on with both indexes
                }
                else if (subToken != evtToken) {
                    return false;
                }
                //the 2 current tokens match: go on with both indexes
                iCurrEvt++;
            }
            //reached the end of subscriptionNamespaces
            if (iCurrEvt != eventNamespaces.length)
                return false; //event has other tokens
            else
                return true; //reached the end also of eventNamespaces
        };
        Bus.IsSubscription = function (subscription) {
            return __.IsNotNull(subscription) && __.IsString(subscription.SubscribeTo) && __.IsFunction(subscription.Callback);
        };
        Bus.IsEvent = function (evt) {
            return __.IsNotNull(evt) && __.IsString(evt.Name);
        };
        /*##########################################################################
        #########################   PUBLIC STUFF   #########################
        ##########################################################################*/
        /**
         * Returns a copy of current options (not modifiable)
         */
        Bus.prototype.GetOptions = function () {
            return __.CloneObj(this._opt);
        };
        Bus.prototype.GetMatchingSubscriptionsForEventName = function (evtName) {
            var me = this;
            return me.GetSubscriptions().filter(function (sub) { return me.MatchesEventName(sub.SubscribeTo, evtName); });
        };
        Bus.prototype.GetMatchingSubscriptionsCountForEventName = function (evtName) {
            return this.GetMatchingSubscriptionsForEventName(evtName).length;
        };
        Bus.prototype.GetSubscriptionsCount = function () {
            return this.GetSubscriptions().length;
        };
        /**
         * Returns a COPY of registered subscriptions
         */
        Bus.prototype.GetSubscriptions = function () { throw new Error("BAD method implementation"); };
        Bus.prototype.GetRemoteConnections = function () { throw new Error("BAD method implementation"); };
        Bus.prototype.Subscribe = function (subscription) { throw new Error("BAD method implementation"); };
        /**
         * Returns the number of removed subscriptions
         */
        Bus.prototype.RemoveSubscription = function (subscription) { throw new Error("BAD method implementation"); };
        /**
         * Returns the number of removed subscriptions
         */
        Bus.prototype.RemoveSubscriptionBySubscribeTo = function (subscribeTo) { throw new Error("BAD method implementation"); };
        /**
         * @returns {number} Returns the number of removed subscriptions
         */
        Bus.prototype.RemoveSubscriptionByCallback = function (callback) { throw new Error("BAD method implementation"); };
        /**
         *
         * @returns Returns LOCAL subscriptions matching the event name which will be fired.
         * Connection's belonging subscriptions are ignored from this (sync) return
         *
         * @param evt
         * @param onLocalSubscriptionEnded
         * @param onAllSubscribersEnded
         * WARNING: the number of results is:    # local subscriptions + [FOR EACH remote connections ] # remote subscriptions
         * considering only subscriptions matching the event name AND connections accepted by permissions
         *
         * @param forwardOnlyToTheseConnections
         * @param dontForwardToTheseConnections
         */
        Bus.prototype.Publish = function (evt, onLocalSubscriptionEnded, onAllSubscribersEnded, forwardOnlyToTheseConnections, dontForwardToTheseConnections) {
            var _this = this;
            if (!Bus.IsEvent(evt)) {
                this._opt.LoggerError("PublishAndForget must receive an IBusEvent object", this);
                return null;
            }
            if (!__.IsFunction(onLocalSubscriptionEnded))
                onLocalSubscriptionEnded = function () { };
            if (!__.IsFunction(onAllSubscribersEnded))
                onAllSubscribersEnded = function () { };
            this._opt.LoggerDebug("Published event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "'", this);
            //execute local subscriptions
            var me = this;
            var localSubs = me.GetMatchingSubscriptionsForEventName(evt.Name);
            var parallel = __.Parallel();
            localSubs.forEach(function (sub) {
                parallel = parallel.Parallel(function (onEnd) {
                    var context = {
                        FiringEvent: evt,
                        Async: false,
                        AsyncEnded: function (asyncResult) {
                            if (!this.Async)
                                return;
                            //call eventually passed callback
                            onLocalSubscriptionEnded.call(me, evt, sub, asyncResult, true);
                            onEnd(asyncResult);
                        }
                    };
                    var syncResult = sub.Callback.call(me, context);
                    if (!context.Async) {
                        //call eventually passed callback
                        onLocalSubscriptionEnded.call(me, evt, sub, syncResult, false);
                        onEnd(syncResult);
                    }
                });
            });
            //execute remote subscriptions (one parallel execution per accepted connection)
            var permissions = me.GetAllConnectionsPermissions();
            var connections = me.GetRemoteConnections(); //then filter them
            if (__.IsArray(forwardOnlyToTheseConnections))
                connections = forwardOnlyToTheseConnections;
            if (__.IsArray(dontForwardToTheseConnections))
                connections = connections.filter(function (conn) { return __.SearchValInArray(dontForwardToTheseConnections, function (toExclude) { return toExclude.GetId() == conn.GetId(); }) == null; });
            connections = connections.filter(function (conn) {
                var connPermissions = permissions[conn.GetId()];
                if (__.IsNull(connPermissions))
                    return false;
                var permissionAllow = __.SearchInArray(connPermissions.EventsCanForwardTo, function (namespace) {
                    return me.MatchesEventName(namespace, evt.Name);
                });
                if (permissionAllow < 0) //no permission for this connection
                    return false;
                return true;
            });
            connections.forEach(function (conn) {
                _this._opt.LoggerDebug("Forwarding published event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "' to remote connection '" + conn.GetPeerBusName() + " (" + conn.GetPeerAddress() + ")'", _this);
                parallel = parallel.Parallel(function (onEnd) {
                    conn.SendMessage(new BusRemoteMessage(BusRemoteMessageType.EventSent, evt, function (response) {
                        onEnd(response);
                    }));
                });
            });
            parallel.ExecuteAndJoin(function (resultsOrderedByPosition, resultsOrderedByTime, timeoutException) {
                //TODO: handle forwardToRemoteConnections properly
                _this._opt.LoggerDebug("Publish of event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "' collected results. Notify its end.", _this);
                //collect results and eventually call passed callback
                var publishResults = [];
                for (var i = 0; i < localSubs.length; i++) {
                    publishResults.push({
                        SubscriberName: localSubs[i].SubscriberName,
                        SuccessfulResult: resultsOrderedByPosition[i].SuccessfulResult,
                    });
                }
                for (var i = localSubs.length; i < resultsOrderedByPosition.length; i++) {
                    var connectionAnswers = resultsOrderedByPosition[i].SuccessfulResult;
                    //explode the answering subscribers from this connection
                    publishResults = publishResults.concat(connectionAnswers);
                }
                //warning publishResults.length can be > localSubs.length + connections.length
                onAllSubscribersEnded.call(me, evt, publishResults, timeoutException);
            });
            return localSubs;
        };
        Bus.prototype.ListenForRemoteConnection = function (port, onListening, onConnection) {
            if (onListening === void 0) { onListening = null; }
            if (onConnection === void 0) { onConnection = null; }
            throw new Error("Not implemented in browser");
        };
        /**
         * Makes ALL server sockets stop listening
         * If a socketId is passed (received in callback of ListenForRemoteConnection) only that one will be closed.
         */
        Bus.prototype.StopListening = function (socketId) { throw new Error("BAD method implementation"); };
        Bus.prototype.CloseAllConnections = function () {
            this.GetRemoteConnections().forEach(function (conn) {
                conn.Close();
            });
        };
        Bus.prototype.RemoteConnectToBusServer = function (url, onEnd) {
            if (onEnd === void 0) { onEnd = null; }
            throw new Error("TODO: Not implemented");
        };
        Bus.prototype.GetAllConnectionsPermissions = function () { throw new Error("BAD method implementation"); };
        Bus.prototype.GetConnectionPermissions = function (conn) { throw new Error("BAD method implementation"); };
        Bus.prototype.UpdateConnectionsPermissions = function () { throw new Error("BAD method implementation"); };
        return Bus;
    }());
    IronLibsBus.Bus = Bus;
    var BusEvent = /** @class */ (function () {
        function BusEvent(Name, Payload, FiredBy) {
            if (Payload === void 0) { Payload = null; }
            if (FiredBy === void 0) { FiredBy = ""; }
            this.Name = Name;
            this.Payload = Payload;
            this.FiredBy = FiredBy;
            this.FiredAt = new Date();
        }
        return BusEvent;
    }());
    IronLibsBus.BusEvent = BusEvent;
    function GetDefault() {
        return mainBus;
    }
    IronLibsBus.GetDefault = GetDefault;
    function CreateDefault(opt) {
        mainBus = new Bus(opt);
        return mainBus;
    }
    IronLibsBus.CreateDefault = CreateDefault;
    function CreateNew(opt) {
        return new Bus(opt);
    }
    IronLibsBus.CreateNew = CreateNew;
})(IronLibsBus || (IronLibsBus = {}));
if (!isNode)
    IronLibs.IronLibsCommon.Bus = IronLibsBus;
module.exports = IronLibsBus;
