"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var IronLibs = require("../common");
var __ = IronLibs.IronLibsCommon;
var IronLibsRaspiPin;
(function (IronLibsRaspiPin) {
    //##################################################################################################################
    //######  MODULE-SCOPE PRIVATE STUFF
    //##################################################################################################################
    var GpioLib;
    if (process.platform != "win32")
        GpioLib = require('pigpio');
    /**
     *  IMPORTANT: the state representation and related data of each pin MUST reside in a SHARED space,
     *  NOT relied to each Pin instance (which can get lost upon all application modules)
     */
    var PinsTable = {};
    var BlinkBehaviour = /** @class */ (function () {
        function BlinkBehaviour(pinNumber, highTime, lowTime) {
            this.pinNumber = pinNumber;
            this.highTime = highTime;
            this.lowTime = lowTime;
            var self = this;
            var descriptor = PinsTable[pinNumber];
            var clbk = function () {
                if (self.stopped)
                    return;
                if (self.currVal == PIN_VALUE.HIGH) {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.HIGH);
                    //raspiPin.setHigh(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.HIGH);
                    if (globalOptions.Debug)
                        __.Log("  >> Pin " + self.pinNumber + " Blink " + __.GetEnumKey(PIN_VALUE, self.currVal));
                    self.currVal = PIN_VALUE.LOW;
                    setTimeout(clbk, self.highTime); //stay HIGH for highTime ms
                }
                else {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                    //raspiPin.setLow(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.LOW);
                    if (globalOptions.Debug)
                        __.Log("  >> Pin " + self.pinNumber + " Blink " + __.GetEnumKey(PIN_VALUE, self.currVal));
                    self.currVal = PIN_VALUE.HIGH;
                    setTimeout(clbk, self.lowTime); //stay HIGH for highTime ms
                }
            };
            this.stopped = false;
            this.currVal = PIN_VALUE.HIGH;
            if (globalOptions.Debug)
                __.Log("  >> Pin " + pinNumber + " started Blink (" +
                    __.GetEnumKey(PIN_VALUE, PIN_VALUE.HIGH) + ": " + highTime + ", " +
                    __.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW) + ": " + lowTime + ")");
            clbk();
        }
        /**
         * Make the loop DIE
         */
        BlinkBehaviour.prototype.Stop = function () {
            this.stopped = true;
            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.pinNumber + " stopped Blink");
        };
        return BlinkBehaviour;
    }());
    function Throw(msg) {
        if (globalOptions.UseCustomExceptions)
            throw new OperationException(msg);
        else
            throw new Error(msg);
    }
    var globalOptions = {
        UseCustomExceptions: true,
        Debug: true
    };
    var initialized = false;
    var OperationException = /** @class */ (function (_super) {
        __extends(OperationException, _super);
        function OperationException(message) {
            var _this = _super.call(this, message) || this;
            _this.message = message;
            _this.name = 'OperationException';
            _this.message = message;
            _this.stack = new Error().stack;
            return _this;
        }
        OperationException.prototype.toString = function () {
            return this.name + ': ' + this.message;
        };
        return OperationException;
    }(Error));
    IronLibsRaspiPin.OperationException = OperationException;
    var PIN_MODE;
    (function (PIN_MODE) {
        PIN_MODE[PIN_MODE["UNDEFINED"] = 0] = "UNDEFINED";
        PIN_MODE[PIN_MODE["SIMPLE_OUTPUT"] = 1] = "SIMPLE_OUTPUT";
        PIN_MODE[PIN_MODE["INPUT"] = 2] = "INPUT";
        PIN_MODE[PIN_MODE["SW_PWM"] = 3] = "SW_PWM";
        PIN_MODE[PIN_MODE["HW_PWM"] = 4] = "HW_PWM";
        PIN_MODE[PIN_MODE["BLINK"] = 5] = "BLINK";
        PIN_MODE[PIN_MODE["FADE"] = 6] = "FADE";
    })(PIN_MODE = IronLibsRaspiPin.PIN_MODE || (IronLibsRaspiPin.PIN_MODE = {}));
    var PIN_VALUE;
    (function (PIN_VALUE) {
        PIN_VALUE[PIN_VALUE["LOW"] = 0] = "LOW";
        PIN_VALUE[PIN_VALUE["HIGH"] = 1] = "HIGH";
    })(PIN_VALUE = IronLibsRaspiPin.PIN_VALUE || (IronLibsRaspiPin.PIN_VALUE = {}));
    /**
     * Ensures that the GpioLib library is initialized.
     * Multiple calls are correctly handled
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Initialize( /*setRealtimeScheduling : boolean = true*/) {
        if (initialized)
            return true;
        initialized = __.IsNotNull(GpioLib); //raspiPin.initialize();
        // if(initialized && setRealtimeScheduling)
        //     raspiPin.setRealtimeScheduling();
        return initialized;
    }
    IronLibsRaspiPin.Initialize = Initialize;
    /**
     * Invokes the GpioLib library terminations (releases internal TCP ports for remote control, etc...)
     * To be invoked after Initialize
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Terminate() {
        if (!initialized)
            return false;
        GpioLib.terminate();
        initialized = false;
        return true;
    }
    IronLibsRaspiPin.Terminate = Terminate;
    /**
     * Merges the specified options to the main ones
     * @param newOptions a subset of IOptions
     */
    function SetGlobalOptions(newOptions) {
        if (!__.IsNotNullObject(newOptions))
            Throw("SetGlobalOptions accepts an object");
        globalOptions = __.MergeObj(newOptions, globalOptions);
    }
    IronLibsRaspiPin.SetGlobalOptions = SetGlobalOptions;
    var Pin = /** @class */ (function () {
        function Pin(num, initialMode) {
            if (initialMode === void 0) { initialMode = PIN_MODE.UNDEFINED; }
            this.num = num;
            if (__.IsNull(PinsTable[this.num])) {
                if (__.IsNull(GpioLib)) {
                    __.Log("  >> ERROR (unsupported in Windows) Can't initialize Pin " + this.num + " initialized as " + __.GetEnumKey(PIN_MODE, initialMode));
                    return;
                }
                //FIRST time we used this pin: create its main table entry
                var pinMode = { mode: GpioLib.Gpio.OUTPUT };
                if (initialMode == PIN_MODE.INPUT)
                    pinMode = { mode: GpioLib.Gpio.INPUT };
                if (globalOptions.Debug)
                    __.Log("  >> Creating Pin " + this.num + " instance");
                PinsTable[this.num] = {
                    Mode: initialMode,
                    GPio: new GpioLib.Gpio(this.num, pinMode)
                };
                //
                // if (initialMode != PIN_MODE.UNDEFINED)
                // {
                //     raspiPin.setPinMode(this.num, initialMode == PIN_MODE.INPUT ? 0 : 1);
                // }
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " initialized as " + __.GetEnumKey(PIN_MODE, initialMode));
            }
        }
        //##################################################################################################################
        //######  PRIVATE STUFF
        // ############################################################################################
        // ##################################################################################################################
        //##################################################################################################################
        //######  PUBLIC STUFF  ############################################################################################
        //##################################################################################################################
        Pin.prototype.GetMode = function () { return PinsTable[this.num].Mode; };
        Pin.prototype.GetNumber = function () { return this.num; };
        /**
         * Clears the internal pin status STOPPING all its activities and write LOW
         * (to permit a change in its mode)
         */
        Pin.prototype.Reset = function () {
            if (__.IsNull(GpioLib)) {
                __.Log("  >> ERROR (unsupported in Windows) Reset Pin " + this.num);
                return;
            }
            this.SilentReset(true);
            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode) + " after Reset");
        };
        Pin.prototype.SilentReset = function (writeLow) {
            if (__.IsNull(GpioLib)) {
                __.Log("  >> ERROR (unsupported in Windows) SilentReset Pin " + this.num);
                return;
            }
            var descriptor = PinsTable[this.num];
            descriptor.Mode = PIN_MODE.UNDEFINED;
            //DESTROY any activity
            //TODO: check and stop timers
            if (__.IsNotNull(descriptor.Blink)) {
                descriptor.Blink.Stop();
                descriptor.Blink = null;
            }
            if (writeLow) {
                //WRITE the value
                descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                //raspiPin.setLow(this.num);
                //rpio.write(this.num, rpio.LOW);
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " Write " + __.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW));
            }
        };
        Pin.prototype.Write = function (value) {
            if (__.IsNull(GpioLib)) {
                __.Log("  >> ERROR (unsupported in Windows) Write Pin " + this.num + " VALUE " + __.GetEnumKey(PIN_VALUE, value));
                return;
            }
            var descriptor = PinsTable[this.num];
            //TYPES check (mainly for non-TypeScript user code)
            if (!__.IsNumber(value))
                Throw("Write accepts a numeric value");
            //MODE check and set
            var mode = PinsTable[this.num].Mode;
            if (mode == PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SIMPLE_OUTPUT));
            }
            else if (mode != PIN_MODE.SIMPLE_OUTPUT) {
                this.SilentReset(false); //DESTROY any activity
                PinsTable[this.num].Mode = PIN_MODE.SIMPLE_OUTPUT;
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode));
                descriptor.GPio.mode(GpioLib.Gpio.OUTPUT);
                //raspiPin.setPinMode(this.num, 1);
            }
            //WRITE the value
            descriptor.GPio.digitalWrite(value);
            // if(value == PIN_VALUE.HIGH)
            //     raspiPin.setHigh(this.num);
            // else
            //     raspiPin.setLow(this.num);
            //rpio.write(this.num, value == PIN_VALUE.HIGH ? rpio.HIGH : rpio.LOW);
            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " Write " + __.GetEnumKey(PIN_VALUE, value));
        };
        //
        // public StartPwm(hertz : number, percentage : number) : boolean
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("StartPwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     var ret : boolean = raspiPin.startPwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " START PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        //
        //     return ret;
        // }
        //
        //
        // public StartHwPwm(percentage : number) : boolean
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(percentage))
        //         Throw("StartPwm accepts 1 numeric value");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     var ret : boolean = raspiPin.startHwPwm(this.num, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " START HARDWARE PWM\tpercentage: " + limitedPerc + "%");
        //
        //     return ret;
        // }
        //
        // public ChangePwm(hertz : number, percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.changePwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " CHANGE PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        // }
        //
        // public ChangeHwPwm(percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 1 numeric value");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.changeHwPwm(this.num, percentage);
        //
        //
        //     if (globalOptions.Debug)
        //     {
        //         var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //         __.Log("  >> Pin " + this.num + " CHANGE HARDWARE PWM\tpercentage: " + limitedPerc + "%");
        //     }
        // }
        //
        //
        // public StopPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.stopPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP PWM");
        // }
        //
        //
        // public StopHwPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.stopHwPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP HARDWARE PWM");
        // }
        Pin.prototype.Read = function () {
            //MODE check and set
            var descriptor = PinsTable[this.num];
            if (descriptor.Mode != PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, descriptor.Mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT));
            }
            //READ the value
            var value = descriptor.GPio.digitalRead();
            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " Read value: " + __.GetEnumKey(PIN_VALUE, value));
            return value;
        };
        Pin.prototype.Blink = function (highTime, lowTime) {
            if (lowTime === void 0) { lowTime = highTime; }
            if (__.IsNull(GpioLib)) {
                __.Log("  >> ERROR (unsupported in Windows) Blink Pin " + this.num);
                return;
            }
            //TYPES check (mainly for non-TypeScript user code)
            if (!__.IsNumber(highTime) || !__.IsNumber(lowTime))
                Throw("Blink accepts at least one numeric value");
            //MODE check and set
            var mode = PinsTable[this.num].Mode;
            if (mode == PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.BLINK));
            }
            else if (mode != PIN_MODE.BLINK) {
                PinsTable[this.num].Mode = PIN_MODE.BLINK;
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode));
            }
            //DESTROY any activity (also a previous blink in order to avoid to wait for its end to start the new one....)
            this.SilentReset(false);
            //Start the blink
            PinsTable[this.num].Blink = new BlinkBehaviour(this.num, highTime, lowTime);
        };
        return Pin;
    }());
    IronLibsRaspiPin.Pin = Pin;
})(IronLibsRaspiPin || (IronLibsRaspiPin = {}));
module.exports = IronLibsRaspiPin;
