"use strict";
var express = require("express");
var IronLibs = require("../common");
var __ = IronLibs.IronLibsCommon;
var IronLibsExpressMVC;
(function (IronLibsExpressMVC) {
    var NotificationType;
    (function (NotificationType) {
        NotificationType[NotificationType["ERROR"] = 0] = "ERROR";
        NotificationType[NotificationType["SUCCESS"] = 1] = "SUCCESS";
        NotificationType[NotificationType["INFO"] = 2] = "INFO";
    })(NotificationType = IronLibsExpressMVC.NotificationType || (IronLibsExpressMVC.NotificationType = {}));
    var ControllerBase = /** @class */ (function () {
        function ControllerBase() {
            this.router = null;
            this.router = express.Router();
        }
        ControllerBase.prototype.GetRouter = function () { return this.router; };
        /**
         * Registers a controller action invoked for ANY http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        ControllerBase.prototype.RegisterActionAll = function (url, clbk) {
            var me = this;
            var urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.all(u, clbk);
            });
        };
        /**
         * Registers a controller action invoked for GET http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        ControllerBase.prototype.RegisterActionGet = function (url, clbk) {
            var me = this;
            var urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.get(u, clbk);
            });
        };
        /**
         * Registers a controller action invoked for POST http verb
         *
         * @param url one or more urls (also with wildcards) to be matched in order to call the callback
         * @param clbk The action executed upon an url match
         */
        ControllerBase.prototype.RegisterActionPost = function (url, clbk) {
            var me = this;
            var urls = __.IsArray(url) ? url : [url];
            urls.forEach(function (u) {
                me.router.post(u, clbk);
            });
        };
        ControllerBase.prototype.Respond = function (res, msg, statusCode, headerValues) {
            if (statusCode === void 0) { statusCode = 200; }
            if (headerValues === void 0) { headerValues = null; }
            res.statusCode = statusCode;
            if (__.IsNotNullObject(headerValues)) {
                for (var key in headerValues) {
                    if (!headerValues.hasOwnProperty(key))
                        continue;
                    res.header(key, headerValues[key]);
                }
            }
            res.status(statusCode);
            if (__.IsNull(msg))
                msg = { IsValid: (statusCode >= 200 && statusCode < 300), Notifications: [] };
            if (__.IsString(msg))
                res.write(msg);
            else
                res.json(msg);
            res.end();
        };
        ControllerBase.prototype.RespondOk = function (res, data, notification, type) {
            if (data === void 0) { data = null; }
            if (notification === void 0) { notification = null; }
            if (type === void 0) { type = NotificationType.SUCCESS; }
            var ret = {
                IsValid: true,
                Notifications: [],
            };
            if (__.IsNotNull(data))
                ret.Data = data;
            if (__.IsString(notification))
                ret.Notifications.push({ MessageText: notification, Type: type });
            this.Respond(res, ret, 200);
        };
        ControllerBase.prototype.RespondError = function (res, data, notification, clientError) {
            if (data === void 0) { data = null; }
            if (notification === void 0) { notification = null; }
            if (clientError === void 0) { clientError = true; }
            var ret = {
                IsValid: false,
                Notifications: [],
            };
            if (__.IsNotNull(data))
                ret.Data = data;
            if (__.IsString(notification))
                ret.Notifications.push({ MessageText: notification, Type: NotificationType.ERROR });
            this.Respond(res, ret, clientError ? 400 : 500);
        };
        ControllerBase.prototype.RespondRedirect = function (res, where) {
            this.Respond(res, "", 302, { location: where });
        };
        return ControllerBase;
    }());
    IronLibsExpressMVC.ControllerBase = ControllerBase;
})(IronLibsExpressMVC || (IronLibsExpressMVC = {}));
module.exports = IronLibsExpressMVC;
