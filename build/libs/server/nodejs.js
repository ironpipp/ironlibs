"use strict";
var IronLibs = require("../common");
var https = require("https");
var http = require("http");
var fs = require("fs");
var path = require("path");
var child_process = require("child_process");
var __ = IronLibs.IronLibsCommon;
var IronLibsNodeJs;
(function (IronLibsNodeJs) {
    function IsUnixEnvironment() {
        return process.platform != "win32";
    }
    IronLibsNodeJs.IsUnixEnvironment = IsUnixEnvironment;
    function GetDirectoriesSeparator() {
        return IsUnixEnvironment() ? "/" : "\\";
    }
    IronLibsNodeJs.GetDirectoriesSeparator = GetDirectoriesSeparator;
    /**
     * Returns the APPLICATION DATA directory with the ending separator (ex: "/")
     */
    function GetDataDirectory(appName, ensureExists) {
        if (ensureExists === void 0) { ensureExists = true; }
        var ret;
        var sep = GetDirectoriesSeparator();
        if (__.IsNotEmptyString(process.env.APPDATA))
            ret = __.EnsureEndsWith(process.env.APPDATA, sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");
        else
            ret = __.EnsureEndsWith(process.cwd(), sep) + (__.IsNotEmptyString(appName) ? appName + sep : "");
        if (ensureExists) {
            var fs_1 = require("fs");
            if (!fs_1.existsSync(ret))
                fs_1.mkdirSync(ret);
        }
        return ret;
    }
    IronLibsNodeJs.GetDataDirectory = GetDataDirectory;
    /**
     * Reads package.json and ensures all required modules are installed. Will exit if one or more is not found.
     */
    function CheckModules() {
        var pack;
        try {
            pack = require("../package");
        }
        catch (e) {
            console.log("ERROR: Could not load package.json from project root. This file is required for reading project properties.");
            process.exit(1);
        }
        var moduleExists = function (modName) {
            try {
                return require.resolve(modName);
            }
            catch (e) {
                return false;
            }
        };
        var isModuleMissing = false;
        for (var key in pack["dependencies"]) {
            if (!moduleExists(key)) {
                isModuleMissing = true;
                console.log("ERROR: Missing module '" + key + "'");
            }
        }
        if (isModuleMissing) {
            console.log("ERROR: Required modules are not installed. Run 'npm install' from command line.");
            process.exit(1);
        }
        delete require.cache[pack];
    }
    IronLibsNodeJs.CheckModules = CheckModules;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToUtf8String(buff) {
        var uInt8Array = new Uint8Array(buff);
        var i = uInt8Array.length;
        var binaryString = new Array(i);
        while (i--) {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }
        return binaryString.join(''); //WARNING: returns UTF-8 string!!!
    }
    IronLibsNodeJs.ArrayBufferToUtf8String = ArrayBufferToUtf8String;
    /**
     * Return the BASE64 string of the SHA1 hash of the passed string
     */
    function GetHash(toBeHashed) {
        var crypto = require('crypto');
        toBeHashed = __.EnsureString(toBeHashed, "");
        var shasum = crypto.createHash('sha1');
        shasum.update(toBeHashed);
        return shasum.digest('base64');
    }
    IronLibsNodeJs.GetHash = GetHash;
    var sanitizeFilePath = null;
    /**
     * Accepts only a FILE name (with extension, NO path segments).
     * Returns a valid one.
     * Requires "sanitize-filename" package
     */
    function SanitizeFileName(fileName, replaceInvalidCharsWith) {
        if (replaceInvalidCharsWith === void 0) { replaceInvalidCharsWith = ""; }
        if (__.IsNull(sanitizeFilePath))
            sanitizeFilePath = require("sanitize-filename");
        if (!__.IsString(replaceInvalidCharsWith))
            replaceInvalidCharsWith = "";
        return sanitizeFilePath(fileName, { replacement: replaceInvalidCharsWith });
    }
    IronLibsNodeJs.SanitizeFileName = SanitizeFileName;
    /**
     * The SANITIZED paths tree will be returned (without file!). Il will always end with pathSegmentsSeparator.
     * Requires "sanitize-filename" package
     */
    function SanitizeFolderPath(fullPath, replaceInvalidCharsWith, pathSegmentsSeparator) {
        if (replaceInvalidCharsWith === void 0) { replaceInvalidCharsWith = ""; }
        if (pathSegmentsSeparator === void 0) { pathSegmentsSeparator = GetDirectoriesSeparator(); }
        if (__.StringEndsWith(fullPath, pathSegmentsSeparator))
            fullPath = fullPath.substr(0, fullPath.length - pathSegmentsSeparator.length);
        var segments = fullPath.split(pathSegmentsSeparator);
        var ret = segments[0] + pathSegmentsSeparator;
        for (var i = 1; i < segments.length; i++) {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + pathSegmentsSeparator;
        }
        return ret;
    }
    IronLibsNodeJs.SanitizeFolderPath = SanitizeFolderPath;
    /**
     * Ensures that all subdirectories exist. If one or more are missing creates them.
     *
     * fullPath MUST NOT point to a file, ONLY DIRECTORIES!
     * fullPath CAN contain invalid characters. But only its SANITIZED version is created and returned.
     */
    function EnsureSanitizedPathsTreeIsCreated(fullPath, replaceInvalidCharsWith) {
        if (replaceInvalidCharsWith === void 0) { replaceInvalidCharsWith = ""; }
        var separator = GetDirectoriesSeparator();
        var segments = fullPath.split(separator);
        var ret = segments[0] + separator;
        for (var i = 1; i < segments.length; i++) {
            segments[i] = SanitizeFileName(segments[i], replaceInvalidCharsWith);
            ret = ret + segments[i] + separator;
            if (!fs.existsSync(ret))
                fs.mkdirSync(ret);
        }
        return ret;
    }
    IronLibsNodeJs.EnsureSanitizedPathsTreeIsCreated = EnsureSanitizedPathsTreeIsCreated;
    /**
     * Checks if the passed string is an absolute path (valid for both UNIX and Windows)
     * Examples of absolute paths: \    \asd\qwe    /asd/qwe    d:\  c:\asd\qwe
     * @param s
     */
    function IsAbsolutePath(s) {
        return (s.length > 0 && (s[0] == "/" || s[0] == "\\")) || //ex:  \    \asd\qwe    /asd/qwe
            (s.length > 2 && s[1] == ":" && s[2] == "\\"); //ex:  d:\  c:\qwe
    }
    IronLibsNodeJs.IsAbsolutePath = IsAbsolutePath;
    function CopyFile(sourceFile, destFile, onEnd, deleteSourceAfterSuccessfulCopy) {
        if (onEnd === void 0) { onEnd = null; }
        if (deleteSourceAfterSuccessfulCopy === void 0) { deleteSourceAfterSuccessfulCopy = false; }
        if (!__.IsFunction(onEnd))
            onEnd = function () { };
        var is = fs.createReadStream(sourceFile);
        var os = fs.createWriteStream(destFile);
        is.on('error', onEnd);
        os.on('error', onEnd);
        is.pipe(os);
        is.on('end', function () {
            is.close();
            is.destroy();
            os.close();
            os.destroy();
        });
        is.on('close', function () {
            if (deleteSourceAfterSuccessfulCopy)
                fs.unlink(sourceFile, onEnd);
            else
                onEnd(null);
        });
    }
    IronLibsNodeJs.CopyFile = CopyFile;
    /**
     * Converts an ArrayBuffer to an UTF8 string
     */
    function ArrayBufferToBlob(buff) {
        var uInt8Array = new Uint8Array(buff);
        var i = uInt8Array.length;
        var binaryString = new Array(i);
        while (i--) {
            binaryString[i] = String.fromCharCode(uInt8Array[i]);
        }
        return binaryString.join(''); //WARNING: returns UTF-8 string!!!
    }
    IronLibsNodeJs.ArrayBufferToBlob = ArrayBufferToBlob;
    function OpenWithExplorer(path) {
        var exec = child_process.exec;
        exec('start "" "' + SanitizeFolderPath(path) + '"');
    }
    IronLibsNodeJs.OpenWithExplorer = OpenWithExplorer;
    /**
     * Executes a process with arguments, calling the specified callback only when the process EXITS or upon timeout (if set)
     * The returned ChildProcess can be ignored (or used to do stuff)
     *
     * @param cmd The path of the command to execute
     * @param cmdArgs Eventual arguments
     * @param clbkOnEnd When this is called it's SURE that the process is NOT running (ended correctly, with an error or timeout+kill)
     * @param timeout If specified > 0 and after this time the process is still running, it will be KILLED and callback called with an error
     * @param options optional parameters passed to child_process.execFile()
     */
    function ExecuteCommand(cmd, cmdArgs, clbkOnEnd, timeout, options) {
        if (timeout === void 0) { timeout = 5000; }
        if (options === void 0) { options = { encoding: null }; }
        if (!__.IsArray(cmdArgs))
            cmdArgs = [];
        if (!__.IsNumber(timeout))
            timeout = 5000;
        if (!__.IsFunction(clbkOnEnd))
            clbkOnEnd = function () { };
        var ended = false;
        var ex = child_process.execFile(cmd, cmdArgs, options, function (error, stdout, stderr) {
            if (ended)
                return;
            ended = true;
            if (__.IsNotNull(error))
                return clbkOnEnd(new Error("Error executing '" + (cmd + ' ' + cmdArgs.join(' ')) + "': EXITCODE " + error.code), stdout, stderr);
            //command ended correctly
            clbkOnEnd(null, stdout, stderr);
        });
        setTimeout(function () {
            if (!ended) {
                if (ex != null)
                    ex.kill();
                ended = true;
                clbkOnEnd(new Error("Timeout executing '" + (cmd + ' ' + cmdArgs.join(' ')) + "'"), null, null);
            }
        }, timeout);
        return ex;
    }
    IronLibsNodeJs.ExecuteCommand = ExecuteCommand;
    /**
     *  Compresses passed data to a valid .7z archive (contains only ONE file named "compressed")
     *  Requires "lzma" package
     *
     * @param dataToCompress The data to compress in any form
     * @param compressionLevel from 1 (fastest) to 9 (best)
     * @param onEnd The "compressedData" param can be converted to a typed array calling "new Uint8Array(compressedData)". The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaCompress(dataToCompress, compressionLevel, onEnd, onProgress) {
        var lzma = require("lzma");
        lzma.compress(dataToCompress, compressionLevel, onEnd, onProgress);
    }
    IronLibsNodeJs.LzmaCompress = LzmaCompress;
    /**
     *  Decompresses back the passed compressed data
     *  Requires "lzma" package
     *
     * @param compressedData The binary data to decompress
     * @param onEnd The "decompressedData" param is a string if it decodes as valid UTF-8 text, otherwise will be a Uint8Array instance. The "error" param is 0 upon success or the Error object otherwise
     * @param onProgress The "progress" param goes from 0 to 1
     */
    function LzmaDecompress(compressedData, onEnd, onProgress) {
        var lzma = require("lzma");
        lzma.decompress(compressedData, onEnd, onProgress);
    }
    IronLibsNodeJs.LzmaDecompress = LzmaDecompress;
    var simpleWebServerSockets = [];
    /**
     * Simply starts a web server serving static resources mapped to one or more directories on file system.
     * Can be used also for dynamic content (ex a small API) using onWebRequest callback
     *
     * @requires  {} NPM Libraries 'uWebSockets.js' and 'mime-types'
     * @param opt
     */
    function StartSimpleWebServer(opt) {
        var uWS = require('uWebSockets.js');
        var mime = require('mime-types');
        var defaultSimpleWebServerOptions = {
            port: 8989,
            basePath: "/",
            staticPathsMapping: [{ pathStartingWith: "/", serveFromDirectory: path.resolve(__dirname) }],
            cacheControlForStaticPaths: "max-age=3600",
            useSSL: false
        };
        if (!__.IsNotNullObject(opt))
            opt = __.CloneObj(defaultSimpleWebServerOptions);
        else
            opt = __.MergeObj(defaultSimpleWebServerOptions, opt);
        if (!__.StringStartsWith(opt.basePath, "/"))
            opt.basePath = "/" + opt.basePath;
        if (!__.StringEndsWith(opt.basePath, "/"))
            opt.basePath += "/";
        var onReq = function (res, req) {
            //We ALWAYS listen to onAborted to handle also ASYNC responses
            res.onAborted(function () {
                try {
                    res.end(); /*Can throw "Invalid access of discarded (invalid, deleted) uWS.HttpResponse/SSLHttpResponse"*/
                }
                catch (e) { }
            });
            if (__.IsFunction(opt.onWebRequest)) {
                if (opt.onWebRequest(res, req) == true)
                    return;
            }
            //resolve the file path
            var url = req.getUrl().replace(/\.\./g, "");
            var filePath;
            for (var i = 0; i < opt.staticPathsMapping.length; i++) {
                var map = opt.staticPathsMapping[i];
                if (__.StringStartsWith(url, map.pathStartingWith, false)) {
                    filePath = __.EnsureEndsWith(map.serveFromDirectory, GetDirectoriesSeparator());
                    filePath = path.resolve(filePath, url.substr(map.pathStartingWith.length));
                    break;
                }
            }
            if (__.IsNullOrEmpty(filePath)) {
                res.writeStatus("500");
                res.end("ERROR: server can't resolve your request");
                return;
            }
            //select the right content type
            var lastUrlSegment = url.substr(url.lastIndexOf("/"));
            if (lastUrlSegment.indexOf(".") >= 0)
                lastUrlSegment = lastUrlSegment.substr(lastUrlSegment.lastIndexOf("."));
            var contentType = mime.contentType(lastUrlSegment);
            if (contentType === false)
                contentType = 'application/octet-stream';
            var binary = contentType.indexOf("text") < 0 && contentType.indexOf("script") < 0;
            try {
                fs.readFile(filePath, binary ? undefined : { encoding: "utf8" }, function (err, data) {
                    if (err) {
                        if (__.StringStartsWith(err.message, "ENOENT", false)) {
                            res.writeStatus("404");
                            res.end("NOT FOUND");
                        }
                        else {
                            res.writeStatus("500");
                            res.end("ERROR: " + err.message);
                        }
                    }
                    else {
                        res.writeStatus("200");
                        res.writeHeader("content-type", contentType);
                        if (__.IsNotEmptyString(opt.cacheControlForStaticPaths))
                            res.writeHeader("cache-control", opt.cacheControlForStaticPaths);
                        res.end(data);
                    }
                });
            }
            catch (err) {
                res.writeStatus("500");
                res.end("ERROR: " + err.message);
            }
        };
        var app = opt.useSSL ? "SSLApp" : "App";
        uWS[app](opt.useSSL ? {
            key_file_name: opt.sslKeyFileName,
            cert_file_name: opt.sslCertFileName,
        } : {})
            .any(opt.basePath + '*', onReq)
            .any(opt.basePath, onReq)
            .listen(opt.port, function (listenSocket) {
            simpleWebServerSockets.push(listenSocket);
            if (__.IsFunction(opt.onListening)) {
                opt.onListening(__.IsNull(listenSocket));
            }
        });
    }
    IronLibsNodeJs.StartSimpleWebServer = StartSimpleWebServer;
    function StopAllSimpleWebServers() {
        var uWS = require('uWebSockets.js');
        simpleWebServerSockets.forEach(function (sock) {
            try {
                uWS.us_listen_socket_close(sock);
            }
            catch (e) { }
        });
        simpleWebServerSockets = [];
    }
    IronLibsNodeJs.StopAllSimpleWebServers = StopAllSimpleWebServers;
    /**
     * Makes a Server to Server HTTP[S] call and notifies the caller SURELY 1 ONLY TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text / binary)
     *
     * @param requestOptions Together with https.RequestOptions can be passed also dataToPost.
     * Warning: "content-type" header must be specified by the caller, while "content-length" is set automatically
     * @param onCallEnd The callback is called FOR SURE ONLY ONE TIME with
     * an instance of Error, or otherwise with the body of the response (JSON / text)
     * together with its HTTP StatusCode and https.IncomingMessage
     */
    function S2SCall(requestOptions, onCallEnd) {
        if (!__.IsFunction(onCallEnd))
            onCallEnd = function () { };
        var options = __.MergeObj({
            hostname: "localhost",
            port: 80,
            protocol: 'http:',
            path: "/",
            method: 'GET',
            timeout: 2000,
            rejectUnauthorized: true
        }, requestOptions);
        if (__.IsNotEmptyString(requestOptions.dataToPost)) {
            if (__.IsNull(options.headers))
                options.headers = {};
            options.headers['content-length'] = requestOptions.dataToPost.length;
        }
        var notified = false;
        var libToCall = options.protocol == "https:" ? https : http;
        var resp = null;
        var s2sRequest = libToCall.request(options, function (s2sResponse) {
            var readData = "";
            resp = s2sResponse;
            s2sResponse.setEncoding('utf8');
            s2sResponse.on('data', function (data) {
                readData += data;
            });
            s2sResponse.on('error', function (e) {
                if (!notified) {
                    notified = true;
                    onCallEnd(new Error(e.message), s2sResponse.statusCode, s2sResponse);
                }
            });
            s2sResponse.on('end', function () {
                debugger;
                if (!notified) {
                    notified = true;
                    var toNotify = readData;
                    try {
                        toNotify = JSON.parse(readData);
                    }
                    catch (e) {
                    }
                    onCallEnd(toNotify, s2sResponse.statusCode, s2sResponse);
                }
            });
        });
        s2sRequest.on('error', function (e) {
            if (!notified) {
                notified = true;
                onCallEnd(new Error(e.message), resp === null || resp === void 0 ? void 0 : resp.statusCode, resp);
            }
        });
        s2sRequest.on('timeout', function () {
            if (!notified) {
                notified = true;
                onCallEnd(new Error("TIMEOUT"), resp === null || resp === void 0 ? void 0 : resp.statusCode, resp);
            }
        });
        if (__.IsNotEmptyString(requestOptions.dataToPost))
            s2sRequest.write(requestOptions.dataToPost);
        s2sRequest.end();
    }
    IronLibsNodeJs.S2SCall = S2SCall;
    var configurationManagerInstances = {};
    /**
     * Represent a configuration file manager: permits to get/set its values and eventually read/save a JSON file
     */
    var ConfigurationManager = /** @class */ (function () {
        function ConfigurationManager(fileName, createIfNotFound, defaultConfiguration) {
            if (createIfNotFound === void 0) { createIfNotFound = true; }
            if (defaultConfiguration === void 0) { defaultConfiguration = null; }
            this.currentFileName = null;
            this.currentConfiguration = null;
            this.defaultConfiguration = null;
            var me = this;
            me.currentFileName = fileName;
            me.defaultConfiguration = __.EnsureNotNull(defaultConfiguration);
            me.currentConfiguration = me.defaultConfiguration;
            if (createIfNotFound && !fs.existsSync(fileName)) {
                me.SaveCurrentConfigurationToFile();
            }
            me.LoadCurrentConfigurationFromFile();
        }
        /**
         * Returns the current "in memory" configuration value
         * WARNING: the returned value is CLONED (so can't change it!)
         */
        ConfigurationManager.prototype.GetCurrentConfiguration = function () {
            return __.CloneObj(this.currentConfiguration);
        };
        /**
         * Updates the "in memory" configuration value
         * WARNING: doesn't write files automatically!
         *
         * @param newConfiguration
         */
        ConfigurationManager.prototype.SetCurrentConfiguration = function (newConfiguration) {
            this.currentConfiguration = __.CloneObj(newConfiguration);
        };
        ConfigurationManager.prototype.GetCurrentFileName = function () {
            return this.currentFileName;
        };
        /**
         * Updates the "in memory" configuration value reading it form file
         */
        ConfigurationManager.prototype.LoadCurrentConfigurationFromFile = function () {
            var json = fs.readFileSync(this.currentFileName, { encoding: 'utf8' });
            var read = JSON.parse(json);
            this.currentConfiguration = __.MergeObj(this.defaultConfiguration, read);
        };
        /**
         * Saves to the file the current "in memory" configuration value
         */
        ConfigurationManager.prototype.SaveCurrentConfigurationToFile = function () {
            fs.writeFileSync(this.currentFileName, JSON.stringify(this.currentConfiguration, null, 4), { encoding: 'utf8' });
        };
        /**
         * Sets a "default manager instance" for a particular "topic/argument".
         * Ex. use case:
         * At app start we create a manager instance and call SetManagerForTopic with "MainConf" topic.
         * All other modules needing it call ConfigurationManager.GetManagerForTopic to obtain it.
         */
        ConfigurationManager.SetManagerForTopic = function (topic, manager) {
            configurationManagerInstances[topic] = manager;
        };
        /**
         * Returns the "default manager instance" for a particular "topic/argument", if ever set
         */
        ConfigurationManager.GetManagerForTopic = function (topic) {
            return configurationManagerInstances[topic];
        };
        return ConfigurationManager;
    }());
    IronLibsNodeJs.ConfigurationManager = ConfigurationManager;
    var ServerResources;
    (function (ServerResources) {
        var currentLanguage = "en";
        var allResources = {};
        var availableLanguages = ["en", "it"];
        /**
         * Loads in memory EVERY available LANGUAGE from files.
         */
        function LoadResourcesFiles(rootPath, fileTemplate) {
            if (fileTemplate === void 0) { fileTemplate = "res_%s.json"; }
            var langs = GetAvailableLanguages();
            langs.forEach(function (language) {
                var fileName = path.resolve(rootPath, __.Format(fileTemplate, language));
                if (!fs.existsSync(fileName)) {
                    throw new Error(__.Format("Language \"%s\" not found (file \"%s\")", language, fileName));
                }
                var json = fs.readFileSync(fileName, { encoding: 'utf8' });
                allResources[language] = JSON.parse(json);
            });
            if (langs.length)
                SetCurrentLanguage(langs[0]); //initially a default one
        }
        ServerResources.LoadResourcesFiles = LoadResourcesFiles;
        /**
         * Sets the current language for the SERVER
         */
        function SetCurrentLanguage(defaultServerLanguageToSet) {
            if (!IsValidLanguage(defaultServerLanguageToSet))
                throw new Error(__.Format("Language \"%s\" not supported", defaultServerLanguageToSet));
            currentLanguage = defaultServerLanguageToSet;
        }
        ServerResources.SetCurrentLanguage = SetCurrentLanguage;
        /**
         * Gets the current language for the SERVER
         */
        function GetCurrentLanguage() {
            return currentLanguage;
        }
        ServerResources.GetCurrentLanguage = GetCurrentLanguage;
        /**
         * Returns a resources dictionary.
         * Returns the specified language (if supported) or the default one
         */
        function GetResources(forceLanguage) {
            if (forceLanguage === void 0) { forceLanguage = null; }
            if (__.IsEmptyString(forceLanguage))
                forceLanguage = currentLanguage;
            if (!IsValidLanguage(forceLanguage))
                throw new Error(__.Format("Language \"%s\" not supported", forceLanguage));
            return allResources[forceLanguage];
        }
        ServerResources.GetResources = GetResources;
        function IsValidLanguage(lang) {
            return __.SearchInArray(GetAvailableLanguages(), lang) >= 0;
        }
        ServerResources.IsValidLanguage = IsValidLanguage;
        function GetAvailableLanguages() {
            return __.CloneObj(availableLanguages);
        }
        ServerResources.GetAvailableLanguages = GetAvailableLanguages;
        function SetAvailableLanguages(languages) {
            availableLanguages = languages;
        }
        ServerResources.SetAvailableLanguages = SetAvailableLanguages;
    })(ServerResources = IronLibsNodeJs.ServerResources || (IronLibsNodeJs.ServerResources = {}));
})(IronLibsNodeJs || (IronLibsNodeJs = {}));
module.exports = IronLibsNodeJs;
