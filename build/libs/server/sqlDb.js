"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var IronLibs = require("../common");
var fs = require("fs");
var sqlite3 = require("sqlite3");
var __ = IronLibs.IronLibsCommon;
var IronLibsSqlDb;
(function (IronLibsSqlDb) {
    var DB_FIELD_TYPE;
    (function (DB_FIELD_TYPE) {
        DB_FIELD_TYPE[DB_FIELD_TYPE["TEXT"] = 0] = "TEXT";
        DB_FIELD_TYPE[DB_FIELD_TYPE["NUMBER_INTEGER"] = 1] = "NUMBER_INTEGER";
        DB_FIELD_TYPE[DB_FIELD_TYPE["NUMBER_DOUBLE"] = 2] = "NUMBER_DOUBLE";
        DB_FIELD_TYPE[DB_FIELD_TYPE["DATE"] = 3] = "DATE";
        DB_FIELD_TYPE[DB_FIELD_TYPE["BINARY"] = 4] = "BINARY";
    })(DB_FIELD_TYPE = IronLibsSqlDb.DB_FIELD_TYPE || (IronLibsSqlDb.DB_FIELD_TYPE = {}));
    var LOAD_DUMP_BEHAVIOUR;
    (function (LOAD_DUMP_BEHAVIOUR) {
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["INSERTBULK"] = 0] = "INSERTBULK";
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["UPDATEIFFOUND"] = 1] = "UPDATEIFFOUND";
        LOAD_DUMP_BEHAVIOUR[LOAD_DUMP_BEHAVIOUR["DELETEALLDATA"] = 2] = "DELETEALLDATA";
    })(LOAD_DUMP_BEHAVIOUR = IronLibsSqlDb.LOAD_DUMP_BEHAVIOUR || (IronLibsSqlDb.LOAD_DUMP_BEHAVIOUR = {}));
    var Entity = /** @class */ (function () {
        function Entity() {
        }
        Entity.prototype.SqlTableName = function () {
            return "Entity";
        };
        Entity.prototype.SqlTableFields = function () {
            return [];
        };
        Entity.prototype.SqlTableFieldNames = function () {
            return this.SqlTableFields().map(function (el) { return el.name; });
        };
        return Entity;
    }());
    IronLibsSqlDb.Entity = Entity;
    var IdEntity = /** @class */ (function (_super) {
        __extends(IdEntity, _super);
        function IdEntity() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        IdEntity.prototype.SqlTableName = function () {
            return "IdEntity";
        };
        IdEntity.prototype.SqlTableFields = function () {
            return [
                { name: "Id", type: DB_FIELD_TYPE.TEXT, primary: true },
            ];
        };
        return IdEntity;
    }(Entity));
    IronLibsSqlDb.IdEntity = IdEntity;
    var EntityCollection = /** @class */ (function () {
        function EntityCollection(tconstructor, fkName, values, isLoaded) {
            if (values === void 0) { values = []; }
            if (isLoaded === void 0) { isLoaded = false; }
            this.tconstructor = tconstructor;
            this.fkName = fkName;
            this.values = values;
            this.isLoaded = isLoaded;
            if (__.IsArray(values))
                this.values = values;
            else
                this.values = [];
        }
        EntityCollection.prototype.Values = function () {
            return this.values;
        };
        EntityCollection.prototype.IsLoaded = function () {
            return this.isLoaded;
        };
        EntityCollection.prototype.FkName = function () {
            return this.fkName;
        };
        EntityCollection.prototype.Tconstructor = function () {
            return this.tconstructor;
        };
        EntityCollection.prototype.LoadWithValues = function (values) {
            var me = this;
            me.values = [];
            values.forEach(function (el) {
                me.values.push(el);
            });
            me.isLoaded = true;
        };
        return EntityCollection;
    }());
    IronLibsSqlDb.EntityCollection = EntityCollection;
    /*#################################################################################################################*/
    /*#################################################################################################################*/
    var DB = /** @class */ (function () {
        /**
         *
         * @param options Sets the following possible options:
         *        fileName : Specifies the filename of the stored database file, or when inMemoryMode == true specifies the filename of the dump file.
         *        verbose : Specifies whether to log SQL statements or not.
         *        loggingFunction : Specifies the logging function accepting the string to log.
         *        inMemoryMode : If FALSE data is stored immediately (BAD queries performances, especially for non-SSD hard disks).
         *                       If TRUE data is kept in memory and persisted only in intervals of inMemoryDumpEveryMs milliseconds.
         *        inMemoryDumpEveryMs : Has sense only when inMemoryMode == true.
         *                              If > 0  data is persisted automatically as a dump every the specified interval in milliseconds.
         *                              If <= 0 data is NEVER persisted automatically.
         *        inMemoryDumpOnlyIfDirty : If TRUE the automatic dump is updated only if data has changed in any repository.
         *                                  If FALSE the dump is always updated.
         *        inMemoryRepositoriesToDump : The repositories to use to dump data. WARNING: only data of these repositories will be persisted automatically!
         */
        function DB(options) {
            if (options === void 0) { options = {}; }
            this.lastSuccessfulDump = null;
            this.automaticDumpTimer = null;
            this.automaticDumpPaused = false;
            this.automaticDumpIsProcessing = false;
            this.isClosed = false;
            var me = this;
            me.opt = __.MergeObj(DB.DefaultOptions, options);
            me.instanceId = __.Guid.New();
            //OPTIONS ERRORS CHECK
            if (me.opt.inMemoryMode && !__.IsArray(me.opt.inMemoryEntitiesToDump))
                throw new Error("When inMemoryMode == true then inMemoryRepositoriesToDump must be a valid array");
            if (me.opt.verbose && !__.IsFunction(me.opt.loggingFunction))
                throw new Error("When verbose == true then loggingFunction must be a valid function");
            me.db = new sqlite3.Database(me.opt.inMemoryMode ? ":memory:" : me.opt.fileName);
            //Start automatic dump?
            if (me.opt.inMemoryMode && me.opt.inMemoryDumpEveryMs > 0) {
                me.automaticDumpTimer = setInterval(function () {
                    if (me.automaticDumpPaused || me.automaticDumpIsProcessing)
                        return;
                    me.automaticDumpIsProcessing = true;
                    me.PerformDump(me.opt.fileName, me.opt.inMemoryEntitiesToDump, me.opt.inMemoryDumpOnlyIfDirty);
                }, me.opt.inMemoryDumpEveryMs);
                if (me.opt.verbose)
                    me.opt.loggingFunction("[DB " + me.opt.fileName + "] Started automatic dump every " + me.opt.inMemoryDumpEveryMs + "ms");
            }
        }
        DB.prototype.GetInstanceId = function () {
            return this.instanceId;
        };
        DB.prototype.GetIsVerbose = function () {
            return this.opt.verbose;
        };
        DB.prototype.GetInMemoryEntitiesToDump = function () {
            return this.opt.inMemoryEntitiesToDump;
        };
        DB.prototype.GetLoggingFunction = function () {
            return this.opt.loggingFunction;
        };
        DB.prototype.GetFileName = function () {
            return this.opt.fileName;
        };
        DB.prototype.GetLastSuccessfulDump = function () {
            return this.lastSuccessfulDump;
        };
        DB.prototype.ExecuteSelect = function (sql, onEnd) {
            var me = this;
            var ret = [];
            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SELECT: " + sql);
            me.db.each(sql, function (err, row) {
                ret.push(row);
            }, function (err, count) {
                if (__.IsFunction(onEnd))
                    onEnd(ret, err);
            });
        };
        DB.prototype.ExecuteSql = function (sql, callback) {
            if (this.opt.verbose)
                this.opt.loggingFunction("[DB " + this.opt.fileName + "] Execute SQL: " + sql);
            this.db.run(sql, callback);
        };
        DB.prototype.Transaction = function (callback, serial) {
            if (serial === void 0) { serial = true; }
            if (serial)
                this.db.serialize(callback);
            else
                this.db.parallelize(callback);
        };
        /**
         * Pauses an already started automatic dump
         */
        DB.prototype.PauseAutomaticDump = function () {
            this.automaticDumpPaused = true;
        };
        /**
         * Pauses an already paused automatic dump
         */
        DB.prototype.RestoreAutomaticDump = function () {
            this.automaticDumpPaused = false;
        };
        /**
         * Executes a dump independently from the automatic system.
         *
         * @param fileName Specify the file to be saved. If NULL then options.fileName will be used
         * @param specificEntitiesToDump Specify the entities types to dump. If NULL then options.inMemoryEntitiesToDump will be used
         * @param onEnd Called with non-null "err" in case of error
         */
        DB.prototype.ForceDump = function (fileName, specificEntitiesToDump, onEnd) {
            if (!__.IsArray(specificEntitiesToDump))
                specificEntitiesToDump = this.opt.inMemoryEntitiesToDump;
            if (__.IsEmptyString(fileName))
                fileName = this.opt.fileName;
            this.PerformDump(fileName, specificEntitiesToDump, false, onEnd);
        };
        DB.prototype.LoadDump = function (dump, entitiesToLoad, behaviour, onEnd) {
            if (behaviour === void 0) { behaviour = LOAD_DUMP_BEHAVIOUR.INSERTBULK; }
            if (onEnd === void 0) { onEnd = null; }
            var me = this;
            if (__.IsNull(dump))
                dump = this.opt.fileName;
            if (!__.IsArray(entitiesToLoad))
                entitiesToLoad = this.opt.inMemoryEntitiesToDump;
            var handleData = function (data) {
                var restoreNextRepo = function (which, previousError) {
                    if (which == entitiesToLoad.length) {
                        //collection finished!
                        if (__.IsFunction(onEnd))
                            onEnd(previousError);
                        return;
                    }
                    var entityRepo = Repository.GetInstance(entitiesToLoad[which], me);
                    entityRepo.LoadDump(data, behaviour, function (err) {
                        //go on on errors...
                        restoreNextRepo((which + 1), previousError || err);
                    });
                };
                restoreNextRepo(0, null);
            };
            //need to read data from a file? If so let's do it ONLY ONCE, don't delegate each repository!
            if (__.IsNotEmptyString(dump)) {
                fs.readFile(dump, "utf-8", function (err, data) {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return;
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);
        };
        /**
         * Eventually CONVERTS the data to SQL-Style based on its TYPE
         */
        DB.FormatFieldValueToSql = function (data, type) {
            if (type == DB_FIELD_TYPE.DATE) {
                if (__.IsDate(data))
                    return data.getTime().toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.NUMBER_INTEGER || type == DB_FIELD_TYPE.NUMBER_DOUBLE) {
                if (__.IsNumber(data))
                    return data.toString();
                else
                    return "NULL";
            }
            else if (type == DB_FIELD_TYPE.TEXT) {
                if (__.IsString(data))
                    return '"' + data.replace(/"/g, "\"\"") + '"';
                else
                    return "NULL";
            }
            return "NULL";
        };
        /**
         * check if the file is already present, in this case load it and MERGE the content replacing the entities in specified dataToSave
         *
         */
        DB.SaveOrMergeDumpDataToFile = function (dataToSave, fileName, onEnd) {
            var writeFile = function (data) {
                //Save the dump file
                fs.writeFile(fileName, JSON.stringify(data), onEnd);
            };
            if (fs.existsSync(fileName)) {
                fs.readFile(fileName, "utf-8", function (err, foundData) {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on error
                    }
                    var foundDataObj = JSON.parse(foundData);
                    //REPLACE entities to dump keeping found data
                    dataToSave = __.MergeObj(foundDataObj, dataToSave);
                    writeFile(dataToSave);
                });
            }
            else
                writeFile(dataToSave);
        };
        DB.prototype.Close = function (callback) {
            var me = this;
            if (me.isClosed)
                return false;
            this.db.close(function (err) {
                me.isClosed = true;
                if (__.IsNotNull(me.automaticDumpTimer))
                    clearInterval(me.automaticDumpTimer);
                if (__.IsFunction(callback))
                    callback(err);
            });
            return true;
        };
        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/
        DB.prototype.PerformDump = function (fileToSave, entitiesToDump, dumpOnlyIfDirty, onEnd) {
            var me = this;
            if (me.opt.verbose)
                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Running automatic dump");
            var dataToSave = {};
            var reposToSetNotDirty = [];
            var dumpNextRepo = function (which) {
                if (which == entitiesToDump.length) {
                    //collection of repos finished!
                    //check if there's something to save
                    var toSaveCount = 0;
                    for (var k in dataToSave) {
                        if (!dataToSave.hasOwnProperty(k))
                            continue;
                        toSaveCount++;
                    }
                    if (toSaveCount == 0) {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] Nothing to save because nothing is dirty. Automatic dump completed!");
                        me.lastSuccessfulDump = new Date();
                        me.automaticDumpIsProcessing = false;
                        if (__.IsFunction(onEnd))
                            onEnd(null);
                        return;
                    }
                    //SAVE DATA!
                    DB.SaveOrMergeDumpDataToFile(dataToSave, fileToSave, function (writeErr) {
                        if (__.IsNull(writeErr)) {
                            if (me.opt.verbose)
                                me.opt.loggingFunction("[DB " + me.opt.fileName + "] Automatic dump completed!");
                            me.lastSuccessfulDump = new Date();
                            me.automaticDumpIsProcessing = false;
                            reposToSetNotDirty.forEach(function (repoToSetNotDirty) {
                                repoToSetNotDirty.SetIsDirty(false);
                            });
                        }
                        else if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR loading found dump before save: " + JSON.stringify(writeErr));
                        if (__.IsFunction(onEnd)) {
                            onEnd(writeErr);
                        }
                    });
                    return;
                }
                var entityRepo = Repository.GetInstance(entitiesToDump[which], me);
                var entityName = Repository.GetEntityName(entitiesToDump[which]);
                //check if must skip this repo because it's not dirty
                if (dumpOnlyIfDirty == true && entityRepo.GetIsDirty() == false) {
                    if (me.opt.verbose)
                        me.opt.loggingFunction("[DB " + me.opt.fileName + "] Skipping dump for not dirty repository \"" + entityName + "\".");
                    dumpNextRepo(which + 1);
                    return;
                }
                reposToSetNotDirty.push(entityRepo);
                entityRepo.GetAll(function (data, err) {
                    if (__.IsNotNull(err)) {
                        if (me.opt.verbose)
                            me.opt.loggingFunction("[DB " + me.opt.fileName + "] ERROR during automatic dump!");
                        me.automaticDumpIsProcessing = false;
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on first error
                    }
                    dataToSave[entityName] = data; //keep data to be saved
                    dumpNextRepo(which + 1);
                });
            };
            dumpNextRepo(0);
        };
        DB.DefaultOptions = {
            fileName: "database.sqlite3",
            verbose: false,
            loggingFunction: function (msg) { __.Log(msg); },
            inMemoryMode: true,
            inMemoryDumpEveryMs: 2000,
            inMemoryDumpOnlyIfDirty: true,
            inMemoryEntitiesToDump: []
        };
        return DB;
    }());
    IronLibsSqlDb.DB = DB;
    /*#################################################################################################################*/
    /*#################################################################################################################*/
    var Repository = /** @class */ (function () {
        /**
         * DON'T USE directly THIS!
         *
         * @param Tconstructor
         * @param db
         */
        function Repository(Tconstructor, db) {
            this.Tconstructor = Tconstructor;
            this.db = db;
            this.isDirty = false;
            var me = this;
            var cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNotNull(Repository.reposInstancesCache[cacheKey]))
                throw new Error("Error: Repository instantiation failed: use Repository.GetInstance() instead of new.");
            else
                Repository.reposInstancesCache[cacheKey] = this;
            me.instanceId = __.Guid.New();
            var t = new Tconstructor();
            me.SqlTableName = t.SqlTableName();
            me.SqlTableFields = t.SqlTableFields();
        }
        Repository.GetInstance = function (Tconstructor, db) {
            var cacheKey = Repository.GetCacheKey(Tconstructor, db);
            if (__.IsNull(Repository.reposInstancesCache[cacheKey]))
                Repository.reposInstancesCache[cacheKey] = new Repository(Tconstructor, db);
            return (Repository.reposInstancesCache[cacheKey]);
        };
        Repository.GetCacheKey = function (Tconstructor, db) {
            return Repository.GetEntityName(Tconstructor) + "_" + db.GetInstanceId();
        };
        Repository.GetEntityName = function (typeConstructor) {
            var txt = typeConstructor.toString();
            var startPos = txt.indexOf(" ") + 1;
            return txt.substr(startPos, txt.indexOf("(") - startPos);
        };
        Repository.prototype.GetIsDirty = function () {
            return this.isDirty;
        };
        Repository.prototype.SetIsDirty = function (dirtyness) {
            this.isDirty = dirtyness;
        };
        /**
         * Ensures that the data table for the repository type is present
         * @param forceRecreate If TRUE all found table data will be erased
         * @param onEnd
         */
        Repository.prototype.Init = function (forceRecreate, onEnd) {
            if (forceRecreate === void 0) { forceRecreate = false; }
            var me = this;
            if (forceRecreate == true)
                me.RecreateTable(onEnd);
            else
                me.db.ExecuteSql(me.GetSqlCreateTable(true), onEnd);
        };
        /**
         * Destroys if exists and recreates the empty table.
         * Useful to DELETE ALL ITS DATA.
         */
        Repository.prototype.RecreateTable = function (onEnd) {
            var me = this;
            this.db.Transaction(function () {
                me.db.ExecuteSql("DROP TABLE IF EXISTS " + me.SqlTableName, function (err) {
                    if (__.IsNotNull(err)) {
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                    }
                    else {
                        me.isDirty = true;
                        me.db.ExecuteSql(me.GetSqlCreateTable(false), onEnd);
                    }
                });
            });
        };
        Repository.prototype.LoadDump = function (dump, behaviour, onEnd) {
            if (behaviour === void 0) { behaviour = LOAD_DUMP_BEHAVIOUR.INSERTBULK; }
            if (onEnd === void 0) { onEnd = null; }
            var me = this;
            var myTypeName = Repository.GetEntityName(me.Tconstructor);
            if (__.IsNull(dump))
                dump = me.db.GetFileName();
            if (me.db.GetIsVerbose())
                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Loading dump for type \"" + myTypeName + "\"");
            var handleData = function (data) {
                //ok, here we have the dump data, try to cast to my type
                var castedData = data[myTypeName].map(function (record) {
                    return me.ConvertObjectToType(record);
                });
                var insertData = function () {
                    if (behaviour == LOAD_DUMP_BEHAVIOUR.UPDATEIFFOUND)
                        me.SaveAll(castedData, function (result, err) {
                            if (me.db.GetIsVerbose()) {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded checking to update!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                    else
                        me.InsertBulk(castedData, function (err) {
                            if (me.db.GetIsVerbose()) {
                                if (__.IsNull(err))
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] Dump loaded with InsertBulk!");
                                else
                                    me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            }
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                        });
                };
                //now check if need to delete previous data
                if (behaviour == LOAD_DUMP_BEHAVIOUR.DELETEALLDATA) {
                    me.RecreateTable(function (err) {
                        if (__.IsNotNull(err)) {
                            if (me.db.GetIsVerbose())
                                me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                            if (__.IsFunction(onEnd))
                                onEnd(err);
                            return; //STOP on error
                        }
                        insertData();
                    });
                }
                else
                    insertData();
            };
            //need to read data from a file?
            if (__.IsNotEmptyString(dump)) {
                fs.readFile(dump, "utf-8", function (err, data) {
                    if (__.IsNotNull(err)) {
                        if (me.db.GetIsVerbose())
                            me.db.GetLoggingFunction()("[DB " + me.db.GetFileName() + "] ERROR loading dump: " + JSON.stringify(err));
                        if (__.IsFunction(onEnd))
                            onEnd(err);
                        return; //STOP on error
                    }
                    handleData(JSON.parse(data));
                });
            }
            else
                handleData(dump);
        };
        /**
         * Checks if an Entity exists in this repository (search by PrimaryKey).
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with "result" containing the check, or with non-null "err" in case of error
         */
        Repository.prototype.Exists = function (pk, onEnd) {
            var me = this;
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            var sql = "select count(*) from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";
            me.db.ExecuteSelect(sql, function (data, err) {
                var result = false;
                if (__.IsNull(err)) {
                    result = data[0]["count(*)"] == 1;
                }
                if (__.IsFunction(onEnd))
                    onEnd(result, err);
            });
        };
        /**
         * Deletes the passed entity (search by PrimaryKey) IF IT EXISTS in this repository.
         *
         * @param pk  an Entity PrimaryKey
         * @param onEnd Called with non-null "err" in case of error
         */
        Repository.prototype.Delete = function (pk, onEnd) {
            var me = this;
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            var sql = "DELETE from " + me.SqlTableName + " where [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type) + ";";
            me.db.ExecuteSql(sql, function (err) {
                if (__.IsNull(err))
                    me.SetIsDirty(true);
                onEnd(err);
            });
        };
        Repository.prototype.GetCount = function (onEnd, whereClause) {
            var me = this;
            var sql = "SELECT count(*) from " + me.SqlTableName;
            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;
            me.db.ExecuteSelect(sql, function (countData, err) {
                var count = -1;
                if (__.IsNull(err))
                    count = countData[0]["count(*)"];
                if (__.IsFunction(onEnd))
                    onEnd(count, err);
            });
        };
        /**
         * Returns ALL entities in this repository.
         *
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         */
        Repository.prototype.GetAll = function (onEnd, collectionsToFill) {
            if (collectionsToFill === void 0) { collectionsToFill = null; }
            this.GetWhere("", onEnd, collectionsToFill);
        };
        /**
         * Returns ALL entities in this repository satisfying the passed clause
         *
         * @param whereClause The SQL where clause
         * @param onEnd Called with "data" containing the entities, or with non-null "err" in case of error
         * @param collectionsToFill Specify which collections (properties of the retrieved entities) to retrieve
         * @param onlySpecifiedFields
         */
        Repository.prototype.GetWhere = function (whereClause, onEnd, collectionsToFill, onlySpecifiedFields) {
            if (collectionsToFill === void 0) { collectionsToFill = null; }
            if (onlySpecifiedFields === void 0) { onlySpecifiedFields = null; }
            var me = this;
            if (__.IsNull(collectionsToFill))
                collectionsToFill = [];
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            var sql = "SELECT ";
            if (__.IsNotEmptyArray(onlySpecifiedFields))
                sql += onlySpecifiedFields.map(function (x) { return "[" + x + "]"; }).join(",");
            else
                sql += "*";
            sql += " from " + me.SqlTableName;
            if (__.IsNotEmptyString(whereClause))
                sql += " WHERE " + whereClause;
            me.db.ExecuteSelect(sql, function (data, err) {
                var mainResult = null;
                if (__.IsNull(err)) {
                    mainResult = data.map(function (dbRow) {
                        return me.ConvertObjectToType(dbRow);
                    });
                }
                var asyncFunctions = [];
                //now check for COLLECTIONS to be retrieved
                if (__.IsArray(mainResult)) {
                    mainResult.forEach(function (record) {
                        collectionsToFill.forEach(function (collectionFieldName) {
                            var collection = (record[collectionFieldName]);
                            var repo = Repository.GetInstance(collection.Tconstructor(), me.db);
                            asyncFunctions.push(function (notifyEnd) {
                                //Get the elements of the collection
                                //TODO: here it's possible to select some nested collections...
                                repo.GetWhere(collection.FkName() + " = " + DB.FormatFieldValueToSql(record[pkInfo.name], pkInfo.type), function (data, err) {
                                    if (__.IsNull(err)) {
                                        //replace the collection with the retrieved values
                                        collection.LoadWithValues(data);
                                    }
                                    notifyEnd(err); //return the error if present
                                });
                            });
                        });
                    });
                }
                //the JOINING function
                asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                    var errToReturn = Repository.GetFirstNotNullError(err, resultsOrderedByTime);
                    onEnd(mainResult, errToReturn); //pass the FIRST error received
                });
                __.ExecuteAndJoin(asyncFunctions);
            });
        };
        /**
         * Returns the entity with the specified PrimaryKey in this repository.
         *
         * @param pk
         * @param onEnd Called with "data" containing the entity if found or NULL if not found,
         * or with non-null "err" in case of error
         * @param collectionsToFill
         */
        Repository.prototype.GetByPk = function (pk, onEnd, collectionsToFill) {
            if (collectionsToFill === void 0) { collectionsToFill = null; }
            var me = this;
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            this.GetWhere("[" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(pk, pkInfo.type), function (data, err) {
                var result = null;
                if (__.IsNull(err) && data.length > 0) {
                    result = data[0];
                }
                if (__.IsFunction(onEnd))
                    onEnd(result, err);
            }, collectionsToFill);
        };
        /**
         * Returns the entities with the specified PrimaryKeys in this repository.
         *
         * @param pks
         * @param onEnd Called with "data" containing the entities found, or with non-null "err" in case of error
         * @param collectionsToFill Specifies which collections (properties of the retrieved entities) to retrieve
         */
        Repository.prototype.GetByPks = function (pks, onEnd, collectionsToFill) {
            if (collectionsToFill === void 0) { collectionsToFill = null; }
            var me = this;
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(null, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            var idsList = pks.map(function (pk) { return DB.FormatFieldValueToSql(pk, pkInfo.type); });
            this.GetWhere("[" + pkInfo.name + "] in (" + idsList.join(",") + ")", onEnd, collectionsToFill);
        };
        /**
         * Executes an OPTIIMIZED bulk INSERT for all passed entities which MUST BE NEW.
         * WARNING: if any of them already exist then an error will be returned and NO entity will be inserted!
         *
         * @param items The NEW entities
         * @param onEnd Called with non-null "err" in case of error
         */
        Repository.prototype.InsertBulk = function (items, onEnd) {
            var me = this;
            var sql = "INSERT INTO " + me.SqlTableName + "(";
            me.SqlTableFields.forEach(function (field, i) {
                sql += "[" + field.name + "]";
                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });
            sql += ") VALUES (";
            items.forEach(function (data, dataIdx) {
                sql += me.GetSqlEntityValues(data) + ")";
                if (dataIdx < items.length - 1)
                    sql += ",(";
            });
            me.db.ExecuteSql(sql, function (err) {
                if (__.IsNull(err))
                    me.SetIsDirty(true);
                if (__.IsFunction(onEnd))
                    onEnd(err);
            });
        };
        /**
         * Persists the passed entities.
         * Expects a Not-Null items, otherwise the callback will be called with a specific error.
         * A CHECK is made whether each item needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since (2*items.length) queries are performed.
         *
         * @param items The entities to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE for each item, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entities) to save
         */
        Repository.prototype.SaveAll = function (items, onEnd, collectionsToSave) {
            if (collectionsToSave === void 0) { collectionsToSave = null; }
            var me = this;
            var asyncFunctions = [];
            var resultsCreated = [];
            items.forEach(function (item, itemIdx) {
                asyncFunctions.push(function (notifyEnd) {
                    //Save ALL the elements in ANY order
                    me.Save(item, function (created, err) {
                        resultsCreated[itemIdx] = created; //store the "created" information at the right position
                        notifyEnd(err); //return the error if present
                    }, collectionsToSave);
                });
            });
            //the JOINING function
            asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                if (__.IsFunction(onEnd)) {
                    var errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                    onEnd(resultsCreated, errToReturn); //pass the FIRST error received and the "created" array
                }
            });
            __.ExecuteAndJoin(asyncFunctions);
        };
        /**
         * Persists the passed entity.
         * Expects a Not-Null obj, otherwise the callback will be called with a specific error.
         * A CHECK is made whether it needs to be INSERTED or UPDATED.
         * WARNING: not so performing... since 2 queries are performed.
         *
         * @param obj The entity to be persisted
         * @param onEnd  Called with "created" == true if INSERT, == false if UPDATE, or with non-null "err" in case of error
         * @param collectionsToSave Specifies which collections (properties of the saved entity) to save
         */
        Repository.prototype.Save = function (obj, onEnd, collectionsToSave) {
            if (collectionsToSave === void 0) { collectionsToSave = null; }
            var me = this;
            if (__.IsNull(collectionsToSave))
                collectionsToSave = [];
            if (!__.IsFunction(onEnd))
                onEnd = function () { };
            if (__.IsNull(obj)) {
                setTimeout(function () {
                    onEnd(false, new Error("NULL obj passed to Repository.Save"));
                }, 1);
                return;
            }
            var pkInfo = me.SqlTableFields.filter(function (x) { return x.primary; })[0];
            if (__.IsNull(pkInfo)) {
                setTimeout(function () {
                    onEnd(false, new Error("Entity has no PrimaryKey"));
                }, 1);
                return;
            }
            //must CHECK if needs to be INSERTED or UPDATED
            me.Exists(obj[pkInfo.name], function (exists, err) {
                if (__.IsNotNull(err)) {
                    onEnd(false, err);
                    return;
                }
                /**
                 * Internal function which fires async save of all collectionsToSave
                 * @param finalCreated Specifies what to pass to onEnd
                 */
                var SaveAllCollections = function (finalCreated) {
                    var asyncFunctions = [];
                    collectionsToSave.forEach(function (collectionFieldName) {
                        var collection = (obj[collectionFieldName]);
                        var repo = Repository.GetInstance(collection.Tconstructor(), me.db);
                        var subitemsToSave = obj[collectionFieldName].Values();
                        subitemsToSave.forEach(function (el) { el[collection.FkName()] = obj[pkInfo.name]; }); //link all items in the collection to their parent
                        asyncFunctions.push(function (notifyEnd) {
                            repo.SaveAll(subitemsToSave, function (created, err2) {
                                notifyEnd(err2);
                            });
                        });
                    });
                    //the JOINING function
                    asyncFunctions.push(function (resultsOrderedByPosition, resultsOrderedByTime) {
                        var errToReturn = Repository.GetFirstNotNullError(null, resultsOrderedByTime);
                        onEnd(finalCreated, errToReturn); //pass the FIRST error received and the "created" array
                    });
                    __.ExecuteAndJoin(asyncFunctions);
                };
                if (exists) {
                    //create the sql statement
                    var sql_1 = "UPDATE " + me.SqlTableName + " set ";
                    me.SqlTableFields.forEach(function (field, i) {
                        sql_1 += "[" + field.name + "] = " + DB.FormatFieldValueToSql(obj[field.name], field.type);
                        if (i < me.SqlTableFields.length - 1)
                            sql_1 += ",";
                    });
                    sql_1 += " WHERE [" + pkInfo.name + "] = " + DB.FormatFieldValueToSql(obj[pkInfo.name], pkInfo.type) + ";";
                    me.db.ExecuteSql(sql_1, function (err) {
                        if (__.IsNotNull(err)) {
                            onEnd(false, err); // "created" == false, eventual error passed straight away
                            return;
                        }
                        me.SetIsDirty(true);
                        if (collectionsToSave.length == 0) {
                            onEnd(false, err); // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(false); //save was OK, go on with specified collections
                    });
                }
                else {
                    me.InsertBulk([obj], function (err) {
                        if (__.IsNotNull(err) || collectionsToSave.length == 0) {
                            if (__.IsFunction(onEnd))
                                onEnd(true, err); // "created" == false, eventual error passed straight away
                        }
                        else
                            SaveAllCollections(true); //save was OK, go on with specified collections
                    });
                }
            });
        };
        /*##########################################################################
         #########################   PROTECTED STUFF   #########################
         ##########################################################################*/
        /**
         * Returns the first NON null error from the one passed and eventual errors returned by the async functions whose results have been passed
         */
        Repository.GetFirstNotNullError = function (err, results) {
            if (__.IsNotNull(err))
                return err;
            if (__.IsArray(results)) {
                for (var i = 0; i < results.length; i++) {
                    if (__.IsNotNull(results[i].SuccessfulResult)) //this query returned an error
                        return results[i].SuccessfulResult;
                    if (__.IsNotNull(results[i].Exception)) //this query threw an exception
                        return results[i].Exception;
                }
            }
            return null;
        };
        /**
         * Tries to convert a PLAIN JavaScript object to a T-Typed instance
         */
        Repository.prototype.ConvertObjectToType = function (obj) {
            var me = this;
            //CONVERT to the specified type
            var objT = new me.Tconstructor();
            me.SqlTableFields.forEach(function (field) {
                //Eventually CONVERT the data to be written
                if (field.type == DB_FIELD_TYPE.DATE && __.IsNotNull(obj[field.name]))
                    objT[field.name] = new Date(obj[field.name]);
                else
                    objT[field.name] = obj[field.name];
            });
            return objT;
        };
        /**
         * Returns the fields SQL-VALUES of the given record
         */
        Repository.prototype.GetSqlEntityValues = function (data) {
            var me = this;
            var sql = "";
            me.SqlTableFields.forEach(function (field, i) {
                sql += DB.FormatFieldValueToSql(data[field.name], field.type);
                if (i < me.SqlTableFields.length - 1)
                    sql += ",";
            });
            return sql;
        };
        Repository.prototype.GetSqlCreateTable = function (ifNotExist) {
            var ret = "CREATE TABLE" +
                (ifNotExist ? " IF NOT EXISTS " : " ") +
                this.SqlTableName +
                "(";
            var fields = this.SqlTableFields;
            var primaryKeyName = null;
            fields.forEach(function (f, i) {
                ret += "'" + f.name + "' ";
                if (f.type == DB_FIELD_TYPE.TEXT)
                    ret += "TEXT";
                else if (f.type == DB_FIELD_TYPE.NUMBER_INTEGER)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.NUMBER_DOUBLE)
                    ret += "DOUBLE";
                else if (f.type == DB_FIELD_TYPE.DATE)
                    ret += "INTEGER";
                else if (f.type == DB_FIELD_TYPE.BINARY)
                    ret += "BINARY";
                if (f.primary == true) {
                    ret += " PRIMARY KEY UNIQUE NOT NULL";
                    primaryKeyName = f.name;
                }
                if (i < fields.length - 1)
                    ret += ", ";
            });
            ret += ");";
            if (__.IsNotEmptyString(primaryKeyName))
                ret += " CREATE INDEX " + this.SqlTableName + "_PK on " + this.SqlTableName + "(" + primaryKeyName + ");";
            return ret;
        };
        Repository.reposInstancesCache = {};
        return Repository;
    }());
    IronLibsSqlDb.Repository = Repository;
})(IronLibsSqlDb || (IronLibsSqlDb = {}));
module.exports = IronLibsSqlDb;
