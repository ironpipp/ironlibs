"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsCommon = void 0;
var window;
var isNode = window == null;
var uuid;
var s;
if (isNode) {
    uuid = require('node-uuid');
    s = require("sprintf-js").sprintf;
}
else {
    s = window["sprintf"];
}
//import server-client SHARED modules
if (!isNode)
    window["require"] = function () { return window["EventsManager"]; }; //MODULES HACK
var events = require("./eventsManager");
var logEmitter = null;
if (isNode) {
    var ServerLogEmitter = /** @class */ (function (_super) {
        __extends(ServerLogEmitter, _super);
        function ServerLogEmitter() {
            return _super.call(this, "ServerLogEmitter") || this;
        }
        ServerLogEmitter.prototype.NotifyLog = function (msg) {
            this.NotifyEvent("ServerLog-NewLog", msg);
        };
        ServerLogEmitter.prototype.NotifyFatalError = function (msg) {
            this.NotifyEvent("Server-FatalError", msg);
        };
        return ServerLogEmitter;
    }(events.EventNotifier));
    logEmitter = new ServerLogEmitter();
}
var IronLibsCommon;
(function (IronLibsCommon) {
    var IronLibsPlugin;
    (function (IronLibsPlugin) {
        IronLibsPlugin[IronLibsPlugin["nodeJs"] = 0] = "nodeJs";
        IronLibsPlugin[IronLibsPlugin["raspiPin"] = 1] = "raspiPin";
        IronLibsPlugin[IronLibsPlugin["bus"] = 2] = "bus";
        IronLibsPlugin[IronLibsPlugin["puppeteer"] = 3] = "puppeteer";
        IronLibsPlugin[IronLibsPlugin["sqlDb"] = 4] = "sqlDb";
        IronLibsPlugin[IronLibsPlugin["expressMVC"] = 5] = "expressMVC";
        IronLibsPlugin[IronLibsPlugin["store"] = 6] = "store";
        IronLibsPlugin[IronLibsPlugin["ui"] = 7] = "ui";
        IronLibsPlugin[IronLibsPlugin["net"] = 8] = "net";
    })(IronLibsPlugin = IronLibsCommon.IronLibsPlugin || (IronLibsCommon.IronLibsPlugin = {}));
    function LoadPlugin(lib) {
        if (!isNode)
            return;
        if (lib == IronLibsPlugin.nodeJs)
            IronLibsCommon.Node = require("./server/nodejs");
        else if (lib == IronLibsPlugin.raspiPin)
            IronLibsCommon.RaspiPin = require("./raspi/raspiPin");
        else if (lib == IronLibsPlugin.bus)
            IronLibsCommon.Bus = require("./bus");
        else if (lib == IronLibsPlugin.sqlDb)
            IronLibsCommon.SqlDb = require("./server/sqlDb");
        else if (lib == IronLibsPlugin.expressMVC)
            IronLibsCommon.MVC = require("./server/expressMVC");
        else if (lib == IronLibsPlugin.puppeteer)
            IronLibsCommon.Pupp = require("./server/puppeteer");
        else if (lib == IronLibsPlugin.store)
            IronLibsCommon.Store = require("./client/store");
        else if (lib == IronLibsPlugin.ui)
            IronLibsCommon.UI = require("./client/ui");
        else if (lib == IronLibsPlugin.net)
            IronLibsCommon.Net = require("./client/net");
        __.Log("Loaded IronLibs Plugin '" + IronLibsPlugin[lib] + "'");
    }
    IronLibsCommon.LoadPlugin = LoadPlugin;
    /*##########################################################################
     #########################   VERY GENERIC METHODS   #########################
     #####################   which don't need a namespace  ######################
     ##########################################################################*/
    var __ = IronLibsCommon;
    var currentLanguage = null;
    var isDebug = false;
    /**
     * Changes the actual DEBUG MODE
     *
     * @method SetDebugMode
     * @param val The value to set
     */
    function SetDebugMode(val) {
        isDebug = val;
    }
    IronLibsCommon.SetDebugMode = SetDebugMode;
    /**
     * Returns TRUE if application is configured to be in DEBUG MODE
     *
     * @method IsDebugMode
     * @return {boolean}
     */
    function IsDebugMode() {
        return isDebug;
    }
    IronLibsCommon.IsDebugMode = IsDebugMode;
    /**
     * log to console one or MORE passed arguments
     */
    function Log() {
        //F.P. unfortunately we can't use console.log.apply, so switch on arguments length
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        var when = __.Dates.FormatDate(new Date());
        if (arguments.length == 1)
            console.log(when, arguments[0]);
        else if (arguments.length == 2)
            console.log(when, arguments[0], arguments[1]);
        else if (arguments.length == 3)
            console.log(when, arguments[0], arguments[1], arguments[2]);
        else if (arguments.length == 4)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3]);
        else if (arguments.length == 5)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
        else if (arguments.length == 6)
            console.log(when, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        else
            console.log(when, arguments);
        if (logEmitter != null)
            logEmitter.NotifyLog(arguments);
    }
    IronLibsCommon.Log = Log;
    function LogFatal() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        Log.apply(this, ["## FATAL ## "].concat(params));
        if (logEmitter != null)
            logEmitter.NotifyFatalError(params);
    }
    IronLibsCommon.LogFatal = LogFatal;
    function LogError() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        Log.apply(this, ["## ERROR ## "].concat(params));
        if (logEmitter != null)
            logEmitter.NotifyFatalError(params);
    }
    IronLibsCommon.LogError = LogError;
    function LogWarn() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        Log.apply(this, ["## WARN  ## "].concat(params));
    }
    IronLibsCommon.LogWarn = LogWarn;
    function LogDebug() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        Log.apply(this, ["-- DEBUG -- "].concat(params));
    }
    IronLibsCommon.LogDebug = LogDebug;
    /**
     * Formats a string with N parameters of any type.
     * Wrapper of sprintf
     *
     * @param formatString
     * @param params
     * @returns {string} the formatted string
     */
    function Format(formatString) {
        var params = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            params[_i - 1] = arguments[_i];
        }
        return s.apply(this, arguments);
    }
    IronLibsCommon.Format = Format;
    /**
     * Returns the most suitable size format for the given number
     *
     * @method FormatFileSize
     * @param {number|string} fileSize  The number of BYTES for format
     * @param {boolean} shortForm  If TRUE returns the 2-characters representation
     * @return {string}
     */
    function FormatFileSize(fileSize, shortForm) {
        if (IsString(fileSize))
            fileSize = parseFloat(fileSize);
        if (isNaN(fileSize))
            return "";
        var formatString = "%.2f %s";
        if (fileSize < 1024)
            return fileSize + (shortForm == true ? " B" : " Bytes");
        else if (fileSize < 1024 * 1024)
            return Format(formatString, fileSize / 1024, shortForm == true ? "KB" : "KBytes");
        else if (fileSize < 1024 * 1024 * 1024)
            return Format(formatString, fileSize / (1024 * 1024), shortForm == true ? "MB" : "MBytes");
        else
            return Format(formatString, fileSize / (1024 * 1024 * 1024), shortForm == true ? "GB" : "GBytes");
    }
    IronLibsCommon.FormatFileSize = FormatFileSize;
    /**
     * Returns TRUE if obj is NULL or UNDEFINED
     *
     * @method IsNull
     * @param obj What to check
     * @return {boolean}
     */
    function IsNull(obj) {
        return (obj === undefined || obj === null);
    }
    IronLibsCommon.IsNull = IsNull;
    /**
     * Returns TRUE if obj isn't NULL or UNDEFINED
     *
     * @method IsNotNull
     * @param obj What to check
     * @return {boolean}
     */
    function IsNotNull(obj) {
        return (obj !== undefined && obj !== null);
    }
    IronLibsCommon.IsNotNull = IsNotNull;
    /**
     * Returns TRUE if obj IS an object and isn't NULL or UNDEFINED
     *
     * @method IsNotNullObject
     * @param obj What to check
     * @return {boolean}
     */
    function IsNotNullObject(obj) {
        return obj !== undefined && obj !== null && typeof (obj) === "object";
    }
    IronLibsCommon.IsNotNullObject = IsNotNullObject;
    /**
     * Returns TRUE if obj is a FUNCTION
     *
     * @method IsFunction
     * @param obj What to check
     * @return {boolean}
     */
    function IsFunction(obj) {
        return typeof (obj) === "function";
    }
    IronLibsCommon.IsFunction = IsFunction;
    /**
     * Returns TRUE if obj is a NUMBER
     *
     * @method IsNumber
     * @param obj What to check
     * @return {boolean}
     */
    function IsNumber(obj) {
        return typeof obj === "number" && isNaN(obj) == false;
    }
    IronLibsCommon.IsNumber = IsNumber;
    /**
     * Returns TRUE if obj is a Date instance
     *
     * @method IsDate
     * @param obj What to check
     * @return {boolean}
     */
    function IsDate(obj) {
        return (IsNotNull(obj) && (obj instanceof Date) && __.IsNumber(obj.getDate()));
    }
    IronLibsCommon.IsDate = IsDate;
    var RoundingMethod;
    (function (RoundingMethod) {
        RoundingMethod[RoundingMethod["FLOOR"] = 0] = "FLOOR";
        RoundingMethod[RoundingMethod["ROUND"] = 1] = "ROUND";
        RoundingMethod[RoundingMethod["NO_ROUNDING"] = 2] = "NO_ROUNDING";
    })(RoundingMethod = IronLibsCommon.RoundingMethod || (IronLibsCommon.RoundingMethod = {}));
    /**
     * Returns a number with precision clamped to a fixed resolution
     * Ex: LimitNumberPrecision(1.12345678, 3) == 1.123
     *
     * ATTENTION: JavaScript is tricky/buggy! Here some examples:
     *
     * 2.05*100 == 204.99999999999997  not 205  so
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.FLOOR) == 2.04
     *
     * LimitNumberPrecision(2.05, 2, __.RoundingMethod.NO_ROUNDING) == 2.05
     *
     * @param num The float number to limit
     * @param precision the number of DIGITS after "."
     * @param method The method used to "cut" digits: Math.round, Math.floor or basic extra digits removal with no rounding
     *
     */
    function LimitNumberPrecision(num, precision, method) {
        if (method === void 0) { method = RoundingMethod.ROUND; }
        if (!__.IsNumber(num))
            return num;
        var multiplier = Math.pow(10, precision);
        if (method == RoundingMethod.ROUND)
            return Math.round((num * multiplier)) / multiplier;
        else if (method == RoundingMethod.FLOOR)
            return Math.floor((num * multiplier)) / multiplier;
        else {
            var str = num.toString();
            var dotPos = str.indexOf(".");
            if (dotPos < 0)
                return num;
            str = str.substr(0, dotPos + precision + 1);
            return parseFloat(str);
        }
        //https://www.jacklmoore.com/notes/rounding-in-javascript/
        //return Number(Math.floor(<any>(num + 'e' + precision)) + 'e-' + precision);
    }
    IronLibsCommon.LimitNumberPrecision = LimitNumberPrecision;
    /**
     * Returns TRUE if obj is a native BOOLEAN instance
     *
     * @method IsBoolean
     * @param obj What to check
     * @return {boolean}
     */
    function IsBoolean(obj) {
        return typeof obj === "boolean";
    }
    IronLibsCommon.IsBoolean = IsBoolean;
    /**
     * Returns TRUE if obj is a NOT-NULL NUMBER, BOOLEAN, DATE or STRING
     *
     * @method IsNativeType
     * @param obj What to check
     * @return {boolean}
     */
    function IsNativeType(obj) {
        return IsNumber(obj) || IsBoolean(obj) || IsString(obj) || IsDate(obj);
    }
    IronLibsCommon.IsNativeType = IsNativeType;
    /**
     * Returns TRUE if obj is an ARRAY
     *
     * @method IsArray
     * @param obj What to check
     * @return {boolean}
     */
    function IsArray(obj) {
        return IsNotNull(obj) && obj instanceof Array;
    }
    IronLibsCommon.IsArray = IsArray;
    /**
     * Returns TRUE if obj is a STRING
     *
     * @method IsString
     * @param obj What to check
     * @return {boolean}
     */
    function IsString(obj) {
        return typeof obj == "string";
    }
    IronLibsCommon.IsString = IsString;
    /**
     * Returns TRUE if obj is NULL or an EMPTY STRING
     *
     * @method IsEmptyString
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmptyString(obj) {
        return IsNull(obj) || obj === "";
    }
    IronLibsCommon.IsEmptyString = IsEmptyString;
    /**
     * Returns TRUE if obj is NULL or an EMPTY OBJECT (without any property)
     *
     * @method IsEmptyObject
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmptyObject(obj) {
        if (IsNull(obj))
            return true;
        if (IsNativeType(obj))
            return false;
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            return false;
        }
        return true;
    }
    IronLibsCommon.IsEmptyObject = IsEmptyObject;
    /**
     * Returns TRUE if obj is a STRING and is not empty
     * (WARNING: can have only empty spaces! To check them use IsNotWhiteSpace)
     *
     * @param {string|*} obj
     * @return {boolean}
     */
    function IsNotEmptyString(obj) {
        return IsString(obj) && obj.length > 0;
    }
    IronLibsCommon.IsNotEmptyString = IsNotEmptyString;
    /**
     * Returns TRUE if obj is NULL, an EMPTY STRING or an EMPTY ARRAY
     *
     * @method IsNullOrEmpty
     * @param obj What to check
     * @return {boolean}
     */
    function IsNullOrEmpty(obj) {
        return (IsEmptyString(obj)) || (IsArray(obj) && obj.length == 0);
    }
    IronLibsCommon.IsNullOrEmpty = IsNullOrEmpty;
    /**
     * Returns TRUE if obj is an ARRAY AND has at least one element
     * @return {boolean}
     */
    function IsNotEmptyArray(obj) {
        return IsArray(obj) && obj.length > 0;
    }
    IronLibsCommon.IsNotEmptyArray = IsNotEmptyArray;
    /**
     * Returns TRUE if the passed object is a jquery-wrapped object.
     * (automatic check of NOT-NULL)
     *
     * @method IsJquery
     * @param obj What to check
     * @return {boolean}
     */
    function IsJquery(obj) {
        return window != null && obj instanceof window["$"];
    }
    IronLibsCommon.IsJquery = IsJquery;
    /**
     * Returns TRUE if the passed object is a string representing a valid EMAIL address.
     * (automatic check of NOT-NULL)
     *
     * @method IsEmail
     * @param obj What to check
     * @return {boolean}
     */
    function IsEmail(obj) {
        //F.P. This is the default jQuery validate implementation BUT is TOO RELAXED (es: accepts "asd@asd")  (from https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address )
        //return __.IsString(obj) && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( obj );
        //prefer this (from https://emailregex.com/ )
        return __.IsString(obj) && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(obj);
    }
    IronLibsCommon.IsEmail = IsEmail;
    var PLUS_ONLY = /\+.*$/;
    var PLUS_AND_DOT = /\.|\+.*$/g;
    var normalizableProviders = {
        'gmail.com': {
            'cut': PLUS_AND_DOT
        },
        'googlemail.com': {
            'cut': PLUS_AND_DOT,
            'aliasOf': 'gmail.com'
        },
        'hotmail.com': {
            'cut': PLUS_ONLY
        },
        'live.com': {
            'cut': PLUS_AND_DOT
        },
        'outlook.com': {
            'cut': PLUS_ONLY
        }
    };
    /**
     * Returns the passed email address:
     * - lower case domain (RFC 1035 "Name servers and resolvers must compare [domains] in a case-insensitive manner")
     * and for some domains (which consider the emails equivalent):
     * - without dots
     * - without substring after "+"
     *
     * @method NormalizeEmail
     */
    function NormalizeEmail(eMail) {
        if (!IsString(eMail)) {
            return eMail; //original passed object
        }
        var email = eMail.toString(); //clone it
        var emailParts = email.split(/@/);
        if (emailParts.length !== 2) {
            return eMail; //original passed object
        }
        var username = emailParts[0];
        var domain = emailParts[1].toLowerCase();
        if (normalizableProviders.hasOwnProperty(domain)) {
            if (normalizableProviders[domain].hasOwnProperty('cut')) {
                username = username.replace(normalizableProviders[domain].cut, '');
            }
            if (normalizableProviders[domain].hasOwnProperty('aliasOf')) {
                domain = normalizableProviders[domain].aliasOf;
            }
        }
        return username + '@' + domain; //USERNAME eventually sanitized, DOMAIN forced LOWER CASE
    }
    IronLibsCommon.NormalizeEmail = NormalizeEmail;
    /**
     * Returns ALWAYS a string: the representation of the passed object:
     * EMPTY string upon NULL, or toString() upon OBJECT
     *
     * @param {*} str
     * @param {string} defaultValue
     * @return {string}
     */
    function EnsureString(str, defaultValue) {
        if (defaultValue === void 0) { defaultValue = ""; }
        if (IsNull(str))
            return defaultValue;
        else if (IsString(str))
            return str;
        return str.toString();
    }
    IronLibsCommon.EnsureString = EnsureString;
    /**
     * Returns ALWAYS something NOT NULL (which can be specified as defaultValue).
     * Similar to {{#crossLink "__/EnsureString"}}{{/crossLink}} but for objects.
     *
     * @method EnsureNotNull
     * @param obj Anything to be checked
     * @param {string} defaultValue What to return in case str is NULL. Defaults to {}
     * @return {object}
     */
    function EnsureNotNull(obj, defaultValue) {
        if (defaultValue === void 0) { defaultValue = {}; }
        if (__.IsNull(obj))
            return defaultValue;
        return obj;
    }
    IronLibsCommon.EnsureNotNull = EnsureNotNull;
    /**
     * If "str" ends with "endsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringEndsWith
     * @param str The string to check
     * @param endsWith The ending to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    function StringEndsWith(str, endsWith, caseSensitive) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        str = EnsureString(str);
        endsWith = EnsureString(endsWith);
        if (!caseSensitive) {
            str = str.toLowerCase();
            endsWith = endsWith.toLowerCase();
        }
        var ret = str;
        var pos = ret.lastIndexOf(endsWith);
        return !(pos < 0 || pos != ret.length - endsWith.length);
    }
    IronLibsCommon.StringEndsWith = StringEndsWith;
    /**
     * Returns always a NEW string ending with "endsWith" parameter.
     * If the passed "str" wasn't already ending with "endsWith", this will be appended.
     * Parameters are always string-ensured.
     *
     * @method EnsureEndsWith
     * @param str
     * @param endsWith
     * @param caseSensitive
     */
    function EnsureEndsWith(str, endsWith, caseSensitive) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        str = EnsureString(str);
        endsWith = EnsureString(endsWith);
        if (StringEndsWith(str, endsWith, caseSensitive))
            return str;
        return str + endsWith;
    }
    IronLibsCommon.EnsureEndsWith = EnsureEndsWith;
    /**
     * If "str" starts with "startsWith" returns TRUE, FALSE otherwise.
     * Parameters are always string-ensured.
     *
     * @method StringStartsWith
     * @param str The string to check
     * @param startsWith The starting string to check
     * @param caseSensitive CASE check, defaults to TRUE
     */
    function StringStartsWith(str, startsWith, caseSensitive) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        str = EnsureString(str);
        startsWith = EnsureString(startsWith);
        if (!caseSensitive) {
            str = str.toLowerCase();
            startsWith = startsWith.toLowerCase();
        }
        var ret = str;
        var pos = ret.indexOf(startsWith);
        return pos == 0;
    }
    IronLibsCommon.StringStartsWith = StringStartsWith;
    /**
     * Returns always a NEW string starting with "startsWith" parameter.
     * If the passed "str" wasn't already starting with "startsWith", this will be prepended.
     * Parameters are always string-ensured.
     *
     * @method EnsureStartsWith
     * @param str
     * @param startsWith
     * @param caseSensitive
     */
    function EnsureStartsWith(str, startsWith, caseSensitive) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        str = EnsureString(str);
        startsWith = EnsureString(startsWith);
        if (StringStartsWith(str, startsWith, caseSensitive))
            return str;
        return startsWith + str;
    }
    IronLibsCommon.EnsureStartsWith = EnsureStartsWith;
    /**
     * Encodes the passed string to BASE64.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64EncodeUnicode
     */
    function Base64EncodeUnicode(str) {
        if (isNode) {
            return Buffer.from(str).toString('base64');
        }
        else {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
                return String.fromCharCode(parseFloat('0x' + p1));
            }));
        }
    }
    IronLibsCommon.Base64EncodeUnicode = Base64EncodeUnicode;
    /**
     * Decodes the passed BASE64 string.
     *
     * The "Unicode Problem"
     * Since DOMStrings are 16-bit-encoded strings, in most browsers calling window.btoa on a Unicode string will cause a Character Out Of Range exception if a character exceeds the range of a 8-bit byte (0x00~0xFF). There are two possible methods to solve this problem:
     * - the first one is to escape the whole string (with UTF-8, see encodeURIComponent) and then encode it;
     * - the second one is to convert the UTF-16 DOMString to an UTF-8 array of characters and then encode it.
     * This is the FIRST method implementation
     *
     * @method Base64DecodeUnicode
     */
    function Base64DecodeUnicode(str) {
        if (isNode) {
            return Buffer.from(str, 'base64').toString('utf-8');
        }
        else {
            // Going backwards: from bytestream, to percent-encoding, to original string.
            return decodeURIComponent(atob(str).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        }
    }
    IronLibsCommon.Base64DecodeUnicode = Base64DecodeUnicode;
    /**
     * Base type to create a new JS ENUM structure (useful to convert its values numeric <=> string   and for FLAGS)
     *
     * @class EnumBaseType
     */
    var EnumBaseType = /** @class */ (function () {
        /**
         * Creates a new JS ENUM structure specifying its values and whether it's a FLAG
         *
         * ex:
         *      let MY_ENUM = new EnumBaseType();
         *      MY_ENUM["Prop1"] = 1;
         *      MY_ENUM["Prop2"] = 2;
         *      console.log(MY_ENUM.GetStringValue(2));     //logs "Prop2"
         *
         * or:
         *      let MY_ENUM = new EnumBaseType({Prop1 : 1, Prop2 : 2}, true);
         *      console.log(MY_ENUM.GetStringValue(3));     //logs "Prop1,Prop2"
         *
         * @method constructor
         * @param {object} values An Object with the definition of the ENUM values as key-value
         * @param {boolean} [EnumIsFlag] Indicates if this type is a FLAG (OR-able values). Default = FALSE
         */
        function EnumBaseType(values, EnumIsFlag) {
            //TODO: with TypeScript >= 2 this property can be specified as READONLY
            if (values === void 0) { values = null; }
            if (EnumIsFlag === void 0) { EnumIsFlag = false; }
            this.EnumIsFlag = EnumIsFlag;
            if (__.IsNotNull(values)) {
                for (var p in values) {
                    if (!values.hasOwnProperty(p))
                        continue;
                    this[p] = values[p];
                }
            }
        }
        /**
         * Returns the string (or LIST of strings separated by "," if EnumIsFlag == true) represented by the passed numeric value
         *
         * @method GetStringValue
         * @param {number} numericEnumValue
         * @returns {string}
         */
        EnumBaseType.prototype.GetStringValue = function (numericEnumValue) {
            var ret = "";
            var me = this;
            for (var key in me) {
                if (!me.hasOwnProperty(key) || !__.IsNumber(me[key]))
                    continue;
                if (this.EnumIsFlag) {
                    if (me[key] == 0 || (numericEnumValue & me[key]))
                        ret = ret + (ret == "" ? "" : ",") + key;
                }
                else {
                    if (numericEnumValue == me[key])
                        return key;
                }
            }
            return ret;
        };
        /**
         * Given a FLAG number and its ENUM definition returns an array of strings containing the
         * values represented by the flag number.
         * Useful for Kendo MVVM checkboxes
         *
         * @param flag The number (ex. 129)
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} ex. ["1", "128"]
         */
        EnumBaseType.ConvertFlagToArray = function (flag, enumType) {
            var ret = [];
            var flagStringValues = enumType.GetStringValue(flag);
            flagStringValues.split(",").forEach(function (checked) {
                var intValue = enumType[checked];
                if (__.IsNotNull(intValue))
                    ret.push(intValue.toString());
            });
            return ret;
        };
        /**
         * Given an array of strings containing the values represented by the flag
         * returns the computed flag number value (OR)
         * Useful for Kendo MVVM checkboxes
         *
         * @param flags The array of string values (ex. ["1", "128"] )
         * @param enumType the EnumBaseType of the passed flag
         * @return {Array} The computed flag number (ex. 129)
         */
        EnumBaseType.ConvertArrayToFlag = function (flags, enumType) {
            var ret = 0;
            for (var enumTxt in enumType) {
                if (!enumType.hasOwnProperty(enumTxt) || !__.IsNumber(enumType[enumTxt]))
                    continue;
                if (__.SearchInArray(flags, enumType[enumTxt].toString()) >= 0)
                    ret = ret | enumType[enumTxt];
            }
            return ret;
        };
        return EnumBaseType;
    }());
    IronLibsCommon.EnumBaseType = EnumBaseType;
    var Guid;
    (function (Guid) {
        function New() {
            return uuid.v4().toUpperCase();
        }
        Guid.New = New;
        /**
         * Returns a NEW GUID without dashes
         *
         * @method NewShort
         * @return {string}
         */
        function NewShort() {
            return New().replace(/-/gi, "");
        }
        Guid.NewShort = NewShort;
        function Empty() {
            return "00000000-0000-0000-0000-000000000000";
        }
        Guid.Empty = Empty;
        /**
         * Returns the short EMPTY GUID (without dashes)
         *
         * @method EmptyShort
         * @return {string}
         */
        function EmptyShort() {
            return "00000000000000000000000000000000";
        }
        Guid.EmptyShort = EmptyShort;
        /**
         * Checks if the value is a string that contain a valid NON-EMPTY Guid (NORMAL or SHORT)
         * NO case-sensitive!
         *
         * @param value {string} the string to check
         * @method IsReady
         * @return {boolean}
         */
        function IsReady(value) {
            return (IsGuid(value) && value != Empty()) || (IsShortGuid(value) && value != EmptyShort());
        }
        Guid.IsReady = IsReady;
        /**
         * Returns TRUE if obj SEEMS to be a GUID
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        function IsGuid(obj) {
            return IsString(obj) &&
                obj.length == 36 &&
                obj.replace(/\s/g, "").length == 36 && //with NO spaces or NEW LINES
                obj.replace(/-/g, "").length == 32; //with 4 dashes
        }
        Guid.IsGuid = IsGuid;
        /**
         * Returns TRUE if obj SEEMS to be a SHORT GUID (without dashes)
         *
         * @method IsGuid
         * @param obj What to check
         * @return {boolean}
         */
        function IsShortGuid(obj) {
            return IsString(obj) &&
                obj.length == 32 &&
                obj.replace(/\s/g, "").length == 32; //with NO spaces or NEW LINES
        }
        Guid.IsShortGuid = IsShortGuid;
        /**
         * Checks if 2 Guids are equal (NORMAL or SHORT, case insensitive)
         *
         * @method Equals
         * @return {boolean}
         */
        function Equals(value1, value2) {
            return IsNotEmptyString(value1) && IsNotEmptyString(value2) && value1.toLowerCase() == value2.toLowerCase();
        }
        Guid.Equals = Equals;
    })(Guid = IronLibsCommon.Guid || (IronLibsCommon.Guid = {}));
    /**
     * Following the current culture format (GetCurrentLanguage()) returns a NUMBER object or NULL if the string could not be parsed.
     *
     * @method ParseNumber
     * @param {string} str The string to parse
     * @return {number}
     */
    function ParseNumber(str) {
        return parseFloat(str);
    }
    IronLibsCommon.ParseNumber = ParseNumber;
    /**
     * Returns TRUE if the passed string represents the "true" boolean value (case INSENSITIVE check), FALSE otherwise
     *
     * @method ParseBoolean
     * @param {string} str The string to parse
     * @return {boolean}
     */
    function ParseBoolean(str) {
        str = EnsureString(str, "false").trim().toLowerCase();
        return str == "true";
    }
    IronLibsCommon.ParseBoolean = ParseBoolean;
    /**
     * Returns TRUE if the passed string is EMPTY or is an URL ( starts with http(s):// or ftp(s):// )
     *
     * @method StringIsUrl
     * @param {string} str The string to check
     * @return {boolean}
     */
    function StringIsUrl(str) {
        return IsEmptyString(str) || (/^(https?|ftps?):\/\//gi.test(str));
    }
    IronLibsCommon.StringIsUrl = StringIsUrl;
    function UrlToObject(url) {
        var urlParams = {};
        var e, a = /\+/g, // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g, d = function (s) { return decodeURIComponent(s.replace(a, " ")); }, q = IsNull(url) || typeof url != "string" ? window.location.search.substring(1) : url;
        while (e = r.exec(q))
            urlParams[d(e[1])] = d(e[2]);
        return urlParams;
    }
    IronLibsCommon.UrlToObject = UrlToObject;
    /**
     * Returns the CURRENT LANGUAGE
     *
     * @method GetCurrentLanguage
     * @return {string}
     */
    function GetCurrentLanguage() {
        if (!IsNullOrEmpty(currentLanguage)) {
            return currentLanguage;
        }
        //never set with SetCurrentLanguage. Use KENDO if available
        if (IsNotNull(window["kendo"]) && IsFunction(window["kendo"].culture)) //must be set BEFORE document.ready
            return window["kendo"].culture().name;
        //never set with SetCurrentLanguage and KENDO NOT available. Try reading the DOM
        if (IsNotNull(window) && IsNotNull(window["$"]))
            return (window["$"]("html")).attr("lang");
        return null;
    }
    IronLibsCommon.GetCurrentLanguage = GetCurrentLanguage;
    /**
     * Changes the CURRENT LANGUAGE
     *
     * @method SetCurrentLanguage
     * @param val The value to set
     */
    function SetCurrentLanguage(val) {
        currentLanguage = val;
        if (isNode)
            return;
        //update Kendo culture (if possible)
        if (IsNotNull(window["kendo"]) && IsFunction(window["kendo"].culture)) //must be set BEFORE document.ready
            window["kendo"].culture(val);
        //update the DOM (if possible)
        if (IsNotNull(window) && IsNotNull(window["$"]))
            (window["$"]("html")).attr("lang", val);
    }
    IronLibsCommon.SetCurrentLanguage = SetCurrentLanguage;
    /**
     * Given a LocalizedStringModel returns the localized value for a specific language
     * (or the CURRENT one if not specified)
     *
     * @method GetLocalizedValue
     * @return {string}
     */
    function GetLocalizedValue(localizedStringModel, language) {
        var ret = "";
        //no language specified? Defaults to CURRENT one
        if (IsEmptyString(language))
            language = GetCurrentLanguage().toLowerCase();
        else
            language = language.toLowerCase();
        //wrong object passed?
        if (IsNull(localizedStringModel) || IsNull(localizedStringModel.Values))
            return ret;
        //search the value for the specified language DISCARDING language CASE
        for (var k in localizedStringModel.Values) {
            if (!localizedStringModel.Values.hasOwnProperty(k))
                continue;
            if (k.toLowerCase() == language) {
                ret = localizedStringModel.Values[k];
                break;
            }
        }
        return ret;
    }
    IronLibsCommon.GetLocalizedValue = GetLocalizedValue;
    /**
     * Given an array of objects returns an object with all grouped keys mapping to the array of related items.
     *
     * @param arr {array} The array to cycle
     * @param key {string} The property to use to group items
     * @param keysComparer {Function} If specified, a custom key comparer (ex: case inensitive match?)
     * @return object
     * @method GroupBy<T>
     */
    function GroupBy(arr, key, keysComparer) {
        if (!__.IsFunction(keysComparer))
            keysComparer = function (a, b) { return a == b; };
        var ret = {};
        arr.reduce(function (lastRet, currVal, currIdx) {
            if (__.IsNull(currVal))
                return lastRet;
            var currKey = __.EnsureString(currVal[key]);
            var keyAlreadyPresent = false;
            for (var presentKey in lastRet) {
                if (!lastRet.hasOwnProperty(presentKey))
                    continue;
                if (keysComparer(presentKey, currKey)) {
                    keyAlreadyPresent = true;
                    currKey = presentKey; //the comparer says they're equal, BUT COULD NOT BE! Use the firstly added
                    break;
                }
            }
            if (keyAlreadyPresent) {
                lastRet[currKey].push(currVal); //this key has already been added: ADD this item
            }
            else {
                lastRet[currKey] = [currVal]; //this key has never been added: create the array with this item
            }
            return lastRet;
        }, ret);
        return ret;
    }
    IronLibsCommon.GroupBy = GroupBy;
    /**
     * Generates a dummy integer HASH code representing the passed string using a dummy custom hashing function
     *
     * @param toHash {string} The string to be hashed
     * @return number
     * @method HashCode
     */
    function HashCode(toHash) {
        var hash = 0, i, chr, len;
        if (!__.IsString(toHash) || __.IsEmptyString(toHash))
            return hash;
        for (i = 0, len = toHash.length; i < len; i++) {
            chr = toHash.charCodeAt(i);
            hash = ((hash << 5) - hash) + (chr << 11);
        }
        return hash | 0; // Convert to 32bit integer;
    }
    IronLibsCommon.HashCode = HashCode;
    /**
     * Returns an empty ID for a new "person" in BLOX URN format
     *
     * @return {string}
     */
    // export function EmptyUrn() : string
    // {
    //     return "URN:BLOX:empty" + $.Guid.Empty();
    // }
    /**
     * Adds "search" function to standard Array object.
     * Does the same than "indexOf", but this is not supported by IE < 9
     * Can search anything: strings, objects, numbers, nulls, ....
     *
     * @method SearchInArray<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {T} toSearch              [OBJ, FUN] The object to search throughout the array
     *                                  or a callback function receiving the element and returning a boolean value stating if the search should terminate
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @return {number} The POSITION index of the searched element
     */
    function SearchInArray(Arr, toSearch, caseSensitive, from) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        if (from === void 0) { from = 0; }
        var len = Arr.length;
        from = Number(from) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;
        if (typeof toSearch == "function") {
            //ToSearch is a NOT NULL Callback
            for (; from < len; from++) {
                if (from in Arr &&
                    toSearch(Arr[from]))
                    return from;
            }
        }
        else if (typeof toSearch == "string") {
            //ToSearch is a NOT NULL STRING
            if (caseSensitive) {
                //NORMAL STRING comparison
                for (; from < len; from++) {
                    if (from in Arr &&
                        Arr[from] == toSearch)
                        return from;
                }
            }
            else {
                //CASE INSENSITIVE COMPARISON
                var ToSearchLower = EnsureString(toSearch).toString().toLowerCase();
                for (; from < len; from++) {
                    if (from in Arr &&
                        Arr[from] != null &&
                        EnsureString(Arr[from]).toString().toLowerCase() == ToSearchLower)
                        return from;
                }
            }
        }
        else {
            //NORMAL OBJECT (null?) COMPARISON
            for (; from < len; from++) {
                if (from in Arr && Arr[from] == toSearch)
                    return from;
            }
        }
        return -1;
    }
    IronLibsCommon.SearchInArray = SearchInArray;
    /**
     * Same as SearchInArray, but returns the found ITEM (not its position in array) if found, or NULL otherwise
     *
     * @method SearchValInArray<T>
     */
    function SearchValInArray(Arr, toSearch, caseSensitive, from) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        if (from === void 0) { from = 0; }
        var pos = SearchInArray(Arr, toSearch, caseSensitive, from);
        if (pos < 0)
            return null;
        return Arr[pos];
    }
    IronLibsCommon.SearchValInArray = SearchValInArray;
    /**
     * Searches the passed array and returns the FIRST ITEM with the specified "Id" field, or "undefined" if not found.
     * Similar to {{#crossLink "__/SearchInArray<T>"}}{{/crossLink}}
     *
     * @method SearchById<T>
     *
     * @param {Array} Arr               The array to search into
     * @param {string} id               The Id value to search
     * @param {boolean} [caseSensitive] (defaults to true): if true AND ToSearch is a string then checks case
     * @param {number} [from]           (defaults to 0): the starting index of the search in the array
     *
     * @returns {T} The FIRST item with the specified "Id" field
     */
    function SearchById(Arr, id, caseSensitive, from) {
        if (caseSensitive === void 0) { caseSensitive = true; }
        if (from === void 0) { from = 0; }
        if (!caseSensitive)
            id = id.toLowerCase();
        var pos = SearchInArray(Arr, function (el) {
            if (IsNull(el) || !IsString(el.Id))
                return false;
            if (caseSensitive)
                return el.Id == id;
            else
                return el.Id.toLowerCase() == id;
        }, true, from);
        return Arr[pos];
    }
    IronLibsCommon.SearchById = SearchById;
    /**
     * Searches the passed array. If the element is found REMOVES it.
     *
     * @param {Array} array The array to search and modify
     * @param {object|function} element The element to search. If is a function it must return bool (TRUE when found)
     * @return {boolean} TRUE if found and removed, FALSE otherwise
     */
    function RemoveFromArray(array, element) {
        var pos = SearchInArray(array, element, false, 0);
        if (pos < 0)
            return false;
        array.splice(pos, 1);
        return true;
    }
    IronLibsCommon.RemoveFromArray = RemoveFromArray;
    /**
     * Removes the element at position "idx" of the passed "array"
     */
    function RemoveFromArrayByIdx(array, idx) {
        array.splice(idx, 1);
    }
    IronLibsCommon.RemoveFromArrayByIdx = RemoveFromArrayByIdx;
    /**
     * Removes all elements in the passed array satisfying the passed condition
     *
     * @method RemoveFromArrayByCondition
     * @param {Array} array The array to alter
     * @param {function} condition The function checking if the element satisfies the condition for it to be removed from the array
     * @returns {number} the removed items count
     */
    function RemoveFromArrayByCondition(array, condition) {
        var ret = 0;
        if (!__.IsFunction(condition))
            return ret;
        for (var i = 0; i < array.length; i++) {
            if (condition(array[i])) {
                array.splice(i, 1);
                i--;
                ret++;
            }
        }
        return ret;
    }
    IronLibsCommon.RemoveFromArrayByCondition = RemoveFromArrayByCondition;
    /**
     * Insert an element into an array at the specified position.
     *
     * @method InsertInArray<T>
     * @param {Array} array The array to alter
     * @param {object|function} element The element to insert
     * @param {number} position The 0-based position where to insert the passed element
     * @return {boolean} TRUE if insertion succeeded, FALSE otherwise
     */
    function InsertInArray(array, element, position) {
        if (!__.IsArray(array) || position < 0 || position > array.length)
            return false;
        array.splice(position, 0, element);
        return true;
    }
    IronLibsCommon.InsertInArray = InsertInArray;
    /*
     * Return a random string of characters
     *
     * @param {Integer} stringLength    Number of random characters the returned string should contain. Defaults to 16.
     * @param {String}  chars           Available characters to use in a string. Defaults to [A-Za-z0-9]
     * @returns {String}                Generated random string
     *
     */
    function RandomString(stringLength, chars) {
        var newString = "";
        if (!chars) {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789";
        }
        if (!stringLength) {
            stringLength = 16;
        }
        for (var i = 0; i < stringLength; i = i + 1) {
            var randomNumber = Math.floor(Math.random() * chars.length);
            newString += chars.substring(randomNumber, randomNumber + 1);
        }
        return newString;
    }
    IronLibsCommon.RandomString = RandomString;
    /**
     * DEEP CLONING of a "thing" which can be a NATIVE type or an object.
     * The ONLY thing which IS NOT cloned (but copied by reference) are DOM NODES
     *
     * KNOWN ISSUE: objects are cloned calling their constructor WITHOUT ANY PARAMETER!!!!
     *
     * @method CloneObj<T>
     * @param {T} obj The "thing" to clone
     * @return {T} A perfect COPY of the passed "thing"
     */
    function CloneObj(obj) {
        var i, ret, ret2;
        if (typeof obj === "object") {
            if (obj === null)
                return obj;
            var constructorType = Object.prototype.toString.call(obj);
            if (constructorType === "[object Array]") {
                ret = [];
                for (i = 0; i < obj.length; i++) {
                    if (typeof obj[i] === "object") {
                        ret2 = CloneObj(obj[i]);
                    }
                    else {
                        ret2 = obj[i];
                    }
                    ret.push(ret2);
                }
            }
            else if (constructorType === "[object Date]") {
                ret = new Date(obj.getTime());
            }
            else if (constructorType.substr(0, 12) === "[object HTML") //F.P. DOM NODES
             {
                ret = obj;
            }
            else if (__.IsJquery(obj)) //F.P. JQuery objects
             {
                ret = $(obj);
            }
            else {
                if (typeof obj.constructor == "function") {
                    try {
                        ret = new obj.constructor(); //F.P. The ONLY problem here: called with NO PARAMETERS.....
                    }
                    catch (e) {
                        ret = {};
                    }
                }
                else
                    ret = {};
                for (i in obj) {
                    if (obj.hasOwnProperty(i)) {
                        if (typeof (obj[i] === "object")) {
                            ret2 = CloneObj(obj[i]);
                        }
                        else {
                            ret2 = obj[i];
                        }
                        ret[i] = ret2;
                    }
                }
            }
        }
        else {
            ret = obj;
        }
        return ret;
    }
    IronLibsCommon.CloneObj = CloneObj;
    /**
     *  Calls a passed callback for each NESTED property of the passed object
     *
     * @method CycleObjectNestedProperties
     * @param obj The object to traverse
     * @param onProp Callback to be called on each property found. Receives the property path, value and parent object. If returns FALSE the traversing is stopped
     * @param callbackOnNestedObjectsToo If TRUE "onProp" get fired also for properties holding nested objects
     */
    function CycleObjectNestedProperties(obj, onProp, callbackOnNestedObjectsToo) {
        if (callbackOnNestedObjectsToo === void 0) { callbackOnNestedObjectsToo = false; }
        /**
         * Returns FALSE when needed to BLOCK cycling, true otherwise
         */
        function CycleFlatProps(prefix, obj) {
            if (!__.IsNotNullObject(obj))
                return true;
            var usedPrefix = __.IsEmptyString(prefix) ? "" : prefix + ".";
            for (var k in obj) {
                if (!obj.hasOwnProperty(k))
                    continue;
                var val = obj[k];
                if (__.IsNotNullObject(val) && !__.IsNativeType(val)) {
                    if (callbackOnNestedObjectsToo) {
                        var userRet = onProp(usedPrefix + k, val, obj);
                        if (userRet === false)
                            return false;
                    }
                    var recursiveRet = CycleFlatProps(usedPrefix + k, val);
                    if (recursiveRet === false)
                        return false;
                }
                else {
                    var userRet = onProp(usedPrefix + k, val, obj);
                    if (userRet === false)
                        return false;
                }
            }
            return true;
        }
        CycleFlatProps("", obj);
    }
    IronLibsCommon.CycleObjectNestedProperties = CycleObjectNestedProperties;
    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     * IF MORE than 2 arguments are passed all RIGHT ones are merged sequentially in the LEFT ones
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @method MergeAnyObj
     * @param params The objects to be merged
     * @returns {object} The merged result
     */
    function MergeAnyObj() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        //CASE with a lot of arguments to be merged
        if (arguments.length >= 2) {
            var prevObj = arguments[0];
            for (var i = 1; i < arguments.length; i++) {
                prevObj = MergeObj(prevObj, arguments[i]);
            }
            return prevObj;
        }
    }
    IronLibsCommon.MergeAnyObj = MergeAnyObj;
    /**
     * Returns a NEW object with all properties of EXTENSION and the ones in BASE which are not present in EXTENSION.
     * NULL values in extension are not ignored but COPIED (overwrite value in base object), UNDEFINED ones are IGNORED!
     * Nested properties are copied with the same logic
     *
     * Ex.          MergeObj(A, B, C, D, E)
     * Expands to:  let AB = MergeObj(A, B)
     *              let ABC = MergeObj(AB, C)
     *              let ABCD = MergeObj(ABC, D)
     *              return MergeObj(ABCD, E)
     *
     * @returns {object} The merged result
     *
     * @see MergeAnyObj
     * @method MergeObj
     * @param base The BASE object to be extended
     * @param extension The extension to be added to BASE
     */
    function MergeObj(base, extension) {
        var ret = CloneObj(base);
        for (var k in extension) {
            if (!extension.hasOwnProperty(k) ||
                Object.prototype.toString.call(extension[k]) == "[object Undefined]") //IGNORE properties in extension with value set to UNDEFINED
                continue;
            if (Object.prototype.toString.call(ret[k]) == "[object Object]") {
                if (Object.prototype.toString.call(extension[k]) == "[object Object]")
                    ret[k] = MergeObj(ret[k], extension[k]); //OBJECT present in base? MERGE it too!
                else
                    ret[k] = CloneObj(extension[k]); //native type present in base? OVERWRITE it with an exact copy!
            }
            else
                ret[k] = CloneObj(extension[k]); //not present/NULL/or other native types? make an exact copy!
        }
        return ret;
    }
    IronLibsCommon.MergeObj = MergeObj;
    /**
     * Returns all the KEYS of the passed object as an ARRAY of strings, or NULL if the passed obj IS NOT an object
     *
     * @method ObjKeysToArray
     * @param obj
     * @return {null|string[]}
     */
    function ObjKeysToArray(obj) {
        if (!IsNotNullObject(obj))
            return null;
        var ret = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            ret.push(k);
        }
        return ret;
    }
    IronLibsCommon.ObjKeysToArray = ObjKeysToArray;
    /**
     * Returns an property (eventually nested) traversing the passed object.
     * Returns the passed object if NO path is given, UNDEFINED if not found.
     *
     * @method GetObjProperty
     * @param obj The object to be traversed. EX: {prop1 : {prop2 : 33}}
     * @param path The path to the requested property. EX: "prop1.prop2"
     */
    function GetObjProperty(obj, path) {
        if (!IsNotNullObject(obj))
            return null;
        var currObj = obj;
        path = __.EnsureString(path);
        if (__.IsEmptyString(path))
            return currObj;
        var segments = path.split(".");
        for (var i = 0; i < segments.length; i++) {
            currObj = currObj[segments[i]];
            if (__.IsNull(currObj))
                return currObj;
        }
        return currObj;
    }
    IronLibsCommon.GetObjProperty = GetObjProperty;
    /**
     * Wraps a function with a passed one. Features:
     *
     * - it's possible to run the passed "newFun" BEFORE or AFTER the original one
     * - the original context is preserved
     * - "newFun" will receive an additional parameter with infos and options:
     *   - "returnMyReturn" option: control which return object to be used
     *   - "proceed" option: control whether or not to call the original function
     *   - "argumentsToPass" option: customize the passed arguments to the original function
     *   - "originalReturn" option: the original returned object (if "before" == FALSE)
     *
     * @param newFun [function]
     *   The new function (will receive an additional parameter with infos and options)
     *
     * @param funName [string]
     *   The function name to wrap
     *
     * @param [funContext] [object]
     *   The context object holding the member "funName".
     *   It will be the "this" for both functions.
     *   Defaults to the global "window" object
     *
     * @param [before] [boolean]
     *   If TRUE "newFun" will be called before the original,
     *   otherwise after. Defaults to TRUE
     *
     * @param [callOnDynamicThis] [boolean]
     *   If TRUE "funContext" will be ignored as "this" and both functions
     *   will be run on the "run-time" object "this". Defaults to TRUE
     */
    function WrapFun(newFun, funName, funContext, before, callOnDynamicThis) {
        //default values
        if (IsNull(funContext))
            funContext = window;
        if (IsNull(before))
            before = true;
        if (IsNull(callOnDynamicThis))
            callOnDynamicThis = true;
        var oriFun = funContext[funName];
        funContext[funName] = function () {
            var wrapOpt = {
                before: before,
                proceed: false,
                argumentsToPass: [],
                returnMyReturn: true
            };
            var newRet, oriRet;
            if (callOnDynamicThis)
                funContext = this;
            //create a new list of arguments (Can't edit directly "arguments" obj..)
            var newArgs = [];
            for (var i = 0; i < arguments.length; i++)
                newArgs.push(arguments[i]);
            if (before) {
                wrapOpt["proceed"] = true;
                wrapOpt["argumentsToPass"] = newArgs.slice(); //clone it!
                wrapOpt["returnMyReturn"] = false;
                newArgs.push(wrapOpt);
                newRet = newFun.apply(funContext, newArgs); //call the NEW function with a new final editable Settings argument (WARNING: args can be edited!)
                if (!IsArray(wrapOpt["argumentsToPass"])) //could have been deleted...
                    wrapOpt["argumentsToPass"] = [];
                if (wrapOpt.proceed) {
                    oriRet = oriFun.apply(funContext, wrapOpt["argumentsToPass"]); //call the ORIGINAL function
                    if (wrapOpt.returnMyReturn)
                        return newRet;
                    else
                        return oriRet;
                }
                else {
                    if (wrapOpt.returnMyReturn)
                        return newRet;
                    else
                        return undefined;
                }
            }
            else {
                oriRet = oriFun.apply(funContext, newArgs); //call the ORIGINAL function (WARNING: args can be edited!)
                wrapOpt["originalReturn"] = oriRet;
                wrapOpt["returnMyReturn"] = true;
                newArgs.push(wrapOpt);
                newRet = newFun.apply(funContext, newArgs); //call the NEW function with a new final editable Settings argument (WARNING: args can be edited!)
                if (wrapOpt.returnMyReturn)
                    return newRet;
                else
                    return oriRet;
            }
        };
        funContext[funName]["originalFun"] = oriFun; //remember the original function to be able to UNWRAP it
    }
    IronLibsCommon.WrapFun = WrapFun;
    /**
     * Reverts to the original a wrapped function created with __.WrapFun(...)
     *
     * @param funName [string] The function name to restore
     * @param [funContext] [object] The context object holding the member "funName". Defaults to the global "window" object
     */
    function UnwrapFun(funName, funContext) {
        if (typeof funContext[funName]["originalFun"] == "function") {
            funContext[funName] = funContext[funName]["originalFun"];
        }
    }
    IronLibsCommon.UnwrapFun = UnwrapFun;
    /**
     * Converts an object to another following the passed rules.
     *
     * @method MapObject
     *
     * @param obj The object to convert
     *
     * @param {string[][]} mappings  The conversion rules.
     * It must be an array of 2 strings arrays.
     * The first states the SOURCE property to be copied, the second the DESTINATION property name.
     * Those properties can be NESTED. Ex: "subObject.subProp"
     *
     * @param {boolean} [ignoreMissingProperties = true] If TRUE when a source property is not found
     * then NULL will be returned (situation threaten as an error)
     *
     * @param {boolean} [cloneObjects = false] If TRUE the converted properties will be a CLONE of the source ones.
     * Otherwise a reference to the same object will be created.
     *
     * @return {Object|null} The converted object or NULL upon errors
     */
    function MapObject(obj, mappings, ignoreMissingProperties, cloneObjects) {
        //CHECK INPUT PARAMETERS
        if (IsNull(obj) || !IsArray(mappings))
            return null;
        if (IsNull(ignoreMissingProperties))
            ignoreMissingProperties = true;
        ignoreMissingProperties = !!ignoreMissingProperties;
        if (IsNull(cloneObjects))
            cloneObjects = false;
        cloneObjects = !!cloneObjects;
        var ret = {};
        for (var i = 0; i < mappings.length; i++) {
            var mapping = mappings[i];
            if (!IsArray(mapping) || mapping.length != 2 || !IsString(mapping[0]) || !IsString(mapping[1])) //unexpected input
             {
                Log("MapObject: Invalid mapping #" + i);
                continue;
            }
            var fromProp = mapping[0];
            var toProp = mapping[1];
            if (!(fromProp in obj) && ignoreMissingProperties == false) //ERROR!
                return null;
            //set the property
            if (cloneObjects)
                ret[toProp] = CloneObj(obj[fromProp]);
            else
                ret[toProp] = obj[fromProp];
        }
        return ret;
    }
    IronLibsCommon.MapObject = MapObject;
    /**
     * Return the STRING representing the passed ENUM value (for a TypeScript ENUM object)
     *
     * @param e The TypeScript ENUM object
     * @param val The integer value of the ENUM
     */
    function GetEnumKey(e, val) {
        for (var k in e) {
            if (e[k] == val)
                return k;
        }
        return "???";
    }
    IronLibsCommon.GetEnumKey = GetEnumKey;
    var FluentParallel = /** @class */ (function () {
        function FluentParallel(firstFunction) {
            this.functions = [];
            if (__.IsFunction(firstFunction))
                this.functions.push(firstFunction);
        }
        FluentParallel.prototype.Parallel = function (otherFunction) {
            if (__.IsFunction(otherFunction))
                this.functions.push(otherFunction);
            return this;
        };
        FluentParallel.prototype.ExecuteAndJoin = function (joinFunction) {
            if (__.IsFunction(joinFunction))
                this.functions.push(joinFunction);
            ExecuteAndJoin(this.functions);
        };
        return FluentParallel;
    }());
    IronLibsCommon.FluentParallel = FluentParallel;
    /**
     * Helper function offering FLUENT syntax for __.ExecuteAndJoin
     *
     * Calls can be joined:
     * __.Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .Parallel(function(onEnd){})
     *   .ExecuteAndJoin(function(resultsOrderedByPosition, resultsOrderedByTime, timeoutException){})
     *
     * @param {(onEnd: (result?: any) => void) => any} firstFunction Eventual first function to add to the list
     */
    function Parallel(firstFunction) {
        return new FluentParallel(firstFunction);
    }
    IronLibsCommon.Parallel = Parallel;
    /**
     * Executes ALL passed functions meant to be ASYNCHRONOUS passing to them as only argument a "END CALLBACK" to be called
     * with the result to ce collected.
     *
     * The LAST argument is the "JOIN" function: it will be called when EVERY passed function has called its own "END CALLBACK"
     * or has thrown an exception.
     * This "JOIN" function accepts 2 arrays with all results and an eventual Timeout error (if ALL functions did not call their own "END CALLBACK" in time (ExecuteAndJoin.TimeoutMs)):
     *   the FIRST ordered by the passed functions positions,
     *   the SECOND ordered by "END CALLBACK" call time.
     *
     * <b>WARNING</b>: the return values of the passed functions are IGNORED (the captured one is what is passed to the "END CALLBACK")<br>
     * <b>WARNING</b>: in the passed functions don't execute other code after "END CALLBACK" is called, just return.<br>
     * <b>WARNING</b>: if a passed function never calls its "END CALLBACK", then the "JOIN" function will never be called<br>
     * <b>WARNING</b>: multiple calls to an "END CALLBACK" are ignored (only the first one is considered)<br>
     *
     * @example
     *    ExecuteAndJoin(asyncFun1, asyncFun2, joinFun);
     *
     * @method ExecuteAndJoin
     */
    function ExecuteAndJoin() {
        var params = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            params[_i] = arguments[_i];
        }
        var functions;
        var me = this;
        //check if functions are passed as ARGUMENTS or in an ARRAY
        if (arguments.length == 1 && IsArray(arguments[0])) {
            functions = CloneObj(arguments[0]);
        }
        else {
            functions = CloneObj(arguments);
        }
        //check input parameters
        if (functions.length == 0)
            throw new Error("ExecuteAndJoin need at least ONE parameter");
        for (var i = 0; i < functions.length; i++) {
            if (!IsFunction(functions[i]))
                throw new Error("Argument " + i + " + of ExecuteAndJoin is not a function");
        }
        var resultsOrderedByPosition = [];
        var resultsOrderedByTime = [];
        var numReturned = 0;
        var allFuncsReturned = false;
        var joinCalledWithTimeoutException = false;
        var callJoin = function (timeoutException) {
            if (joinCalledWithTimeoutException)
                return; //all functions returned but AFTER the TIMEOUT expired. Discard this join call: the one with the exception has already been called.
            clearTimeout(timeoutCheckTimer);
            allFuncsReturned = true;
            setTimeout(function () {
                if (__.IsNull(timeoutException))
                    functions[functions.length - 1].call(me, resultsOrderedByPosition, resultsOrderedByTime);
                else {
                    joinCalledWithTimeoutException = true;
                    functions[functions.length - 1].call(me, resultsOrderedByPosition, resultsOrderedByTime, timeoutException);
                }
            }, 1);
        };
        if (functions.length == 1) {
            //nothing to execute.... join!
            callJoin();
            return;
        }
        //OK, RUN THE ASYNC FUNCTIONS
        //launch the timeout check
        var timeoutCheckTimer = setTimeout(function () {
            if (!allFuncsReturned) {
                callJoin(new Error("Timeout of " + ExecuteAndJoin.TimeoutMs + "ms expired!"));
            }
        }, ExecuteAndJoin.TimeoutMs);
        var _loop_1 = function (i) {
            (function () {
                var ii = i;
                setTimeout(function () {
                    try {
                        functions[ii].call(me, function (result) {
                            //function called END CALLBACK!
                            if (IsNotNullObject(resultsOrderedByPosition[ii]))
                                return; //END CALLBACK called TWICE, discard next calls...
                            //STORE its result
                            resultsOrderedByPosition[ii] = {
                                Position: ii,
                                SuccessfulResult: result,
                                Exception: undefined
                            };
                            resultsOrderedByTime[numReturned] = {
                                Position: ii,
                                SuccessfulResult: result,
                                Exception: undefined
                            };
                            //check if we have to JOIN
                            numReturned++;
                            if (numReturned == functions.length - 1)
                                callJoin();
                        });
                    }
                    catch (e) {
                        //function threw an exception
                        //STORE its result
                        resultsOrderedByPosition[ii] = {
                            Position: ii,
                            SuccessfulResult: undefined,
                            Exception: e
                        };
                        resultsOrderedByTime[numReturned] = {
                            Position: ii,
                            SuccessfulResult: undefined,
                            Exception: e
                        };
                        //check if we have to JOIN
                        numReturned++;
                        if (numReturned == functions.length - 1)
                            callJoin();
                    }
                }, 1);
            })();
        };
        //EXECUTE every function
        for (var i = 0; i < functions.length - 1; i++) {
            _loop_1(i);
        }
    }
    IronLibsCommon.ExecuteAndJoin = ExecuteAndJoin;
    (function (ExecuteAndJoin) {
        ExecuteAndJoin.TimeoutMs = 5000;
    })(ExecuteAndJoin = IronLibsCommon.ExecuteAndJoin || (IronLibsCommon.ExecuteAndJoin = {}));
    /**
     *   Returns a function, that, as long as it continues to be invoked, will not
     *   be triggered. So the function will be called ONLY ONCE, after it stops being called for
     *   N milliseconds.
     *   If `immediate` is passed, triggers the function on its FIRST invocation, instead on the timeout expiration.
     *
     * @param func The function to be called only ONCE
     * @param waitForMs The timeout milliseconds
     * @param immediate If TRUE the  `func` is called upon the first invocation
     */
    function Debounce(func, waitForMs, immediate) {
        if (immediate === void 0) { immediate = false; }
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate)
                    func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, waitForMs);
            if (callNow)
                func.apply(context, args);
        };
    }
    IronLibsCommon.Debounce = Debounce;
    /*##########################################################################
     ######################   GENERAL DATES RELATED METHODS   ##################
     ##########################################################################*/
    var Dates;
    (function (Dates) {
        /**
         * Checks that the passed argument is a string in the Microsoft format like "\/Date(1405728000000)\/"
         *
         * @method IsMsFormatStringDate
         * @param msFormatString
         * @return {boolean}
         */
        function IsMsFormatStringDate(msFormatString) {
            return IsString(msFormatString) &&
                msFormatString.toString().toLowerCase().substr(0, 6) == "/date(";
        }
        Dates.IsMsFormatStringDate = IsMsFormatStringDate;
        /**
         * Checks that the passed argument is a string in the ISO format like "2014-12-30T15:20:00Z"
         *
         * @method IsMsFormatStringDate
         * @param isoFormatString
         * @return {boolean}
         */
        function IsISOFormatStringDate(isoFormatString) {
            var exp = /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/;
            return IsString(isoFormatString) && exp.test(isoFormatString);
        }
        Dates.IsISOFormatStringDate = IsISOFormatStringDate;
        /**
         * Given an ISO string like "2014-12-30T15:20:00Z" returns the corresponding date
         * CONVERTED to the local time.
         * Has a fallback to manual parsing for older IEs (TODO: without considering the time shift for now...)
         *
         * @method GetDateFromISO
         * @param {string|*} isoString a string like "2014-12-30T15:20:00Z"
         * @return {Date|null} Returns the DATE object or NULL if the string is not valid
         */
        function GetDateFromISO(isoString) {
            if (IsDate(isoString))
                return isoString;
            if (IsNull(isoString) || IsEmptyString(isoString))
                return null;
            var isoDate = new Date(isoString);
            if (IsNotNull(isoDate) && isNaN(isoDate.getTime())) //NOT a valid ISO string
                return null;
            return isoDate;
        }
        Dates.GetDateFromISO = GetDateFromISO;
        /**
         * Given a Microsoft formatted string like "\/Date(1239018869048)\/"  returns the corresponding date
         *
         * @method GetDateFromMsFormat
         * @param {Date | string} msFormatString
         * @returns {Date}
         */
        function GetDateFromMsFormat(msFormatString) {
            if (IsDate(msFormatString))
                return msFormatString;
            if (IsNull(msFormatString) || IsEmptyString(msFormatString))
                return null;
            if (!IsMsFormatStringDate(msFormatString)) {
                throw new Error("The value passed to GetDateFromMsFormat is NOT a valid Microsoft format date!");
            }
            //WARNING: doesn't support shift like /Date(43434343+100)/
            var number = msFormatString.toLowerCase().replace("/date(", "").replace(")/", "");
            return new Date(parseInt(number, 10));
        }
        Dates.GetDateFromMsFormat = GetDateFromMsFormat;
        /**
         * Returns a Date object shifted of the current UTC timespan
         * (useful when need to print in the Local form a json UTC DATE received from the server)
         *
         * @method AddUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        function AddUtcOffsetToDate(date, editPassedDate) {
            if (IsNull(date))
                return null;
            if (IsNull(editPassedDate))
                editPassedDate = false;
            date = GetDateFromISO(date); //convert if necessary
            var toEdit;
            if (editPassedDate)
                toEdit = date;
            else
                toEdit = new Date(date.getTime()); //CLONE IT!
            toEdit.setMinutes(toEdit.getMinutes() + (date.getTimezoneOffset() * (-1)));
            return toEdit;
        }
        Dates.AddUtcOffsetToDate = AddUtcOffsetToDate;
        /**
         * Returns a Date object shifted BACK of the current UTC timespan
         * (useful when need to send the json date in UTC starting form a Locale DATE)
         *
         * @method SubtractUtcOffsetToDate
         * @param date The date to shift
         * @param [editPassedDate] {boolean} If TRUE the passed date object will be modified, otherwise not. DEFAULTS to FALSE
         * @returns {Date} The shifted date object
         */
        function SubtractUtcOffsetToDate(date, editPassedDate) {
            if (IsNull(date))
                return null;
            if (IsNull(editPassedDate))
                editPassedDate = false;
            date = GetDateFromISO(date); //convert if necessary
            var toEdit;
            if (editPassedDate)
                toEdit = date;
            else
                toEdit = new Date(date.getTime()); //CLONE IT!
            toEdit.setMinutes(toEdit.getMinutes() - (date.getTimezoneOffset() * (-1)));
            return toEdit;
        }
        Dates.SubtractUtcOffsetToDate = SubtractUtcOffsetToDate;
        /**
         * Useful to handle DATE-ONLY objects discarding the time (ex. birthdate!!!) without problems with server.
         *
         * @method DatePartOnly
         * @param {Date} date
         * @returns {Date} A new date without the TIME (00:00)
         */
        function DatePartOnly(date) {
            return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        }
        Dates.DatePartOnly = DatePartOnly;
        /**
         * Returns the difference of LocalTime with UTC in MINUTES
         * (applying the current daylight time saving)
         *
         * @method GetUtcOffset
         * @returns {number}
         */
        function GetUtcOffset() {
            return (new Date()).getTimezoneOffset();
        }
        Dates.GetUtcOffset = GetUtcOffset;
        function FormatDate(date, showYear, showMonth, showDay, showHour, showMinute, showSecond, showMillisecond) {
            if (showYear === void 0) { showYear = true; }
            if (showMonth === void 0) { showMonth = true; }
            if (showDay === void 0) { showDay = true; }
            if (showHour === void 0) { showHour = true; }
            if (showMinute === void 0) { showMinute = true; }
            if (showSecond === void 0) { showSecond = true; }
            if (showMillisecond === void 0) { showMillisecond = true; }
            var ret = "";
            if (showDay)
                ret += Format("%02d", date.getDate());
            if (showMonth)
                ret += (__.IsEmptyString(ret) ? "" : "/") + Format("%02d", date.getMonth() + 1);
            if (showYear)
                ret += (__.IsEmptyString(ret) ? "" : "/") + Format("%d", date.getFullYear());
            if (!__.IsEmptyString(ret) && (showHour || showMinute || showSecond || showMillisecond))
                ret += " ";
            if (showHour)
                ret += Format("%02d", date.getHours());
            if (showMinute)
                ret += (showHour ? ":" : "") + Format("%02d", date.getMinutes());
            if (showSecond)
                ret += (showHour || showMinute ? ":" : "") + Format("%02d", date.getSeconds());
            if (showMillisecond)
                ret += (showHour || showMinute || showSecond ? "." : "") + Format("%03d", date.getMilliseconds());
            return ret;
        }
        Dates.FormatDate = FormatDate;
    })(Dates = IronLibsCommon.Dates || (IronLibsCommon.Dates = {}));
})(IronLibsCommon = exports.IronLibsCommon || (exports.IronLibsCommon = {}));
