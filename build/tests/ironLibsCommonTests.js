"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var IronLibs = require("../libs/common");
var __ = IronLibs.IronLibsCommon;
describe('IronLibsCommon', function () {
    it('LimitNumberPrecision should limit correctly with NO_ROUNDING', function (done) {
        var num1 = 10000.1234567891234567890123456789;
        assert.strictEqual(__.LimitNumberPrecision(num1, 0, __.RoundingMethod.NO_ROUNDING), 10000);
        assert.strictEqual(__.LimitNumberPrecision(num1, 1, __.RoundingMethod.NO_ROUNDING), 10000.1);
        assert.strictEqual(__.LimitNumberPrecision(num1, 2, __.RoundingMethod.NO_ROUNDING), 10000.12);
        assert.strictEqual(__.LimitNumberPrecision(num1, 3, __.RoundingMethod.NO_ROUNDING), 10000.123);
        assert.strictEqual(__.LimitNumberPrecision(num1, 4, __.RoundingMethod.NO_ROUNDING), 10000.1234);
        assert.strictEqual(__.LimitNumberPrecision(num1, 5, __.RoundingMethod.NO_ROUNDING), 10000.12345);
        assert.strictEqual(__.LimitNumberPrecision(num1, 6, __.RoundingMethod.NO_ROUNDING), 10000.123456);
        var num2 = 2.05 * 100; //== 204.99999999999997
        assert.strictEqual(__.LimitNumberPrecision(num2, 0, __.RoundingMethod.NO_ROUNDING), 204);
        assert.strictEqual(__.LimitNumberPrecision(num2, 1, __.RoundingMethod.NO_ROUNDING), 204.9);
        assert.strictEqual(__.LimitNumberPrecision(num2, 2, __.RoundingMethod.NO_ROUNDING), 204.99);
        assert.strictEqual(__.LimitNumberPrecision(num2, 3, __.RoundingMethod.NO_ROUNDING), 204.999);
        done();
    });
    it('LimitNumberPrecision should limit correctly with ROUND', function (done) {
        var num1 = 10000.1234567891234567890123456789;
        assert.strictEqual(__.LimitNumberPrecision(num1, 0, __.RoundingMethod.ROUND), 10000);
        assert.strictEqual(__.LimitNumberPrecision(num1, 1, __.RoundingMethod.ROUND), 10000.1);
        assert.strictEqual(__.LimitNumberPrecision(num1, 2, __.RoundingMethod.ROUND), 10000.12);
        assert.strictEqual(__.LimitNumberPrecision(num1, 3, __.RoundingMethod.ROUND), 10000.123);
        assert.strictEqual(__.LimitNumberPrecision(num1, 4, __.RoundingMethod.ROUND), 10000.1235);
        assert.strictEqual(__.LimitNumberPrecision(num1, 5, __.RoundingMethod.ROUND), 10000.12346);
        assert.strictEqual(__.LimitNumberPrecision(num1, 6, __.RoundingMethod.ROUND), 10000.123457);
        var num2 = 2.05 * 100; //== 204.99999999999997
        assert.strictEqual(__.LimitNumberPrecision(num2, 0, __.RoundingMethod.ROUND), 205);
        assert.strictEqual(__.LimitNumberPrecision(num2, 1, __.RoundingMethod.ROUND), 205);
        assert.strictEqual(__.LimitNumberPrecision(num2, 2, __.RoundingMethod.ROUND), 205);
        assert.strictEqual(__.LimitNumberPrecision(num2, 3, __.RoundingMethod.ROUND), 205);
        done();
    });
    it('LimitNumberPrecision should limit correctly with FLOOR', function (done) {
        var num1 = 10000.1234567891234567890123456789;
        assert.strictEqual(__.LimitNumberPrecision(num1, 0, __.RoundingMethod.FLOOR), 10000);
        assert.strictEqual(__.LimitNumberPrecision(num1, 1, __.RoundingMethod.FLOOR), 10000.1);
        assert.strictEqual(__.LimitNumberPrecision(num1, 2, __.RoundingMethod.FLOOR), 10000.12);
        assert.strictEqual(__.LimitNumberPrecision(num1, 3, __.RoundingMethod.FLOOR), 10000.123);
        assert.strictEqual(__.LimitNumberPrecision(num1, 4, __.RoundingMethod.FLOOR), 10000.1234);
        assert.strictEqual(__.LimitNumberPrecision(num1, 5, __.RoundingMethod.FLOOR), 10000.12345);
        assert.strictEqual(__.LimitNumberPrecision(num1, 6, __.RoundingMethod.FLOOR), 10000.123456);
        var num = 2.05 * 100; //== 204.99999999999997
        assert.strictEqual(__.LimitNumberPrecision(num, 0, __.RoundingMethod.FLOOR), 204);
        assert.strictEqual(__.LimitNumberPrecision(num, 1, __.RoundingMethod.FLOOR), 204.9);
        assert.strictEqual(__.LimitNumberPrecision(num, 2, __.RoundingMethod.FLOOR), 204.99);
        assert.strictEqual(__.LimitNumberPrecision(num, 3, __.RoundingMethod.FLOOR), 204.999);
        done();
    });
    it('LimitNumberPrecision should limit correctly under JS precision limit of 12 digits after "."', function (done) {
        var num = 10000.1234567891234567890123456789;
        //this shows that JS precision is 12 digits after "."
        assert.strictEqual(__.LimitNumberPrecision(num, 23, __.RoundingMethod.NO_ROUNDING), 10000.12345678912345678901234);
        assert.strictEqual(__.LimitNumberPrecision(num, 23, __.RoundingMethod.NO_ROUNDING), 10000.123456789124777777777);
        assert.strictEqual(__.LimitNumberPrecision(num, 23, __.RoundingMethod.NO_ROUNDING), 10000.123456789124);
        assert.notStrictEqual(__.LimitNumberPrecision(num, 23, __.RoundingMethod.NO_ROUNDING), 10000.123456789123);
        done();
    });
});
