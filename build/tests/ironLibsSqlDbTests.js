"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var DB = require("../libs/server/sqlDb");
var Entities = require("./_sqlDbTestsEntities");
var IronLibs = require("../libs/common");
var __ = IronLibs.IronLibsCommon;
function BigUInt() {
    return (Math.random() * 1000000000000) | 0;
}
function GetSomeNewTestEntities(howMany) {
    if (howMany === void 0) { howMany = 3; }
    var data = [];
    var now = new Date();
    var oneDay = 1000 * 60 * 60 * 24;
    for (var i = 0; i < howMany; i++) {
        var newElement = new Entities.TestEntity();
        var bigRandom = Math.random() * 1000000000000;
        newElement.NumericPk = BigUInt();
        newElement.Name = "Test " + ((bigRandom % 1000) | 0);
        newElement.ElementsCount = ((bigRandom % 6000) | 0);
        newElement.PreciseVal = __.LimitNumberPrecision(i + Math.random(), 10); //This is VERY IMPORTANT for deep equal checks
        newElement.CreatedAt = new Date(now.getTime() + (i * oneDay));
        data.push(newElement);
    }
    return data;
}
function GetSomeNewTest2Entities(howMany) {
    if (howMany === void 0) { howMany = 3; }
    var data = [];
    for (var i = 0; i < howMany; i++) {
        var newElement = new Entities.Test2Entity();
        var bigRandom = Math.random() * 1000000000000;
        newElement.StringPk = __.Guid.New();
        newElement.Name = "Test2 " + ((bigRandom % 1000) | 0);
        data.push(newElement);
    }
    return data;
}
describe('IronLibsSqlDb Tests', function () {
    var dbTest = new DB.DB({
        fileName: "TestsDbAutoDump.json",
        verbose: false,
        inMemoryMode: true,
        inMemoryEntitiesToDump: [Entities.TestEntity, Entities.Test2Entity],
        inMemoryDumpOnlyIfDirty: true,
        inMemoryDumpEveryMs: 3000
    });
    var dbRestored = new DB.DB({
        fileName: "InMemoryRestoredDb",
        verbose: dbTest.GetIsVerbose(),
        inMemoryMode: true,
        inMemoryDumpEveryMs: -1,
        inMemoryDumpOnlyIfDirty: true
    });
    var repoTest;
    var restoredRepoTest;
    var repoTest2;
    var restoredRepoTest2;
    var sharedData = GetSomeNewTestEntities(10);
    /*################################################################################################################*/
    describe('INIT', function () {
        it('Create and init all repositories', function (done) {
            repoTest = DB.Repository.GetInstance(Entities.TestEntity, dbTest);
            repoTest2 = DB.Repository.GetInstance(Entities.Test2Entity, dbTest);
            restoredRepoTest = DB.Repository.GetInstance(Entities.TestEntity, dbRestored);
            restoredRepoTest2 = DB.Repository.GetInstance(Entities.Test2Entity, dbRestored);
            repoTest.Init(true, function (err3) {
                repoTest2.Init(true, function (err4) {
                    restoredRepoTest.Init(true, function (err5) {
                        restoredRepoTest2.Init(true, function (err6) {
                            //assert.strictEqual(err1, null);
                            //assert.strictEqual(err2, null);
                            assert.strictEqual(err3, null);
                            assert.strictEqual(err4, null);
                            assert.strictEqual(err5, null);
                            assert.strictEqual(err6, null);
                            done();
                        });
                    });
                });
            });
        });
    });
    describe('Repo.InsertBulk', function () {
        it('Should insert all records which MUST be new. Check correct case', function (done) {
            var data = GetSomeNewTestEntities(100);
            repoTest.InsertBulk(data, function (err) {
                assert.strictEqual(err, null);
                done();
            });
        });
        it('Should insert all records which MUST be new. Check exception', function (done) {
            repoTest.InsertBulk(sharedData, function (err) {
                assert.strictEqual(err, null);
                repoTest.InsertBulk(sharedData, function (err) {
                    assert.notStrictEqual(err, null);
                    done();
                });
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo.GetAll', function () {
        it('Should read all existing records. Check correct case', function (done) {
            repoTest.GetAll(function (data, err) {
                assert.strictEqual(err, null);
                assert.notStrictEqual(data, null);
                assert.strictEqual(data.length, 110);
                assert.deepStrictEqual(sharedData[0], __.SearchValInArray(data, function (x) { return x.NumericPk == sharedData[0].NumericPk; }));
                done();
            });
        });
        it('Should read all records with CreatedAt < TODAY + 5 DAYS', function (done) {
            var before = new Date(sharedData[0].CreatedAt.getTime() + 1000 * 60 * 60 * 24 * 5);
            var after = new Date(sharedData[0].CreatedAt.getTime() + 1000 * 60 * 60 * 24 * 2);
            repoTest.GetWhere("CreatedAt >= " + after.getTime() + " AND CreatedAt < " + before.getTime() + " order by CreatedAt", function (data, err) {
                assert.strictEqual(err, null);
                assert.notStrictEqual(data, null);
                assert.strictEqual(data.length, 6);
                assert.deepStrictEqual(sharedData[2], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[2].NumericPk); }));
                assert.deepStrictEqual(sharedData[3], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[3].NumericPk); }));
                assert.deepStrictEqual(sharedData[4], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[4].NumericPk); }));
                done();
            });
        });
        it('Should read all records with 5 <= PreciseVal < 10', function (done) {
            repoTest.GetWhere("PreciseVal >= 5 AND PreciseVal < 10", function (data, err) {
                assert.strictEqual(err, null);
                assert.notStrictEqual(data, null);
                assert.strictEqual(data.length, 10);
                assert.deepStrictEqual(sharedData[5], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[5].NumericPk); }));
                assert.deepStrictEqual(sharedData[6], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[6].NumericPk); }));
                assert.deepStrictEqual(sharedData[7], __.SearchValInArray(data, function (x) { return x.NumericPk == (sharedData[7].NumericPk); }));
                done();
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo.GetByPk', function () {
        it('Should get an existing record. Check existing case', function (done) {
            repoTest.GetByPk(sharedData[2].NumericPk, function (data, err) {
                assert.strictEqual(err, null);
                assert.deepStrictEqual(sharedData[2], data);
                done();
            });
        });
        it('Should get an existing record. Check non-existing case', function (done) {
            repoTest.GetByPk("non-existing record", function (data, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(data, null);
                done();
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo.Exists', function () {
        it('Should check if a record exists. Check existing case', function (done) {
            repoTest.Exists(sharedData[5].NumericPk, function (exists, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(exists, true);
                done();
            });
        });
        it('Should check if a record exists. Check non-existing case', function (done) {
            repoTest.Exists("not existing", function (exists, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(exists, false);
                done();
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo.Save', function () {
        it('Should create a new record. Check correct case', function (done) {
            sharedData.push(GetSomeNewTestEntities(1)[0]);
            repoTest.Save(sharedData[sharedData.length - 1], function (created, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(created, true);
                done();
            });
        });
        it('Should change the value of an existing record. Check correct case', function (done) {
            var toEdit = sharedData[1];
            toEdit.Name = "UPDATED NAME";
            toEdit.ElementsCount = 123;
            repoTest.Save(toEdit, function (created, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(created, false);
                repoTest.GetByPk(toEdit.NumericPk, function (data, err) {
                    assert.strictEqual(err, null);
                    assert.deepStrictEqual(toEdit, data);
                    done();
                });
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo.Delete', function () {
        it('Should delete an existing record. Check existing case', function (done) {
            repoTest.Delete(sharedData[5], function (err) {
                assert.strictEqual(err, null);
                repoTest.Exists(sharedData[5], function (exists, err) {
                    assert.strictEqual(err, null);
                    assert.strictEqual(exists, false);
                    done();
                });
            });
        });
        it('Should delete an existing record. Check non-existing case', function (done) {
            repoTest.Delete("not existing", function (err) {
                assert.strictEqual(err, null);
                done();
            });
        });
    });
    /*################################################################################################################*/
    describe('Repo managing COLLECTIONS', function () {
        it('Should get filled collections. Check existing case', function (done) {
            var parentEntity = new Entities.TestEntity();
            parentEntity.NumericPk = BigUInt();
            parentEntity.Name = "Pippo";
            parentEntity.ElementsCount = 333;
            parentEntity.CreatedAt = new Date();
            repoTest.Save(parentEntity, function (created, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(created, true);
                //now create 2 linked Projects and 1 not
                var collEntities = [];
                collEntities.push(new Entities.Test2Entity());
                collEntities.push(new Entities.Test2Entity());
                collEntities.push(new Entities.Test2Entity());
                collEntities[0].StringPk = __.Guid.New();
                collEntities[0].Name = "Project 1";
                collEntities[0].FK_TestColl1 = parentEntity.NumericPk;
                collEntities[1].StringPk = __.Guid.New();
                collEntities[1].Name = "Project 2";
                collEntities[1].FK_TestColl1 = BigUInt();
                collEntities[2].StringPk = __.Guid.New();
                collEntities[2].Name = "Project 3";
                collEntities[2].FK_TestColl1 = parentEntity.NumericPk;
                repoTest2.InsertBulk(collEntities, function (err) {
                    assert.strictEqual(err, null);
                    //OK, now retrieve the USER with its COLLECTION
                    repoTest.GetByPk(parentEntity.NumericPk, function (data, err) {
                        assert.strictEqual(err, null);
                        var userWithCollections = data;
                        assert.strictEqual(userWithCollections.Coll1.Values().length, 2);
                        assert.strictEqual(userWithCollections.Coll1.Values()[0].FK_TestColl1, parentEntity.NumericPk);
                        assert.strictEqual(userWithCollections.Coll1.Values()[1].FK_TestColl1, parentEntity.NumericPk);
                        done();
                    }, ["Coll1"]);
                });
            });
        });
        it('Should save a child collection', function (done) {
            var parentEntity = new Entities.TestEntity();
            parentEntity.NumericPk = BigUInt();
            parentEntity.Name = "Pippo";
            parentEntity.CreatedAt = new Date();
            var collEntities1 = GetSomeNewTest2Entities(3);
            var collEntities2 = GetSomeNewTest2Entities(4);
            parentEntity.Coll1.LoadWithValues(collEntities1);
            parentEntity.Coll2.LoadWithValues(collEntities2);
            repoTest.Save(parentEntity, function (created, err) {
                assert.strictEqual(err, null);
                repoTest.GetByPk(parentEntity.NumericPk, function (data, err) {
                    assert.strictEqual(err, null);
                    assert.strictEqual(data.Coll1.IsLoaded(), true);
                    var readSubitems1 = data.Coll1.Values();
                    var readSubitems2 = data.Coll2.Values();
                    assert.strictEqual(readSubitems1.length, 3);
                    assert.strictEqual(readSubitems2.length, 4);
                    //there must be this project
                    collEntities1.forEach(function (savedEntity) {
                        assert.strictEqual(__.SearchInArray(readSubitems1, function (el) {
                            return el.StringPk == savedEntity.StringPk && el.Name == savedEntity.Name;
                        }) >= 0, true);
                    });
                    collEntities2.forEach(function (savedEntity) {
                        assert.strictEqual(__.SearchInArray(readSubitems2, function (el) {
                            return el.StringPk == savedEntity.StringPk && el.Name == savedEntity.Name;
                        }) >= 0, true);
                    });
                    done();
                }, ["Coll1", "Coll2"]);
            }, ["Coll1", "Coll2"]);
        });
        it('Should save some items with some collections (checking some values)', function (done) {
            this.timeout(20000);
            var mainItems = GetSomeNewTestEntities(100);
            mainItems.forEach(function (item) {
                item.Coll1.LoadWithValues(GetSomeNewTest2Entities(10));
                item.Coll2.LoadWithValues(GetSomeNewTest2Entities(10));
            });
            //SAVE the main array of items
            repoTest.SaveAll(mainItems, function (created, err) {
                assert.strictEqual(err, null);
                assert.strictEqual(repoTest.GetIsDirty(), true);
                assert.strictEqual(repoTest2.GetIsDirty(), true);
                var ids = mainItems.map(function (el) { return el.NumericPk; });
                //now read all saved main items and for each check its collections
                repoTest.GetByPks(ids, function (data, err) {
                    assert.strictEqual(err, null);
                    data.forEach(function (data) {
                        var originalItem = __.SearchValInArray(mainItems, function (x) { return x.NumericPk == data.NumericPk; });
                        assert.notStrictEqual(originalItem, null);
                        assert.strictEqual(data.Coll1.IsLoaded(), true);
                        assert.strictEqual(data.Coll1.Values().length, originalItem.Coll1.Values().length);
                        assert.strictEqual(__.SearchInArray(data.Coll1.Values(), function (el) {
                            return el.StringPk == originalItem.Coll1.Values()[1].StringPk && el.Name == originalItem.Coll1.Values()[1].Name;
                        }) >= 0, true);
                        assert.strictEqual(data.Coll2.IsLoaded(), true);
                        assert.strictEqual(data.Coll2.Values().length, originalItem.Coll2.Values().length);
                        assert.strictEqual(__.SearchInArray(data.Coll2.Values(), function (el) {
                            return el.StringPk == originalItem.Coll2.Values()[1].StringPk && el.Name == originalItem.Coll2.Values()[1].Name;
                        }) >= 0, true);
                    });
                    assert.strictEqual(repoTest.GetIsDirty(), true);
                    assert.strictEqual(repoTest2.GetIsDirty(), true);
                    done();
                }, ["Coll1", "Coll2"]);
            }, ["Coll1", "Coll2"]);
        });
    });
    /*################################################################################################################*/
    describe('DB disk synchronization', function () {
        it('Should have automatically dumped', function (done) {
            this.timeout(4000);
            //Here dump should NOT already been performed and repos should be dirty
            assert.strictEqual(repoTest.GetIsDirty(), true);
            assert.strictEqual(repoTest2.GetIsDirty(), true);
            setTimeout(function () {
                //Here instead dump performed and clean repos
                assert.strictEqual(repoTest.GetIsDirty(), false);
                assert.strictEqual(repoTest2.GetIsDirty(), false);
                assert.notStrictEqual(dbTest.GetLastSuccessfulDump(), null);
                dbTest.PauseAutomaticDump(); //don't interfere anymore with future tests
                done();
            }, 3000);
        });
        it('Should force a dump', function (done) {
            var lastDump = dbTest.GetLastSuccessfulDump();
            setTimeout(function () {
                dbTest.ForceDump(null, null, function (err) {
                    assert.strictEqual(err, null);
                    var currDump = dbTest.GetLastSuccessfulDump();
                    assert.strictEqual(currDump.getTime() > lastDump.getTime(), true);
                    done();
                });
            }, 100);
        });
        it('Should load the saved dump (checking all restored values)', function (done) {
            this.timeout(20000);
            dbRestored.LoadDump("TestsDbAutoDump.json", [Entities.TestEntity, Entities.Test2Entity], DB.LOAD_DUMP_BEHAVIOUR.INSERTBULK, function (err) {
                assert.strictEqual(err, null);
                //check the loaded data: get the 2 sets with collections (reads both restored entities types)
                repoTest.GetAll(function (originalData, err) {
                    assert.strictEqual(err, null);
                    restoredRepoTest.GetAll(function (restoredData, err) {
                        assert.strictEqual(err, null);
                        //check the just length of arrays
                        assert.strictEqual(originalData.length, restoredData.length);
                        var totOriginalSubitems = 0;
                        var totRestoredSubitems = 0;
                        originalData.forEach(function (el) {
                            assert.strictEqual(el.Coll1.IsLoaded(), true);
                            assert.strictEqual(el.Coll2.IsLoaded(), true);
                            totOriginalSubitems += el.Coll1.Values().length + el.Coll2.Values().length;
                        });
                        restoredData.forEach(function (el) {
                            assert.strictEqual(el.Coll1.IsLoaded(), true);
                            assert.strictEqual(el.Coll2.IsLoaded(), true);
                            totRestoredSubitems += el.Coll1.Values().length + el.Coll2.Values().length;
                        });
                        assert.strictEqual(totOriginalSubitems, totRestoredSubitems);
                        done();
                    }, ["Coll1", "Coll2"]);
                }, ["Coll1", "Coll2"]);
            });
        });
        it('Should dump data merging an already existing dump', function (done) {
            var restoredRepoTest = DB.Repository.GetInstance(Entities.TestEntity, dbRestored);
            var restoredRepoTest2 = DB.Repository.GetInstance(Entities.Test2Entity, dbRestored);
            //check initial items count
            var originalCount = -1;
            var originalCount2 = -1;
            restoredRepoTest.GetCount(function (itemsCount, err) {
                assert.strictEqual(err, null);
                originalCount = itemsCount;
                restoredRepoTest2.GetCount(function (itemsCount, err) {
                    assert.strictEqual(err, null);
                    originalCount2 = itemsCount;
                    //now insert more data only in Test2
                    var moreData = GetSomeNewTest2Entities(10);
                    restoredRepoTest2.InsertBulk(moreData, function (err) {
                        assert.strictEqual(err, null);
                        //Now dump repo with new data which must be merged to existing file.
                        dbRestored.ForceDump("TestsDbAutoDump.json", [Entities.Test2Entity], function (err) {
                            assert.strictEqual(err, null);
                            //check that data has been merged
                            restoredRepoTest.RecreateTable(function (err) {
                                assert.strictEqual(err, null);
                                restoredRepoTest2.RecreateTable(function (err) {
                                    assert.strictEqual(err, null);
                                    dbRestored.LoadDump("TestsDbAutoDump.json", [Entities.TestEntity, Entities.Test2Entity], DB.LOAD_DUMP_BEHAVIOUR.INSERTBULK, function (err) {
                                        assert.strictEqual(err, null);
                                        //the merged dump has been loaded. Now check its items count
                                        var newCount = -1;
                                        var newCount2 = -1;
                                        restoredRepoTest.GetCount(function (itemsCount, err) {
                                            assert.strictEqual(err, null);
                                            newCount = itemsCount;
                                            restoredRepoTest2.GetCount(function (itemsCount, err) {
                                                assert.strictEqual(err, null);
                                                newCount2 = itemsCount;
                                                assert.strictEqual(originalCount, newCount);
                                                assert.strictEqual(originalCount2 + moreData.length, newCount2);
                                                done();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
    describe('CLEANUP', function () {
        it('CLOSE all databases', function (done) {
            dbTest.Close(function (err) {
                assert.strictEqual(err, null);
                dbRestored.Close(function (err) {
                    assert.strictEqual(err, null);
                    done();
                });
            });
        });
    });
});
