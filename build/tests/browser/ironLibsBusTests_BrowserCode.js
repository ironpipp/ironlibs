"use strict";
window["require"] = function () { return { "IronLibsCommon": window["IronLibsCommon"] }; }; //module import hack
var IronLibs = window["require"]('ironlibs');
var __ = IronLibs.IronLibsCommon; //shortcut
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.bus);
var BusExample1;
(function (BusExample1) {
    var TestEvents;
    (function (TestEvents) {
        TestEvents.UserPrefix = "CurrUser:%s:";
    })(TestEvents || (TestEvents = {}));
    var user = null;
    function CreateEvent(evtName, payload, firedBy) {
        if (payload === void 0) { payload = null; }
        if (firedBy === void 0) { firedBy = ""; }
        var name = "";
        if (__.IsNotEmptyString(user))
            name = __.Format(TestEvents.UserPrefix, user);
        name += evtName;
        return new __.Bus.BusEvent(name, payload, "BusExample1 page" + (__.IsNullOrEmpty(firedBy) ? "" : " - " + firedBy));
    }
    /*##########################################################################
    #########################   EXPORTS   #########################

    ##########################################################################*/
    function Test1_BrowserSubscribe(putAsyncResultIn) {
        var bus = __.Bus.CreateNew({ Name: "BrowserBus" });
        bus.RemoteConnectToBusServer("ws://" + location.hostname + ":" + (parseInt(location.port) - 1) + "/boh", function (conn, err) {
            if (__.IsNotNull(err)) {
                window["Test1_BrowserSubscribe_End"] = err.message;
                return;
            }
            bus.Subscribe({
                SubscribeTo: "**",
                SubscriberName: "Test1 - Browser Subscriber 1",
                Callback: function (context) {
                    return "Browser Power! Answer: " + (context.FiringEvent.Payload.Num + 1);
                }
            });
            bus.Subscribe({
                SubscribeTo: "**",
                SubscriberName: "Test1 - Browser Subscriber 2",
                Callback: function (context) {
                    return "Anch'io ci sono!";
                }
            });
            window[putAsyncResultIn] = true;
        });
        return true;
    }
    BusExample1.Test1_BrowserSubscribe = Test1_BrowserSubscribe;
})(BusExample1 || (BusExample1 = {}));
module.exports = BusExample1;
