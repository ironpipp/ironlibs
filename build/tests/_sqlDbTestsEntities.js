"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Test2Entity = exports.TestEntity = void 0;
var DB = require("../libs/server/sqlDb");
var TestEntity = /** @class */ (function (_super) {
    __extends(TestEntity, _super);
    function TestEntity() {
        var _this = _super.call(this) || this;
        _this.Coll1 = new DB.EntityCollection(Test2Entity, "FK_TestColl1");
        _this.Coll2 = new DB.EntityCollection(Test2Entity, "FK_TestColl2");
        return _this;
    }
    TestEntity.prototype.SqlTableName = function () { return "Test"; };
    TestEntity.prototype.SqlTableFields = function () {
        var baseFields = _super.prototype.SqlTableFields.call(this);
        return baseFields.concat([
            { name: "NumericPk", type: DB.DB_FIELD_TYPE.NUMBER_INTEGER, primary: true },
            { name: "Name", type: DB.DB_FIELD_TYPE.TEXT },
            { name: "ElementsCount", type: DB.DB_FIELD_TYPE.NUMBER_INTEGER },
            { name: "PreciseVal", type: DB.DB_FIELD_TYPE.NUMBER_DOUBLE },
            { name: "CreatedAt", type: DB.DB_FIELD_TYPE.DATE },
        ]);
    };
    return TestEntity;
}(DB.Entity));
exports.TestEntity = TestEntity;
var Test2Entity = /** @class */ (function (_super) {
    __extends(Test2Entity, _super);
    function Test2Entity() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Test2Entity.prototype.SqlTableName = function () { return "Test2"; };
    Test2Entity.prototype.SqlTableFields = function () {
        var baseFields = _super.prototype.SqlTableFields.call(this);
        return baseFields.concat([
            { name: "StringPk", type: DB.DB_FIELD_TYPE.TEXT, primary: true },
            { name: "Name", type: DB.DB_FIELD_TYPE.TEXT },
            { name: "FK_TestColl1", type: DB.DB_FIELD_TYPE.NUMBER_INTEGER },
            { name: "FK_TestColl2", type: DB.DB_FIELD_TYPE.NUMBER_INTEGER },
            { name: "CreatedAt", type: DB.DB_FIELD_TYPE.DATE },
        ]);
    };
    return Test2Entity;
}(DB.Entity));
exports.Test2Entity = Test2Entity;
