"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert = require("assert");
var puppeteer = require("puppeteer-core");
var path = require("path");
var IronLibs = require("../libs/common");
var IronLibsBus = require("../libs/bus");
var __ = IronLibs.IronLibsCommon;
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.bus);
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.nodeJs);
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.puppeteer);
//class meant to expose protected or private members
var PublicBus = /** @class */ (function (_super) {
    __extends(PublicBus, _super);
    function PublicBus(opt) {
        return _super.call(this, opt) || this;
    }
    PublicBus.prototype.PublicMatchesEventName = function (subscriptionString, eventName) {
        return this.MatchesEventName(subscriptionString, eventName);
    };
    return PublicBus;
}(IronLibsBus.Bus));
describe('Bus creation', function () {
    it('At start no bus is instantiated', function (done) {
        assert.strictEqual(__.Bus.GetDefault(), null);
        done();
    });
    it('CreateDefault() creates and returns the only "main" instance returned by subsequent calls to GetDefault()', function (done) {
        var created = __.Bus.CreateDefault();
        assert.notStrictEqual(created, null);
        var def = __.Bus.GetDefault();
        assert.strictEqual(def, created);
        done();
    });
    it('Bus default options', function (done) {
        var newBus = __.Bus.CreateNew({ LoggerError: __.LogFatal });
        var opt = newBus.GetOptions();
        assert.notStrictEqual(opt, null);
        assert.strictEqual(opt.Name, "Default Bus");
        assert.strictEqual(opt.LoggerError, __.LogFatal);
        done();
    });
});
describe('TESTS for MatchesEventName', function () {
    it("Some simple examples MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("asd", "asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd:qwe"), true);
        done();
    });
    it("Some simple examples DON'T MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("asd", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd:qwe:ert"), false);
        done();
    });
    it("Single wildcard MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("*", ""), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*", "qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd::qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:ert:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:*", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:*", "asd:asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:asd", "asd:wer:wer:asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd::qwe::ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe::ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx:ert"), true);
        done();
    });
    it("Single wildcard DON'T MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("*", "asd:asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:qwe:rty"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:qwe:rty"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx:ert:dfg"), false);
        done();
    });
    it("Double wildcard MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("**", ""), true);
        assert.strictEqual(newBus.PublicMatchesEventName("**", "qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("**", "qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:ert:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:xxx:zzz:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:ert:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe:ert:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:ert:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:ert:ert:ert:ert:ert:qwe:ert:ert:ert:ert:ert"), true);
        done();
    });
    it("Double wildcard DON'T MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "qwe:asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe:"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:asd::"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "qwe:qwe:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "qwe:asd::"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe:zzz"), false);
        done();
    });
    it("Mixed wildcard MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe:ert:wer"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**", "asd:qwe:ert:wer"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:yyy:zzz:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:yyy:zzz:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:yyy:zzz:ert"), true);
        done();
    });
    it("Mixed wildcard DON'T MATCH", function (done) {
        var newBus = new PublicBus();
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "qwe:xxx:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd:xxx:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd:wer:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd::wer:ert:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd::wer:xxx:ert:xxx"), false);
        done();
    });
});
describe('TESTS for GetSubscriptions and RemoveSubscription', function () {
    it('GetSubscriptions returns always copies of arrays', function (done) {
        var newBus = __.Bus.CreateNew();
        var s1 = newBus.GetSubscriptions();
        var s2 = newBus.GetSubscriptions();
        assert.strictEqual(s1 == s2, false);
        assert.strictEqual(s1 === s2, false);
        s1.push({ Callback: function () { }, SubscribeTo: "" });
        var s3 = newBus.GetSubscriptions();
        assert.notStrictEqual(s1.length, s3.length);
        assert.strictEqual(s2.length, s3.length);
        done();
    });
    it("RemoveSubscription should remove when both ISubscription parameters match", function (done) {
        var newBus = __.Bus.CreateNew();
        var clbk = function () { };
        newBus.Subscribe({ SubscribeTo: "asd", Callback: clbk });
        newBus.Subscribe({ SubscribeTo: "asd:*:qwe", Callback: clbk });
        newBus.Subscribe({ SubscribeTo: "asd:*:qwe:*:wer", Callback: clbk });
        newBus.RemoveSubscription({ SubscribeTo: "asd", Callback: clbk });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscription({ SubscribeTo: "asd:*:qwe", Callback: function () { } });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscription({ SubscribeTo: "asd:*:qwe", Callback: clbk });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);
        newBus.RemoveSubscription({ SubscribeTo: "asd:*:qwe:*:xxx", Callback: clbk });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);
        newBus.RemoveSubscription({ SubscribeTo: "asd:*:qwe:*:wer", Callback: clbk });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);
        done();
    });
    it("RemoveSubscriptionByXXX should remove when parameter match", function (done) {
        var newBus = __.Bus.CreateNew();
        var clbk = function () { };
        newBus.Subscribe({ SubscribeTo: "asd", Callback: clbk });
        newBus.Subscribe({ SubscribeTo: "asd:*:qwe", Callback: clbk });
        newBus.Subscribe({ SubscribeTo: "asd:*:qwe", Callback: clbk });
        newBus.Subscribe({ SubscribeTo: "asd:*:qwe:*:wer", Callback: clbk });
        newBus.RemoveSubscriptionBySubscribeTo("qwe");
        assert.strictEqual(newBus.GetSubscriptionsCount(), 4);
        newBus.RemoveSubscriptionBySubscribeTo("asd:*:qwe");
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscriptionByCallback(null);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscriptionByCallback(function () { });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscriptionByCallback(clbk);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);
        done();
    });
});
describe('TESTS for Subscribe', function () {
    it("Subscribe with bad parameter shouldn't add anything", function (done) {
        var newBus = __.Bus.CreateNew();
        newBus.Subscribe(null);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);
        newBus.Subscribe({ SubscribeTo: "asd", Callback: null });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);
        newBus.Subscribe({ SubscribeTo: null, Callback: function () { } });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);
        done();
    });
    it("Subscribe with good parameter should add the subscription", function (done) {
        var newBus = __.Bus.CreateNew();
        newBus.Subscribe({ SubscribeTo: "asd", Callback: function () { } });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);
        newBus.Subscribe({ SubscribeTo: "asd", Callback: function () { } });
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        done();
    });
});
describe('TESTS for Publish', function () {
    it('Publish with bad parameter should return NULL', function (done) {
        var newBus = __.Bus.CreateNew();
        assert.strictEqual(newBus.Publish(null), null);
        assert.strictEqual(newBus.Publish(3), null);
        assert.strictEqual(newBus.Publish("asd"), null);
        done();
    });
    it('Publish with random event Name should return 0 subscribers', function (done) {
        var newBus = __.Bus.CreateNew();
        var pubRet = newBus.Publish(new __.Bus.BusEvent(Math.random().toString()));
        assert.strictEqual(__.IsArray(pubRet), true);
        assert.strictEqual(pubRet.length, 0);
        done();
    });
    it('Publish with same subscribed event Name should have 1 subscriber', function (done) {
        var newBus = __.Bus.CreateNew();
        var evtName = "prova";
        newBus.Subscribe({ SubscribeTo: evtName, Callback: function () { } });
        assert.strictEqual(newBus.Publish(new __.Bus.BusEvent(evtName)).length, 1);
        done();
    });
    it('Publish should execute all passed synchronous subscribers', function (done) {
        var newBus = __.Bus.CreateNew();
        var evtName = "prova";
        var subscription1Executed = false;
        var subscription2Executed = false;
        newBus.Subscribe({ SubscribeTo: evtName, Callback: function () { subscription1Executed = true; } });
        newBus.Subscribe({ SubscribeTo: evtName, Callback: function () { subscription2Executed = true; } });
        newBus.Publish(new __.Bus.BusEvent(evtName), null, function () {
            assert.strictEqual(subscription1Executed, true);
            assert.strictEqual(subscription2Executed, true);
            done();
        });
    });
    it('Publish should execute all passed callbacks', function (done) {
        var newBus = __.Bus.CreateNew();
        var evtName = "prova";
        var subscription1ClbkCalled = false;
        var subscription2ClbkCalled = false;
        newBus.Subscribe({ SubscribeTo: evtName, Callback: function () { }, SubscriberName: "S1" });
        newBus.Subscribe({ SubscribeTo: evtName, Callback: function () { }, SubscriberName: "S2" });
        newBus.Publish(new __.Bus.BusEvent(evtName), function (firingEvent, endedSubscription, result) {
            if (firingEvent.Name != evtName)
                return;
            if (endedSubscription.SubscriberName == "S1")
                subscription1ClbkCalled = true;
            else if (endedSubscription.SubscriberName == "S2")
                subscription2ClbkCalled = true;
        }, function () {
            assert.strictEqual(subscription1ClbkCalled, true);
            assert.strictEqual(subscription2ClbkCalled, true);
            done();
        });
    });
    it('Publish should collect right and ordered results of synchronous subscriptions executions', function (done) {
        var newBus = __.Bus.CreateNew();
        var evtName = "prova";
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function () {
            }
        });
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function () {
                throw new Error("Custom error");
            }
        });
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function () {
                return "OK RESULT";
            }
        });
        newBus.Publish(new __.Bus.BusEvent(evtName), null, function (firingEvent, publishResults, timeoutException) {
            assert.strictEqual(firingEvent.Name, evtName);
            // assert.strictEqual(resultsOrderedByPosition.length, 3);
            // assert.strictEqual(resultsOrderedByTime.length, 3);
            // assert.strictEqual(timeoutException, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[0].Position, 0);
            // assert.strictEqual(resultsOrderedByPosition[0].Exception, undefined);
            // assert.strictEqual(resultsOrderedByPosition[0].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[1].Position, 1);
            // assert.strictEqual(resultsOrderedByPosition[1].Exception.message, "Custom error");
            // assert.strictEqual(resultsOrderedByPosition[1].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[2].Position, 2);
            // assert.strictEqual(resultsOrderedByPosition[2].Exception, undefined);
            // assert.strictEqual(resultsOrderedByPosition[2].SuccessfulResult, "OK RESULT");
            done();
        });
    });
    it('Publish should collect right and ordered results of asynchronous subscriptions executions', function (done) {
        var newBus = __.Bus.CreateNew();
        var evtName = "prova";
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function (context) {
                context.Async = true;
                context.AsyncEnded("S1");
            }
        });
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function (context) {
                context.Async = true;
                throw new Error("Custom error");
            }
        });
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function (context) {
                context.Async = true;
                setTimeout(function () {
                    context.AsyncEnded("S2");
                }, 200);
            }
        });
        newBus.Subscribe({
            SubscribeTo: evtName,
            Callback: function (context) {
                context.Async = true;
                setTimeout(function () {
                    context.AsyncEnded("S3");
                }, 100);
            }
        });
        newBus.Publish(new __.Bus.BusEvent(evtName), null, function (firingEvent, publishResults, timeoutException) {
            assert.strictEqual(firingEvent.Name, evtName);
            // assert.strictEqual(resultsOrderedByPosition.length, 4);
            // assert.strictEqual(resultsOrderedByTime.length, 4);
            // assert.strictEqual(timeoutException, undefined);
            //
            // assert.strictEqual(resultsOrderedByTime[0].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[0].SuccessfulResult, "S1");
            //
            // assert.strictEqual(resultsOrderedByTime[1].Exception.message, "Custom error");
            // assert.strictEqual(resultsOrderedByTime[1].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByTime[2].Position, 3);
            // assert.strictEqual(resultsOrderedByTime[2].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[2].SuccessfulResult, "S3");
            //
            // assert.strictEqual(resultsOrderedByTime[3].Position, 2);
            // assert.strictEqual(resultsOrderedByTime[3].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[3].SuccessfulResult, "S2");
            done();
        });
    });
});
describe('TESTS with BROWSER', function () {
    var _this = this;
    var bus = __.Bus.CreateNew({ Name: "ServerTestsBus" });
    this.timeout(10000);
    var browser;
    var page = null;
    function InitBrowserTest(testName) {
        return __awaiter(this, void 0, void 0, function () {
            var randVarName, p1, called;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        randVarName = "Result_" + ((Math.random() * 1000000) | 0);
                        p1 = new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                            var ret;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, page.evaluate(function (testName, randVarName) { return window["BusExample1"][testName](randVarName); }, testName, randVarName)];
                                    case 1:
                                        ret = _a.sent();
                                        resolve(ret);
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [4 /*yield*/, p1];
                    case 1:
                        called = _a.sent();
                        if (called == false)
                            return [2 /*return*/, new Promise(function (resolve) { resolve(false); })];
                        //then check its result in async
                        return [2 /*return*/, new Promise(function (resolve) {
                                var testCheckTimer = setInterval(function () { return __awaiter(_this, void 0, void 0, function () {
                                    var ended;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0: return [4 /*yield*/, page.evaluate(function (randVarName) { return window[randVarName]; }, randVarName)];
                                            case 1:
                                                ended = _a.sent();
                                                if (ended != null) {
                                                    clearInterval(testCheckTimer);
                                                    resolve(ended);
                                                }
                                                return [2 /*return*/];
                                        }
                                    });
                                }); }, 1);
                            })];
                }
            });
        });
    }
    it('Run TestServer', function () {
        __.Node.StartSimpleWebServer({
            port: 8990,
            staticPathsMapping: [
                {
                    pathStartingWith: "/js/",
                    serveFromDirectory: path.resolve(__dirname, "../..", "build")
                },
                {
                    pathStartingWith: "/lib/",
                    serveFromDirectory: path.resolve(__dirname, "../..", "node_modules")
                },
                {
                    pathStartingWith: "/",
                    serveFromDirectory: path.resolve(__dirname, "../..", "src", "tests", "browser")
                }
            ],
            onListening: function (error) {
                assert.strictEqual(error, false);
                bus.ListenForRemoteConnection(8989, function (socketId, err) {
                    assert.strictEqual(__.IsNotEmptyString(socketId), true);
                    assert.strictEqual(err, null);
                });
            }
        });
    });
    it('Open TestPage in Browser', function () { return __awaiter(_this, void 0, void 0, function () {
        var opt, h1, h1Text;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    opt = { headless: false };
                    if (__.Node.IsUnixEnvironment())
                        opt.executablePath = "/usr/bin/chromium-browser";
                    else
                        opt.executablePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
                    return [4 /*yield*/, puppeteer.launch(opt)];
                case 1:
                    browser = _a.sent();
                    return [4 /*yield*/, browser.newPage()];
                case 2:
                    page = _a.sent();
                    return [4 /*yield*/, page.goto('http://127.0.0.1:8990/ironLibsBusTests_BrowserPage.html')];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, page.waitFor('body > h1')];
                case 4:
                    h1 = _a.sent();
                    return [4 /*yield*/, __.Pupp.GetInnerText(page, h1)];
                case 5:
                    h1Text = _a.sent();
                    assert.strictEqual(h1Text, "!!! BusTest1 in Browser !!!");
                    return [2 /*return*/];
            }
        });
    }); });
    it('Test1: Browser should subscribe and send answer to a server published event', function () { return __awaiter(_this, void 0, void 0, function () {
        var initResult, actResult, publishResults, publishTimeout;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, InitBrowserTest("Test1_BrowserSubscribe")];
                case 1:
                    initResult = _a.sent();
                    assert.strictEqual(initResult, true);
                    return [4 /*yield*/, new Promise(function (resolve) {
                            //Test initialization ended. ACT
                            bus.Publish(new __.Bus.BusEvent("Test1:ServerSideEvent", { Num: 10 }, "ServerSide Test1"), null, function (firingEvent, publishResults, publishTimeout) {
                                resolve([publishResults, publishTimeout]);
                            });
                        })];
                case 2:
                    actResult = _a.sent();
                    publishResults = actResult[0];
                    publishTimeout = actResult[1];
                    assert.strictEqual(publishTimeout, undefined);
                    assert.strictEqual(publishResults.length, 2);
                    assert.strictEqual(publishResults[0].SuccessfulResult, "Browser Power! Answer: 11");
                    assert.strictEqual(publishResults[1].SuccessfulResult, "Anch'io ci sono!");
                    return [2 /*return*/];
            }
        });
    }); });
    it('Close Browser and CLEANUP', function () { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, page.close()];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, browser.close()];
                case 2:
                    _a.sent();
                    __.Node.StopAllSimpleWebServers();
                    bus.CloseAllConnections();
                    bus.StopListening();
                    return [2 /*return*/];
            }
        });
    }); });
});
