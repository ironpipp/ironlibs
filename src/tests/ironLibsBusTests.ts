import assert = require('assert');
import puppeteer = require('puppeteer-core');
import path = require('path');
import IronLibs = require('../libs/common');
import * as IronLibsBus from "../libs/bus"

let __ = IronLibs.IronLibsCommon;
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.bus);
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.nodeJs);
__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.puppeteer);


//class meant to expose protected or private members
class PublicBus extends IronLibsBus.Bus
{
    constructor(opt? : IronLibsBus.IBusOptions)
    {
        super(opt);
    }

    public PublicMatchesEventName(subscriptionString : string, eventName : string) : boolean
    {
        return this.MatchesEventName(subscriptionString, eventName);
    }
}


describe('Bus creation', function()
{
    it('At start no bus is instantiated', function(done)
    {
        assert.strictEqual(__.Bus.GetDefault(), null);

        done();
    });


    it('CreateDefault() creates and returns the only "main" instance returned by subsequent calls to GetDefault()', function(done)
    {
        let created = __.Bus.CreateDefault();
        assert.notStrictEqual(created, null);

        let def = __.Bus.GetDefault();
        assert.strictEqual(def, created);

        done();
    });


    it('Bus default options', function(done)
    {
        let newBus = __.Bus.CreateNew({LoggerError : __.LogFatal});
        let opt = newBus.GetOptions();

        assert.notStrictEqual(opt, null);
        assert.strictEqual(opt.Name, "Default Bus");
        assert.strictEqual(opt.LoggerError, __.LogFatal);

        done();
    });
});


describe('TESTS for MatchesEventName', function()
{
    it("Some simple examples MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("asd", "asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd:qwe"), true);

        done();
    });

    it("Some simple examples DON'T MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("asd", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:qwe", "asd:qwe:ert"), false);

        done();
    });


    it("Single wildcard MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("*", ""), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*", "qwe"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:qwe"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd::qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:ert:qwe"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("*:*", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:*", "asd:asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:asd", "asd:wer:wer:asd"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd::qwe::ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe::ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx:ert"), true);

        done();
    });


    it("Single wildcard DON'T MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("*", "asd:asd"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*", "asd:qwe:rty"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe", "asd:qwe:rty"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:*:ert", "asd:a:b:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:qwe:*:ert", "asd:xxx:qwe:xxx:ert:dfg"), false);

        done();
    });


    it("Double wildcard MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("**", ""), true);
        assert.strictEqual(newBus.PublicMatchesEventName("**", "qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("**", "qwe:ert"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "asd:qwe:ert"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:ert:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:xxx:zzz:qwe"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:ert:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:xxx:zzz:qwe:ert:"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:ert:qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:ert:ert:ert:ert:ert:qwe:ert:ert:ert:ert:ert"), true);

        done();
    });


    it("Double wildcard DON'T MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**", "qwe:asd"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe:"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe", "asd:qwe:ert"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "asd:asd::"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**", "qwe:qwe:ert"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "qwe:asd::"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:qwe:**:ert", "asd:qwe:zzz"), false);
        done();
    });


    it("Mixed wildcard MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("*:**", "qwe:ert:wer"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**", "asd:qwe:ert:wer"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:xxx:yyy:zzz:ert"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:ert", "asd:xxx:yyy:zzz:ert"), true);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:yyy:ert"), true);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*:**:ert", "asd:xxx:yyy:zzz:ert"), true);

        done();
    });


    it("Mixed wildcard DON'T MATCH", function(done)
    {
        let newBus = new PublicBus();

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**", "asd"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:*", "asd"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "asd:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:*:**:ert", "qwe:xxx:ert"), false);

        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd:xxx:ert"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd:wer:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd::wer:ert:xxx"), false);
        assert.strictEqual(newBus.PublicMatchesEventName("asd:**:wer:*:ert", "asd::wer:xxx:ert:xxx"), false);

        done();
    });
});

describe('TESTS for GetSubscriptions and RemoveSubscription', function()
{
    it('GetSubscriptions returns always copies of arrays', function(done)
    {
        let newBus = __.Bus.CreateNew();

        let s1 = newBus.GetSubscriptions();
        let s2 = newBus.GetSubscriptions();

        assert.strictEqual(s1 == s2, false);
        assert.strictEqual(s1 === s2, false);

        s1.push({Callback : () => {}, SubscribeTo : ""});
        let s3 = newBus.GetSubscriptions();

        assert.notStrictEqual(s1.length, s3.length);
        assert.strictEqual(s2.length, s3.length);

        done();
    });


    it("RemoveSubscription should remove when both ISubscription parameters match", function(done)
    {
        let newBus = __.Bus.CreateNew();
        let clbk = () => {};
        newBus.Subscribe({SubscribeTo : "asd", Callback : clbk});
        newBus.Subscribe({SubscribeTo : "asd:*:qwe", Callback : clbk});
        newBus.Subscribe({SubscribeTo : "asd:*:qwe:*:wer", Callback : clbk});

        newBus.RemoveSubscription({SubscribeTo : "asd", Callback : clbk});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);

        newBus.RemoveSubscription({SubscribeTo : "asd:*:qwe", Callback : () => {}});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscription({SubscribeTo : "asd:*:qwe", Callback : clbk});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);

        newBus.RemoveSubscription({SubscribeTo : "asd:*:qwe:*:xxx", Callback : clbk});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);
        newBus.RemoveSubscription({SubscribeTo : "asd:*:qwe:*:wer", Callback : clbk});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);

        done();
    });


    it("RemoveSubscriptionByXXX should remove when parameter match", function(done)
    {
        let newBus = __.Bus.CreateNew();
        let clbk = () => {};
        newBus.Subscribe({SubscribeTo : "asd", Callback : clbk});
        newBus.Subscribe({SubscribeTo : "asd:*:qwe", Callback : clbk});
        newBus.Subscribe({SubscribeTo : "asd:*:qwe", Callback : clbk});
        newBus.Subscribe({SubscribeTo : "asd:*:qwe:*:wer", Callback : clbk});

        newBus.RemoveSubscriptionBySubscribeTo("qwe");
        assert.strictEqual(newBus.GetSubscriptionsCount(), 4);
        newBus.RemoveSubscriptionBySubscribeTo("asd:*:qwe");
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);

        newBus.RemoveSubscriptionByCallback(null);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscriptionByCallback(() => {});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);
        newBus.RemoveSubscriptionByCallback(clbk);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);

        done();
    });
});


describe('TESTS for Subscribe', function()
{
    it("Subscribe with bad parameter shouldn't add anything", function(done)
    {
        let newBus = __.Bus.CreateNew();

        newBus.Subscribe(null);
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);

        newBus.Subscribe({SubscribeTo : "asd", Callback : null});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);

        newBus.Subscribe({SubscribeTo : null, Callback : () => {}});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 0);

        done();
    });

    it("Subscribe with good parameter should add the subscription", function(done)
    {
        let newBus = __.Bus.CreateNew();

        newBus.Subscribe({SubscribeTo : "asd", Callback : () => {}});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 1);

        newBus.Subscribe({SubscribeTo : "asd", Callback : () => {}});
        assert.strictEqual(newBus.GetSubscriptionsCount(), 2);

        done();
    });

});


describe('TESTS for Publish', function()
{
    it('Publish with bad parameter should return NULL', function(done)
    {
        let newBus = __.Bus.CreateNew();

        assert.strictEqual(newBus.Publish(null), null);
        assert.strictEqual(newBus.Publish(<any>3), null);
        assert.strictEqual(newBus.Publish(<any>"asd"), null);

        done();
    });


    it('Publish with random event Name should return 0 subscribers', function(done)
    {
        let newBus = __.Bus.CreateNew();

        let pubRet = newBus.Publish(new __.Bus.BusEvent(Math.random().toString()));

        assert.strictEqual(__.IsArray(pubRet), true);
        assert.strictEqual(pubRet.length, 0);

        done();
    });


    it('Publish with same subscribed event Name should have 1 subscriber', function(done)
    {
        let newBus = __.Bus.CreateNew();
        let evtName = "prova";
        newBus.Subscribe({SubscribeTo : evtName, Callback : () => {}});

        assert.strictEqual(newBus.Publish(new __.Bus.BusEvent(evtName)).length, 1);

        done();
    });


    it('Publish should execute all passed synchronous subscribers', function(done)
    {
        let newBus = __.Bus.CreateNew();
        let evtName = "prova";
        let subscription1Executed = false;
        let subscription2Executed = false;

        newBus.Subscribe({SubscribeTo : evtName, Callback : () => {subscription1Executed = true;}});
        newBus.Subscribe({SubscribeTo : evtName, Callback : () => {subscription2Executed = true;}});

        newBus.Publish(new __.Bus.BusEvent(evtName), null, () =>
        {
            assert.strictEqual(subscription1Executed, true);
            assert.strictEqual(subscription2Executed, true);

            done();
        });
    });


    it('Publish should execute all passed callbacks', function(done)
    {
        let newBus = __.Bus.CreateNew();
        let evtName = "prova";
        let subscription1ClbkCalled = false;
        let subscription2ClbkCalled = false;

        newBus.Subscribe({SubscribeTo : evtName, Callback : () => {}, SubscriberName : "S1"});
        newBus.Subscribe({SubscribeTo : evtName, Callback : () => {}, SubscriberName : "S2"});

        newBus.Publish(new __.Bus.BusEvent(evtName), (firingEvent : IronLibsBus.IBusEvent, endedSubscription : IronLibsBus.ISubscription, result : any) =>
        {
            if (firingEvent.Name != evtName)
                return;

            if (endedSubscription.SubscriberName == "S1")
                subscription1ClbkCalled = true;
            else if (endedSubscription.SubscriberName == "S2")
                subscription2ClbkCalled = true;
        }, () =>
        {
            assert.strictEqual(subscription1ClbkCalled, true);
            assert.strictEqual(subscription2ClbkCalled, true);

            done();
        });
    });


    it('Publish should collect right and ordered results of synchronous subscriptions executions', function(done)
    {
        let newBus = __.Bus.CreateNew();
        let evtName = "prova";

        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : () =>
            {
            }
        });
        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : () =>
            {
                throw new Error("Custom error");
            }
        });
        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : () =>
            {
                return "OK RESULT";
            }
        });

        newBus.Publish(new __.Bus.BusEvent(evtName), null, (firingEvent : IronLibsBus.IBusEvent, publishResults : IronLibsBus.IPublishResult[], timeoutException? : Error) =>
        {
            assert.strictEqual(firingEvent.Name, evtName);
            // assert.strictEqual(resultsOrderedByPosition.length, 3);
            // assert.strictEqual(resultsOrderedByTime.length, 3);
            // assert.strictEqual(timeoutException, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[0].Position, 0);
            // assert.strictEqual(resultsOrderedByPosition[0].Exception, undefined);
            // assert.strictEqual(resultsOrderedByPosition[0].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[1].Position, 1);
            // assert.strictEqual(resultsOrderedByPosition[1].Exception.message, "Custom error");
            // assert.strictEqual(resultsOrderedByPosition[1].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByPosition[2].Position, 2);
            // assert.strictEqual(resultsOrderedByPosition[2].Exception, undefined);
            // assert.strictEqual(resultsOrderedByPosition[2].SuccessfulResult, "OK RESULT");

            done();
        });
    });

    it('Publish should collect right and ordered results of asynchronous subscriptions executions', function(done)
    {
        let newBus = __.Bus.CreateNew();
        let evtName = "prova";

        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : (context) =>
            {
                context.Async = true;
                context.AsyncEnded("S1");
            }
        });
        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : (context) =>
            {
                context.Async = true;
                throw new Error("Custom error");
            }
        });
        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : (context) =>
            {
                context.Async = true;
                setTimeout(() =>
                {
                    context.AsyncEnded("S2");
                }, 200);
            }
        });
        newBus.Subscribe({
            SubscribeTo : evtName,
            Callback    : (context) =>
            {
                context.Async = true;
                setTimeout(() =>
                {
                    context.AsyncEnded("S3");
                }, 100);
            }
        });

        newBus.Publish(new __.Bus.BusEvent(evtName), null, (firingEvent : IronLibsBus.IBusEvent, publishResults : IronLibsBus.IPublishResult[], timeoutException? : Error) =>
        {
            assert.strictEqual(firingEvent.Name, evtName);
            // assert.strictEqual(resultsOrderedByPosition.length, 4);
            // assert.strictEqual(resultsOrderedByTime.length, 4);
            // assert.strictEqual(timeoutException, undefined);
            //
            // assert.strictEqual(resultsOrderedByTime[0].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[0].SuccessfulResult, "S1");
            //
            // assert.strictEqual(resultsOrderedByTime[1].Exception.message, "Custom error");
            // assert.strictEqual(resultsOrderedByTime[1].SuccessfulResult, undefined);
            //
            // assert.strictEqual(resultsOrderedByTime[2].Position, 3);
            // assert.strictEqual(resultsOrderedByTime[2].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[2].SuccessfulResult, "S3");
            //
            // assert.strictEqual(resultsOrderedByTime[3].Position, 2);
            // assert.strictEqual(resultsOrderedByTime[3].Exception, undefined);
            // assert.strictEqual(resultsOrderedByTime[3].SuccessfulResult, "S2");

            done();
        });
    });


});

describe('TESTS with BROWSER', function()
{
    let bus = __.Bus.CreateNew({Name : "ServerTestsBus"});

    this.timeout(10000);
    let browser : puppeteer.Browser;
    let page : puppeteer.Page = null;


    async function InitBrowserTest(testName : string) : Promise<boolean>
    {
        let randVarName = "Result_" + ((Math.random() * 1000000) | 0);

        //firstly execute the function
        let p1 = new Promise<boolean>(async (resolve) =>
        {
            let ret = await page.evaluate((testName, randVarName) => { return window["BusExample1"][testName](randVarName); }, testName, randVarName);
            resolve(ret);
        });
        let called = await p1;
        if (called == false)
            return new Promise((resolve) => {resolve(false);});

        //then check its result in async
        return new Promise<any>((resolve) =>
        {
            let testCheckTimer = setInterval(async () =>
            {
                let ended = await page.evaluate((randVarName) => { return window[randVarName];}, randVarName);

                if (ended != null)
                {
                    clearInterval(testCheckTimer);
                    resolve(ended);
                }
            }, 1);
        });
    }


    it('Run TestServer', () =>
    {
        __.Node.StartSimpleWebServer({
            port               : 8990,
            staticPathsMapping : [
                {
                    pathStartingWith   : "/js/",
                    serveFromDirectory : path.resolve(__dirname, "../..", "build")
                },
                {
                    pathStartingWith   : "/lib/",
                    serveFromDirectory : path.resolve(__dirname, "../..", "node_modules")
                },
                {
                    pathStartingWith   : "/",
                    serveFromDirectory : path.resolve(__dirname, "../..", "src", "tests", "browser")
                }
            ],
            onListening        : (error : boolean) =>
            {
                assert.strictEqual(error, false);

                bus.ListenForRemoteConnection(8989, (socketId : string, err : Error) =>
                {
                    assert.strictEqual(__.IsNotEmptyString(socketId), true);
                    assert.strictEqual(err, null);
                });
            }
        });
    });


    it('Open TestPage in Browser', async () =>
    {
        let opt : puppeteer.LaunchOptions = {headless : false};
        if (__.Node.IsUnixEnvironment())
            opt.executablePath = "/usr/bin/chromium-browser";
        else
            opt.executablePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";

        browser = await puppeteer.launch(opt);
        page = await browser.newPage();
        await page.goto('http://127.0.0.1:8990/ironLibsBusTests_BrowserPage.html');

        let h1 = await page.waitFor('body > h1');
        const h1Text = await __.Pupp.GetInnerText(page, h1);

        assert.strictEqual(h1Text, "!!! BusTest1 in Browser !!!");
    });


    it('Test1: Browser should subscribe and send answer to a server published event', async () =>
    {
        let initResult = await InitBrowserTest("Test1_BrowserSubscribe");
        assert.strictEqual(initResult, true);

        const actResult = await new Promise((resolve) =>
        {
            //Test initialization ended. ACT
            bus.Publish(new __.Bus.BusEvent(
                "Test1:ServerSideEvent",
                {Num : 10},
                "ServerSide Test1"),
                null,
                function(firingEvent : IronLibsBus.IBusEvent, publishResults : IronLibsBus.IPublishResult[], publishTimeout? : Error)
                {
                    resolve([publishResults, publishTimeout]);
                });
        });
        let publishResults = actResult[0];
        let publishTimeout = actResult[1];
        assert.strictEqual(publishTimeout, undefined);
        assert.strictEqual(publishResults.length, 2);
        assert.strictEqual(publishResults[0].SuccessfulResult, "Browser Power! Answer: 11");
        assert.strictEqual(publishResults[1].SuccessfulResult, "Anch'io ci sono!");
    });


    it('Close Browser and CLEANUP', async () =>
    {
        await page.close();
        await browser.close();

        __.Node.StopAllSimpleWebServers();

        bus.CloseAllConnections();
        bus.StopListening();
    });


});
