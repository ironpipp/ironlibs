(<any>window)["require"] = function() {return {"IronLibsCommon" : window["IronLibsCommon"]}; };    //module import hack
let IronLibs = (<any>window)["require"]('ironlibs');
let __ = IronLibs.IronLibsCommon;   //shortcut

import * as IronLibsBus from "../../libs/bus"

__.LoadPlugin(IronLibs.IronLibsCommon.IronLibsPlugin.bus);

module BusExample1
{
    module TestEvents
    {
        export const UserPrefix = "CurrUser:%s:";
    }

    let user : string = null;

    function CreateEvent(evtName : string, payload : any = null, firedBy : string = "") : IronLibsBus.IBusEvent
    {
        let name = "";
        if (__.IsNotEmptyString(user))
            name = __.Format(TestEvents.UserPrefix, user);
        name += evtName;

        return new __.Bus.BusEvent(name, payload, "BusExample1 page" + (__.IsNullOrEmpty(firedBy) ? "" : " - " + firedBy));
    }


    /*##########################################################################
    #########################   EXPORTS   #########################

    ##########################################################################*/

    export function Test1_BrowserSubscribe(putAsyncResultIn : string) : boolean
    {
        let bus = __.Bus.CreateNew({Name : "BrowserBus"});
        bus.RemoteConnectToBusServer("ws://" + location.hostname + ":" + (parseInt(location.port) - 1) + "/boh", (conn : IronLibsBus.IRemoteConnection, err : Error) =>
        {
            if (__.IsNotNull(err))
            {
                window["Test1_BrowserSubscribe_End"] = err.message;
                return;
            }

            bus.Subscribe({
                SubscribeTo    : "**",
                SubscriberName : "Test1 - Browser Subscriber 1",
                Callback       : (context : IronLibsBus.ISubscriptionExecutionContext) =>
                {
                    return "Browser Power! Answer: " + (context.FiringEvent.Payload.Num + 1);
                }
            });
            bus.Subscribe({
                SubscribeTo    : "**",
                SubscriberName : "Test1 - Browser Subscriber 2",
                Callback       : (context : IronLibsBus.ISubscriptionExecutionContext) =>
                {
                    return "Anch'io ci sono!";
                }
            });

            window[putAsyncResultIn] = true;
        });

        return true;
    }

}

export = BusExample1;