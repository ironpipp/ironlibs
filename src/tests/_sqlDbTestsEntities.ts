import DB = require("../libs/server/sqlDb");

export class TestEntity extends DB.Entity
{
    public NumericPk : number;
    public ElementsCount : number;
    public PreciseVal : number;
    public Name : string;
    public CreatedAt : Date;
    public Coll1 : DB.EntityCollection<Test2Entity>;
    public Coll2 : DB.EntityCollection<Test2Entity>;

    public constructor()
    {
        super();
        this.Coll1 = new DB.EntityCollection<Test2Entity>(Test2Entity, "FK_TestColl1");
        this.Coll2 = new DB.EntityCollection<Test2Entity>(Test2Entity, "FK_TestColl2");
    }

    public SqlTableName() : string { return "Test"; }

    public SqlTableFields() : DB.IDBFieldDescriptor[]
    {
        let baseFields : DB.IDBFieldDescriptor[] = super.SqlTableFields();
        return baseFields.concat([
            {name : "NumericPk", type : DB.DB_FIELD_TYPE.NUMBER_INTEGER, primary : true},
            {name : "Name", type : DB.DB_FIELD_TYPE.TEXT},
            {name : "ElementsCount", type : DB.DB_FIELD_TYPE.NUMBER_INTEGER},
            {name : "PreciseVal", type : DB.DB_FIELD_TYPE.NUMBER_DOUBLE},
            {name : "CreatedAt", type : DB.DB_FIELD_TYPE.DATE},
        ]);
    }
}


export class Test2Entity extends DB.Entity
{
    public StringPk : string;
    public Name : string;
    public FK_TestColl1 : number;
    public FK_TestColl2 : number;
    public CreatedAt : Date;

    public SqlTableName() : string { return "Test2"; }

    public SqlTableFields() : DB.IDBFieldDescriptor[]
    {
        let baseFields : DB.IDBFieldDescriptor[] = super.SqlTableFields();
        return baseFields.concat([
            {name : "StringPk", type : DB.DB_FIELD_TYPE.TEXT, primary : true},
            {name : "Name", type : DB.DB_FIELD_TYPE.TEXT},
            {name : "FK_TestColl1", type : DB.DB_FIELD_TYPE.NUMBER_INTEGER},
            {name : "FK_TestColl2", type : DB.DB_FIELD_TYPE.NUMBER_INTEGER},
            {name : "CreatedAt", type : DB.DB_FIELD_TYPE.DATE},
        ]);
    }
}

