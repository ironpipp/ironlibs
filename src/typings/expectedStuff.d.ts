
declare interface JQueryStatic
{
    unblockUI : (opt? : any)=> JQueryStatic,
    blockUI : (opt? : any)=> JQueryStatic,
    browser : any,
    cookie : any,
    notify(msg : any, type : any): JQuery;
}

declare interface JQuery
{
    block(options : any): JQuery;
    unblock(): JQuery;
    modal(options? : any): JQuery;
    ToHtml($el : JQuery) : string;
}

declare module App
{
    export var Res : Types.IClientResources;
}

declare function printStackTrace(opt : any) : string;
