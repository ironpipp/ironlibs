
declare module Types
{
    export interface IIdModel
    {
        Id : string;
    }

    export interface INameModel
    {
        Name : string;
    }

    export interface IIdNameModel extends IIdModel, INameModel {}

    export interface IPagedData<T>
    {
        CurrentPage : number;
        Items : T[];
        ItemsPerPage : number;
        TotalItems : number;
        TotalPages : number;
    }

    /**
     * These are the BARE minimum res used by IronLibss
     */
    interface IClientResources
    {
        Yes : string,
        No : string,
        Ok : string,
        Cancel : string,
        Loading : string,
        Error : string,
        ErrorDuringWork : string,
        OperationSuccessful : string,
        AnErrorForProperty : string,
        ConfirmationRequest : string,
        NoItemToDisplay : string
        AuthenticationProblem : string
    }

}
