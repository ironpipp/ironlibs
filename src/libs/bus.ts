
let window;
let isNode = window == null;

//import server-client SHARED modules
if (!isNode)
    (<any>window)["require"] = function() {return {"IronLibsCommon" : window["IronLibsCommon"]}; };    //module import hack
import IronLibs = require('./common');
let __ = IronLibs.IronLibsCommon;
__.LoadPlugin(__.IronLibsPlugin.nodeJs);

if (!isNode)
    (<any>window)["require"] = function() {return {}; /* no worries: uWs is never used in browser*/ };    //module import hack
import uWS = require('uWebSockets.js');

module IronLibsBus
{

    let mainBus : Bus = null;

    let ___privates : object = {};

    interface IBusPrivateStuff
    {
        Subscriptions : ISubscription[];
        RemoteConnections : IRemoteConnection[];
        ListeningSockets : { [id : string] : uWS.us_listen_socket };
        ConnectionsPermissions : { [id : string] : IConnectionPermissions };
    }

    enum BusRemoteMessageType
    {
        System        = 0,
        EventSent     = 1,
        AnswerToEvent = 2
    }

    // interface IBusRemoteAnswer
    // {
    //     ResultsOrderedByPosition : __.ExecuteAndJoin.Result[];
    //     ResultsOrderedByTime : __.ExecuteAndJoin.Result[]
    // }

    interface IBusRemoteMessage
    {
        readonly Type : BusRemoteMessageType;
        readonly Data : IBusEvent | IPublishResult[];
        readonly Id : string;
        readonly OnAnswer? : (response : IPublishResult[]) => void;
    }

    class BusRemoteMessage implements IBusRemoteMessage
    {
        readonly Id : string;

        constructor(public readonly Type : BusRemoteMessageType, public readonly Data : IBusEvent | IPublishResult[], public readonly OnAnswer? : (response : any) => void)
        {
            this.Id = __.Guid.NewShort();
        }
    }

    export interface IConnectionPermissions
    {
        Connection : IRemoteConnection;
        EventsCanReceiveFrom : string[];
        EventsCanForwardTo : string[];
    }

    export interface IRemoteConnection
    {
        GetUnansweredMessages() : IBusRemoteMessage[];

        GetWs() : uWS.WebSocket | WebSocket;

        GetPeerAddress() : string;

        GetPeerBusName() : string;

        SendMessage(msg : IBusRemoteMessage) : boolean;

        Close() : boolean;

        IsClosed() : boolean;

        GetId() : string;
    }


    //TODO: To be protected: split private class form public exposable stuff
    class ServerReceivedConnection implements IRemoteConnection
    {
        private readonly _opt : IBusOptions;
        private readonly _id : string;
        private readonly _peerIp : string;
        private readonly _unansweredMessages : IBusRemoteMessage[];
        private _closed = false;

        constructor(private readonly _ws : uWS.WebSocket, private readonly _peerBusName : string, private readonly _localBus : Bus)
        {
            this._id = __.Guid.NewShort();
            this._opt = _localBus.GetOptions();
            this._unansweredMessages = [];

            let ipBuff = this._ws.getRemoteAddress();
            this._peerIp = new Uint8Array(ipBuff.slice(ipBuff.byteLength - 4)).join('.');   //ip address

            this._opt.LoggerInfo('Accepted ServerReceivedConnection WebSocket connection from ' + this._peerIp, this._localBus);
        }

        public GetUnansweredMessages() : IBusRemoteMessage[]
        {
            return this._unansweredMessages;
        }

        public GetWs() : uWS.WebSocket | WebSocket
        {
            return this._ws;
        }

        public GetPeerAddress() : string
        {
            return this._peerIp;
        }

        public GetPeerBusName() : string
        {
            return this._peerBusName;
        }

        public SendMessage(msg : IBusRemoteMessage) : boolean
        {
            if (this._closed)
                return false;

            this._unansweredMessages.push(msg);
            this._ws.send(JSON.stringify(msg), false, true);

            return true;
        }

        public IsClosed() : boolean
        {
            return this._closed;
        }

        public Close() : boolean
        {
            if (this._closed)
                return false;
            this._closed = true;

            this._opt.LoggerInfo('Closed ServerReceivedConnection WebSocket with ' + this.GetPeerAddress(), this._localBus);

            try
            {
                this._ws.close();
            }
            catch (e)
            {
            }

            return true;
        }

        public GetId() : string
        {
            return this._id;
        }
    }


    //TODO: To be protected: split private class form public exposable stuff
    class ClientConnectionToServer implements IRemoteConnection
    {
        private readonly _opt : IBusOptions;
        private readonly _id : string;
        private readonly _peerUrl : string;
        private readonly _unansweredMessages : IBusRemoteMessage[];
        private _closed = false;

        constructor(private _ws : WebSocket, private readonly _peerBusName : string, private _localBus : Bus)
        {
            this._id = __.Guid.NewShort();
            this._opt = _localBus.GetOptions();
            this._unansweredMessages = [];

            this._peerUrl = _ws.url;

            this._opt.LoggerInfo('Created ClientConnectionToServer WebSocket to ' + this._peerUrl, this._localBus);
        }

        public GetUnansweredMessages() : IBusRemoteMessage[]
        {
            return this._unansweredMessages;
        }

        public GetWs() : uWS.WebSocket | WebSocket
        {
            return this._ws;
        }

        public GetPeerAddress() : string
        {
            return this._peerUrl;
        }

        public GetPeerBusName() : string
        {
            return this._peerBusName;
        }

        public SendMessage(msg : IBusRemoteMessage) : boolean
        {
            if (this._closed)
                return false;

            this._unansweredMessages.push(msg);
            this._ws.send(JSON.stringify(msg));

            return true;
        }

        public IsClosed() : boolean
        {
            return this._closed;
        }

        public Close() : boolean
        {
            if (this._closed)
                return false;
            this._closed = true;

            this._opt.LoggerInfo('Closed ClientConnectionToServer WebSocket with ' + this.GetPeerAddress(), this._localBus);

            try
            {
                this._ws.close();
            }
            catch (e)
            {
            }

            return true;
        }

        public GetId() : string
        {
            return this._id;
        }
    }

    /*##########################################################################
    #########################   EXPORTS   #########################
    ##########################################################################*/

    export class Bus
    {
        private readonly _opt : IBusOptions;

        constructor(opt? : IBusOptions)
        {
            let me = this;
            if (__.IsNull(opt))
                opt = {};
            this._opt = __.CloneObj(opt);

            //set DEFAULTS
            if (!__.IsString(this._opt.Name))
                this._opt.Name = "Default Bus";
            if (__.IsNullOrEmpty(this._opt.NamespacesSeparator) || this._opt.NamespacesSeparator.indexOf("*") >= 0)
                this._opt.NamespacesSeparator = ":";
            if (!__.IsFunction(this._opt.LoggerInfo))
                this._opt.LoggerInfo = () => {};
            if (!__.IsFunction(this._opt.LoggerError))
                this._opt.LoggerError = () => {};
            if (!__.IsFunction(this._opt.LoggerDebug))
                this._opt.LoggerDebug = () => {};
            if (!__.IsFunction(this._opt.OnRequestPermissionsForConnection))
                this._opt.OnRequestPermissionsForConnection = (fromBusName : string, conn : IRemoteConnection, setPermissions : (permissions : IConnectionPermissions) => void) =>
                {
                    //default rules (WARNING: they are the most permissive!)
                    setPermissions({
                        Connection           : conn,
                        EventsCanForwardTo   : ["**"],
                        EventsCanReceiveFrom : ["**"]
                    });
                };


            /*#############  implement methods accessing REAL private stuff  #############################################*/

            let myPrivateSymbol = Symbol();
            let myPrivateStuff : IBusPrivateStuff = {
                Subscriptions          : [],
                RemoteConnections      : [],
                ConnectionsPermissions : {},
                ListeningSockets       : {}
            };
            ___privates[myPrivateSymbol] = myPrivateStuff;

            this["GetSubscriptions"] = () =>
            {
                return __.CloneObj(myPrivateStuff.Subscriptions);
            };

            this["GetRemoteConnections"] = () =>
            {
                return myPrivateStuff.RemoteConnections;
            };

            this["Subscribe"] = (subscription : ISubscription) =>
            {
                if (!Bus.IsSubscription(subscription))
                {
                    this._opt.LoggerError("Subscribe must receive an ISubscription object", this);
                    return false;
                }

                this._opt.LoggerDebug("Subscription made to event pattern '" + subscription.SubscribeTo + "' by '" + __.EnsureString(subscription.SubscriberName) + "'", this);

                myPrivateStuff.Subscriptions.push(subscription);

                return true;
            };

            this["RemoveSubscription"] = (subscription : ISubscription) =>
            {
                if (!Bus.IsSubscription(subscription))
                {
                    this._opt.LoggerError("RemoveSubscription must receive an ISubscription object", this);
                    return myPrivateStuff.Subscriptions.length;
                }

                let oldLength = myPrivateStuff.Subscriptions.length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, (s) =>
                {
                    return s.SubscribeTo == subscription.SubscribeTo && s.Callback == subscription.Callback;
                });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };

            this["RemoveSubscriptionBySubscribeTo"] = function(subscribeTo : string)
            {
                let oldLength = me.GetSubscriptions().length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, (s) => { return s.SubscribeTo == subscribeTo; });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };

            this["RemoveSubscriptionByCallback"] = function(callback : () => void)
            {
                let oldLength = myPrivateStuff.Subscriptions.length;
                __.RemoveFromArrayByCondition(myPrivateStuff.Subscriptions, (s) => { return s.Callback == callback; });
                return oldLength - myPrivateStuff.Subscriptions.length;
            };

            function HandleReceivedWsMsg(ws : WebSocket | uWS.WebSocket, receivedData : ArrayBuffer | string) : void
            {
                //search for the corresponding connection
                let conns = me.GetRemoteConnections().filter((x) => {return x.IsClosed() == false && x.GetWs() == ws;});
                if (conns.length != 1)
                {
                    me._opt.LoggerError("Received a message from WebSocket, but can't associate WebSocket message to an active connection", me);
                    //TODO: answer with an ERROR otherwise peer would waitttt....
                    return;
                }

                let connection = conns[0];
                let message : IBusRemoteMessage = JSON.parse(__.IsString(receivedData) ? receivedData : __.Node.ArrayBufferToUtf8String(receivedData));

                if (message.Type == 0)       //System
                {
                }
                else if (message.Type == 1)  //EventSent
                {
                    me._opt.LoggerDebug("Received a message from WebSocket, detected to be an EventSent '" + (<IBusEvent>message.Data).Name +
                        "' fired by '" + __.EnsureString((<IBusEvent>message.Data).FiredBy) + "' from remote connection '" +
                        connection.GetPeerBusName() + " (" + connection.GetPeerAddress() + ")'.", me);

                    //check permissions
                    let connPermissions = myPrivateStuff.ConnectionsPermissions[connection.GetId()];
                    let permissionAllow = __.SearchInArray(connPermissions.EventsCanReceiveFrom, (namespace) =>
                    {
                        return me.MatchesEventName(namespace, (<IBusEvent>message.Data).Name);
                    });
                    if (permissionAllow < 0)
                    {
                        me._opt.LoggerDebug("No valid permission has been found to accept this message. Discard it.", me);
                        //TODO: answer with an ERROR otherwise peer would waitttt....
                        return;
                    }
                    me._opt.LoggerDebug("Permission has been found to accept this message. Publish it here and collect results...", me);

                    //Publish it here and collect results
                    me.Publish(<IBusEvent>message.Data, null, function(firingEvent : IBusEvent, publishResults : IPublishResult[], publishTimeout? : Error)
                    {
                        me._opt.LoggerDebug("...sending collected results via WebSocket for event '" + firingEvent.Name + "' to '" + __.EnsureString((<IBusEvent>message.Data).FiredBy) + "'.", me);

                        connection.SendMessage({
                            Type : 2,
                            Data : publishResults,
                            Id   : message.Id
                        });
                    }, null, [connection]);
                }
                else if (message.Type == 2)  //AnswerToEvent
                {
                    //search for the corresponding unanswered message
                    let msgPos = __.SearchInArray(connection.GetUnansweredMessages(), x => x.Id == message.Id);
                    if (msgPos < 0)
                    {
                        me._opt.LoggerError("Received a message from WebSocket, but can't associate the received IBusRemoteAnswer (Id = " + message.Id + ") to any unanswered message for this connection", me);
                        return;
                    }
                    let originalMessage = connection.GetUnansweredMessages()[msgPos];
                    __.RemoveFromArray(connection.GetUnansweredMessages(), originalMessage);

                    me._opt.LoggerDebug("Received a message from WebSocket, detected to be an AnswerToEvent '" + (<IBusEvent>originalMessage.Data).Name + "'. Pass it to the original publisher '" + __.EnsureString((<IBusEvent>originalMessage.Data).FiredBy) + "'.", me);

                    originalMessage.OnAnswer(<IPublishResult[]>message.Data);
                }
                else
                {
                    //TODO: answer with an ERROR otherwise peer would waitttt....
                    return;
                }
            }

            this["RemoteConnectToBusServer"] = function(url : string, onEnd : (conn : IRemoteConnection, err : Error) => void = null)
            {
                if (isNode)
                {
                    //TODO
                    //https://github.com/theturtle32/WebSocket-Node/blob/master/docs/WebSocketClient.md
                    // var WebSocketClient = require('websocket').client;
                    // var client = new WebSocketClient();
                    // var tunnel = require('tunnel');
                    //
                    // var tunnelingAgent = tunnel.httpOverHttp({
                    //     proxy: {
                    //         host: 'proxy.host.com',
                    //         port: 8080
                    //     }
                    // });
                    //
                    // var requestOptions = {
                    //     agent: tunnelingAgent
                    // };
                    //
                    // client.connect('ws://echo.websocket.org/', null, null, null, requestOptions);
                }
                else
                {
                    let browserSocket = new WebSocket(url);

                    browserSocket.onclose = (ev : CloseEvent) =>
                    {
                        let conn = __.SearchValInArray(myPrivateStuff.RemoteConnections, (rc : IRemoteConnection) => {return rc.GetWs() == browserSocket});
                        if (__.IsNotNull(conn))
                        {
                            conn.Close();   //need this to change its state
                            __.RemoveFromArray(myPrivateStuff.RemoteConnections, conn);
                        }
                    };
                    browserSocket.onerror = (ev : Event) =>
                    {
                        //hopefully this happens only during connection
                        if (__.IsFunction(onEnd))
                            onEnd(null, new Error("Readystate=" + browserSocket.readyState));
                    };

                    browserSocket.onmessage = (ev : MessageEvent) =>
                    {
                        HandleReceivedWsMsg(browserSocket, ev.data);
                    };

                    browserSocket.onopen = (ev : Event) =>
                    {
                        //TODO: implement HandShake to get remote peer features
                        let newConn = new ClientConnectionToServer(browserSocket, "TODO: unknown bus name", me);
                        myPrivateStuff.RemoteConnections.push(newConn);

                        me._opt.OnRequestPermissionsForConnection(newConn.GetPeerBusName(), newConn, (permissions : IConnectionPermissions) =>
                        {
                            let permissionToSave = __.CloneObj(permissions);

                            if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                                permissionToSave.EventsCanForwardTo = [];
                            if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                                permissionToSave.EventsCanReceiveFrom = [];

                            myPrivateStuff.ConnectionsPermissions[newConn.GetId()] = permissionToSave;
                        });

                        if (__.IsFunction(onEnd))
                            onEnd(newConn, null);
                    };

                }
            };

            this["StopListening"] = function(socketId? : string) : void
            {
                if (__.IsNotEmptyString(socketId))
                {
                    try
                    {
                        let sock = myPrivateStuff.ListeningSockets[socketId];
                        if (__.IsNotNull(sock))
                            uWS.us_listen_socket_close(sock);

                        delete myPrivateStuff.ListeningSockets[socketId];
                    }
                    catch (e)
                    {}
                }
                else
                {
                    for (let sockId in myPrivateStuff.ListeningSockets)
                    {
                        if (!myPrivateStuff.ListeningSockets.hasOwnProperty(sockId))
                            continue;
                        try
                        {
                            uWS.us_listen_socket_close(myPrivateStuff.ListeningSockets[sockId]);
                            delete myPrivateStuff.ListeningSockets[sockId];
                        }
                        catch (e)
                        {}
                    }
                }
            };

            this["UpdateConnectionsPermissions"] = function() : void
            {
                myPrivateStuff.RemoteConnections.forEach((conn : IRemoteConnection) =>
                {
                    me._opt.OnRequestPermissionsForConnection(conn.GetPeerBusName(), conn, (permissions : IConnectionPermissions) =>
                    {
                        let permissionToSave = __.CloneObj(permissions);

                        if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                            permissionToSave.EventsCanForwardTo = [];
                        if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                            permissionToSave.EventsCanReceiveFrom = [];

                        myPrivateStuff.ConnectionsPermissions[conn.GetId()] = permissionToSave;
                    });
                });
            };

            this["GetConnectionPermissions"] = function(conn : IRemoteConnection) : IConnectionPermissions
            {
                return __.CloneObj(myPrivateStuff.ConnectionsPermissions[conn.GetId()]);
            };

            this["GetAllConnectionsPermissions"] = function() : { [id : string] : IConnectionPermissions }
            {
                return __.CloneObj(myPrivateStuff.ConnectionsPermissions);
            };


            if (isNode)
            {
                this["ListenForRemoteConnection"] = function(port : number, onListening : (socketId : string, err : Error) => void = null, onConnection : (conn : IRemoteConnection) => void = null) : void
                {
                    if (!__.IsFunction(onListening))
                        onListening = () => {};
                    if (!__.IsFunction(onConnection))
                        onConnection = () => {};

                    uWS.App({

                        /* There are tons of SSL options
                        key_file_name: 'misc/key.pem',
                        cert_file_name: 'misc/cert.pem',
    */
                    }).ws('/*', {
                        compression : 1 /*uWS.CompressOptions.SHARED_COMPRESSOR*/,
                        message     : (ws : uWS.WebSocket, message : ArrayBuffer, isBinary : boolean) =>
                        {
                            HandleReceivedWsMsg(ws, message);
                        },

                        /** Handler for new WebSocket connection. WebSocket is valid from open to close, no errors. */
                        open : (ws : uWS.WebSocket, req : uWS.HttpRequest) =>
                        {
                            let newConn = new ServerReceivedConnection(ws, "TODO: unknown bus name", me);
                            myPrivateStuff.RemoteConnections.push(newConn);

                            me._opt.OnRequestPermissionsForConnection(newConn.GetPeerBusName(), newConn, (permissions : IConnectionPermissions) =>
                            {
                                let permissionToSave = __.CloneObj(permissions);

                                if (!__.IsArray(permissionToSave.EventsCanForwardTo))
                                    permissionToSave.EventsCanForwardTo = [];
                                if (!__.IsArray(permissionToSave.EventsCanReceiveFrom))
                                    permissionToSave.EventsCanReceiveFrom = [];

                                myPrivateStuff.ConnectionsPermissions[newConn.GetId()] = permissionToSave;
                            });

                            onConnection(newConn);
                        },

                        /** Handler for close event, no matter if error, timeout or graceful close. You may not use WebSocket after this event. */
                        close : (ws : uWS.WebSocket, code : number, message : ArrayBuffer) =>
                        {
                            let conn = __.SearchValInArray(myPrivateStuff.RemoteConnections, (rc : ServerReceivedConnection) => rc.GetWs() == ws);
                            if (__.IsNotNull(conn))
                            {
                                conn.Close();   //need this to change its state
                                __.RemoveFromArray(myPrivateStuff.RemoteConnections, conn);
                            }
                        }
                    }).listen(port, (listenSocket) =>
                    {
                        let err = listenSocket === false;
                        let socketId = __.Guid.NewShort();

                        myPrivateStuff.ListeningSockets[socketId] = listenSocket;

                        if (err)
                        {
                            me._opt.LoggerError('ERROR listening on port ' + port, me);
                            onListening(null, new Error('ERROR listening on port ' + port));
                        }
                        else
                        {
                            me._opt.LoggerInfo('Listening on port ' + port, me);
                            onListening(socketId, null);
                        }
                    });
                };
            }

            this._opt.LoggerDebug('Created bus instance "' + this._opt.Name + '".', this);
        };



        /**
         *
         * @param subscriptionString The "subscribeTo" string (with eventual wildcards)
         * Examples:
         *      "**"                    matches all
         *      "*"                     matches "MySingle"
         *      "App:MainOperation"     matches none
         *      "App:MainOperation:*"   matches "App:MainOperation:Minimize"
         *      "App:MainOperation:**"  matches "App:MainOperation:Minimize" and "App:MainOperation:Exit:Fatal"
         *      "App:*:Minimize"        matches "App:MainOperation:Minimize" and "App:Project:Minimize"
         *      "**:MasterLog"          matches "App:Project:Render:MasterLog"
         *
         * @param eventName The event name to match.
         * * Examples:
         *      "App:MainOperation:Minimize"
         *      "App:MainOperation:Exit:Fatal"
         *      "App:Project:Minimize"
         *      "App:Project:Render:MasterLog"
         *      "MySingle"
         */
        protected MatchesEventName(subscriptionString : string, eventName : string) : boolean
        {
            if (!__.IsString(subscriptionString) || !__.IsString(eventName))
                return false;

            let me = this;
            let subscriptionNamespaces = subscriptionString.split(me._opt.NamespacesSeparator);
            let eventNamespaces = eventName.split(me._opt.NamespacesSeparator);

            let iCurrEvt = 0;
            for (let iSub = 0; iSub < subscriptionNamespaces.length; iSub++)
            {
                let subToken = subscriptionNamespaces[iSub];
                let evtToken = eventNamespaces[iCurrEvt];

                if (subToken == "**")
                {
                    if (subscriptionNamespaces[iSub + 1] == "**")   //syntax error: can't
                    {
                        me._opt.LoggerError("subscriptionString syntax error: cant have two ** as siblings", this);
                        return false;
                    }
                    else if (iSub == subscriptionNamespaces.length - 1)  //last token: nothing more to do
                        return true;

                    //case "**:...."
                    //like "**:asd" or "**:asd:*:qwe" or "**:*:asd"
                    //try to match the remaining subscription string with all remaining evtName possibilities
                    let remainingSubscriptionString = subscriptionNamespaces.slice(iSub + 1, subscriptionNamespaces.length).join(me._opt.NamespacesSeparator);
                    for (let iRemainingEvt = iCurrEvt; iRemainingEvt < eventNamespaces.length; iRemainingEvt++)
                    {
                        let remainingEventTrial = eventNamespaces.slice(iRemainingEvt, eventNamespaces.length).join(me._opt.NamespacesSeparator);

                        if (me.MatchesEventName(remainingSubscriptionString, remainingEventTrial))
                            return true
                    }
                    return false;
                }
                else if (iCurrEvt >= eventNamespaces.length)
                {
                    //the current subToken requires the presence of at least one more event namespace which is not present
                    return false;
                }
                else if (subToken == "*")
                {
                    //go on with both indexes
                }
                else if (subToken != evtToken)
                {
                    return false;
                }
                //the 2 current tokens match: go on with both indexes
                iCurrEvt++;
            }

            //reached the end of subscriptionNamespaces
            if (iCurrEvt != eventNamespaces.length)
                return false;                           //event has other tokens
            else
                return true;                            //reached the end also of eventNamespaces
        }


        protected static IsSubscription(subscription : ISubscription) : boolean
        {
            return __.IsNotNull(subscription) && __.IsString(subscription.SubscribeTo) && __.IsFunction(subscription.Callback);
        }

        protected static IsEvent(evt : IBusEvent) : boolean
        {
            return __.IsNotNull(evt) && __.IsString(evt.Name);
        }

        /*##########################################################################
        #########################   PUBLIC STUFF   #########################
        ##########################################################################*/


        /**
         * Returns a copy of current options (not modifiable)
         */
        public GetOptions() : IBusOptions
        {
            return __.CloneObj(this._opt);
        }


        public GetMatchingSubscriptionsForEventName(evtName : string) : ISubscription[]
        {
            let me = this;
            return me.GetSubscriptions().filter((sub) => {return me.MatchesEventName(sub.SubscribeTo, evtName); });
        }


        public GetMatchingSubscriptionsCountForEventName(evtName : string) : number
        {
            return this.GetMatchingSubscriptionsForEventName(evtName).length;
        }


        public GetSubscriptionsCount() : number
        {
            return this.GetSubscriptions().length;
        }


        /**
         * Returns a COPY of registered subscriptions
         */
        public GetSubscriptions() : ISubscription[]
        { throw new Error("BAD method implementation"); }


        public GetRemoteConnections() : IRemoteConnection[]
        { throw new Error("BAD method implementation"); }


        public Subscribe(subscription : ISubscription) : boolean
        { throw new Error("BAD method implementation"); }


        /**
         * Returns the number of removed subscriptions
         */
        public RemoveSubscription(subscription : ISubscription) : number
        { throw new Error("BAD method implementation"); }


        /**
         * Returns the number of removed subscriptions
         */
        public RemoveSubscriptionBySubscribeTo(subscribeTo : string) : number
        { throw new Error("BAD method implementation"); }


        /**
         * @returns {number} Returns the number of removed subscriptions
         */
        public RemoveSubscriptionByCallback(callback : () => void) : number
        { throw new Error("BAD method implementation"); }


        /**
         *
         * @returns Returns LOCAL subscriptions matching the event name which will be fired.
         * Connection's belonging subscriptions are ignored from this (sync) return
         *
         * @param evt
         * @param onLocalSubscriptionEnded
         * @param onAllSubscribersEnded
         * WARNING: the number of results is:    # local subscriptions + [FOR EACH remote connections ] # remote subscriptions
         * considering only subscriptions matching the event name AND connections accepted by permissions
         *
         * @param forwardOnlyToTheseConnections
         * @param dontForwardToTheseConnections
         */
        public Publish(evt : IBusEvent,
                       onLocalSubscriptionEnded? : (firingEvent : IBusEvent, endedSubscription : ISubscription, result : any, asyncExecution : boolean) => void,
                       onAllSubscribersEnded ? : (firingEvent : IBusEvent, publishResults : IPublishResult[], publishTimeout? : Error /*calledSubscriptions : ISubscription[], resultsOrderedByPosition : __.ExecuteAndJoin.Result[], resultsOrderedByTime : __.ExecuteAndJoin.Result[], timeoutException? : Error*/) => void,
                       forwardOnlyToTheseConnections? : IRemoteConnection[],
                       dontForwardToTheseConnections? : IRemoteConnection[]) : ISubscription[]
        {
            if (!Bus.IsEvent(evt))
            {
                this._opt.LoggerError("PublishAndForget must receive an IBusEvent object", this);
                return null;
            }

            if (!__.IsFunction(onLocalSubscriptionEnded))
                onLocalSubscriptionEnded = () => {};
            if (!__.IsFunction(onAllSubscribersEnded))
                onAllSubscribersEnded = () => {};

            this._opt.LoggerDebug("Published event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "'", this);

            //execute local subscriptions
            let me = this;
            let localSubs = me.GetMatchingSubscriptionsForEventName(evt.Name);
            let parallel = __.Parallel();
            localSubs.forEach((sub) =>
            {
                parallel = parallel.Parallel(onEnd =>
                {
                    let context : ISubscriptionExecutionContext = {
                        FiringEvent : evt,
                        Async       : false,
                        AsyncEnded  : function(asyncResult : any)
                        {
                            if (!this.Async)
                                return;

                            //call eventually passed callback
                            onLocalSubscriptionEnded.call(me, evt, sub, asyncResult, true);
                            onEnd(asyncResult);
                        }
                    };

                    let syncResult = sub.Callback.call(me, context);

                    if (!context.Async)
                    {
                        //call eventually passed callback
                        onLocalSubscriptionEnded.call(me, evt, sub, syncResult, false);
                        onEnd(syncResult);
                    }
                });
            });

            //execute remote subscriptions (one parallel execution per accepted connection)

            let permissions = me.GetAllConnectionsPermissions();

            let connections = me.GetRemoteConnections();        //then filter them
            if (__.IsArray(forwardOnlyToTheseConnections))
                connections = forwardOnlyToTheseConnections;
            if (__.IsArray(dontForwardToTheseConnections))
                connections = connections.filter((conn : IRemoteConnection) => __.SearchValInArray(dontForwardToTheseConnections, (toExclude : IRemoteConnection) => toExclude.GetId() == conn.GetId()) == null);

            connections = connections.filter((conn : IRemoteConnection) =>
            {
                let connPermissions = permissions[conn.GetId()];

                if (__.IsNull(connPermissions))
                    return false;

                let permissionAllow = __.SearchInArray(connPermissions.EventsCanForwardTo, (namespace) =>
                {
                    return me.MatchesEventName(namespace, evt.Name);
                });

                if (permissionAllow < 0)     //no permission for this connection
                    return false;

                return true;
            });

            connections.forEach((conn : IRemoteConnection) =>
            {
                this._opt.LoggerDebug("Forwarding published event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "' to remote connection '" + conn.GetPeerBusName() + " (" + conn.GetPeerAddress() + ")'", this);

                parallel = parallel.Parallel(onEnd =>
                {
                    conn.SendMessage(new BusRemoteMessage(BusRemoteMessageType.EventSent, evt, (response) =>
                    {
                        onEnd(response);
                    }));
                });
            });


            parallel.ExecuteAndJoin((resultsOrderedByPosition : IronLibs.IronLibsCommon.ExecuteAndJoin.Result[], resultsOrderedByTime : IronLibs.IronLibsCommon.ExecuteAndJoin.Result[], timeoutException? : Error) =>
            {
                //TODO: handle forwardToRemoteConnections properly

                this._opt.LoggerDebug("Publish of event '" + evt.Name + "' by '" + __.EnsureString(evt.FiredBy) + "' collected results. Notify its end.", this);

                //collect results and eventually call passed callback
                let publishResults : IPublishResult[] = [];
                for (let i = 0; i < localSubs.length; i++)
                {
                    publishResults.push({
                        SubscriberName   : localSubs[i].SubscriberName,
                        SuccessfulResult : resultsOrderedByPosition[i].SuccessfulResult,
                    });
                }
                for (let i = localSubs.length; i < resultsOrderedByPosition.length; i++)
                {
                    let connectionAnswers : IPublishResult[] = resultsOrderedByPosition[i].SuccessfulResult;
                    //explode the answering subscribers from this connection
                    publishResults = publishResults.concat(connectionAnswers);
                }
                //warning publishResults.length can be > localSubs.length + connections.length
                onAllSubscribersEnded.call(me, evt, publishResults, timeoutException);
            });

            return localSubs;
        }


        public ListenForRemoteConnection(port : number, onListening : (socketId : string, err : Error) => void = null, onConnection : (conn : IRemoteConnection) => void = null) : void
        {
            throw new Error("Not implemented in browser");
        }


        /**
         * Makes ALL server sockets stop listening
         * If a socketId is passed (received in callback of ListenForRemoteConnection) only that one will be closed.
         */
        public StopListening(socketId? : string) : void
        { throw new Error("BAD method implementation"); }


        public CloseAllConnections() : void
        {
            this.GetRemoteConnections().forEach((conn) =>
            {
                conn.Close();
            });
        }


        public RemoteConnectToBusServer(url : string, onEnd : (conn : IRemoteConnection, err : Error) => void = null) : void
        { throw new Error("TODO: Not implemented"); }


        public GetAllConnectionsPermissions() : { [id : string] : IConnectionPermissions }
        { throw new Error("BAD method implementation"); }


        public GetConnectionPermissions(conn : IRemoteConnection) : IConnectionPermissions
        { throw new Error("BAD method implementation"); }


        public UpdateConnectionsPermissions() : void
        { throw new Error("BAD method implementation"); }
    }


    export interface IPublishResult
    {
        SubscriberName : string;

        FromConnection? : IRemoteConnection;

        SuccessfulResult : any;
    }


    export interface IBusEvent
    {
        /**
         * The namespaced event identification string to which subscribers subscribe to.
         * Ex:  "App:Main:Init"
         */
        readonly Name : string;

        /**
         * Eventual data associated to the event
         */
        readonly Payload : any;

        /**
         * Eventual identification string representing who is firing the event
         */
        readonly FiredBy : string;

        readonly FiredAt : Date;
    }


    export class BusEvent implements IBusEvent
    {
        constructor(public readonly Name : string,
                    public readonly Payload : any    = null,
                    public readonly FiredBy : string = "")
        {
            this.FiredAt = new Date();
        }

        readonly FiredAt : Date;
    }


    export interface ISubscriptionExecutionContext
    {
        FiringEvent : IBusEvent;
        Async : boolean;
        AsyncEnded : (result : any) => void;
    }


    export interface ISubscription
    {
        Callback : (context : ISubscriptionExecutionContext) => any;
        SubscribeTo : string;
        SubscriberName? : string;
    }



    export interface IBusOptions
    {
        Name? : string;

        LoggerInfo? : (msg : string, bus : Bus) => void;
        LoggerDebug? : (msg : string, bus : Bus) => void;
        LoggerError? : (msg : string, bus : Bus) => void;

        NamespacesSeparator? : string;

        /**
         * Called when a new connection is received. Informs the bus server about the events forwarding policies with the peer.
         * WARNING: if not specified the default permissions are the most permissive!
         *
         * @param fromBusName The belonging bus of the connection for which the permissions are requested
         * @param conn The connection for which the permissions are requested
         * @param setPermissions Function to be called to set permissions (async)
         */
        OnRequestPermissionsForConnection? : (fromBusName : string, conn : IRemoteConnection, setPermissions : (permissions : IConnectionPermissions) => void) => void;
    }


    export function GetDefault() : Bus
    {
        return mainBus;
    }


    export function CreateDefault(opt? : IBusOptions) : Bus
    {
        mainBus = new Bus(opt);
        return mainBus;
    }


    export function CreateNew(opt? : IBusOptions) : Bus
    {
        return new Bus(opt);
    }
}

export = IronLibsBus;
if (!isNode)
    IronLibs.IronLibsCommon.Bus = IronLibsBus;