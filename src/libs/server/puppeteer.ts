import puppeteer = require('puppeteer-core');

module IronLibsPuppeteer
{

    export async function GetInnerText(page : puppeteer.Page, el : puppeteer.ElementHandle) : Promise<string>
    {
        return await page.evaluate(elm => {return <string>(elm.textContent)}, el);
    }

    export async function GetInnerHtml(page : puppeteer.Page, el : puppeteer.ElementHandle) : Promise<string>
    {
        return await page.evaluate(elm => {return <string>(elm.innerHTML)}, el);
    }
}

export = IronLibsPuppeteer;