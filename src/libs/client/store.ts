
let window;
let isNode = window == null;

//import server-client SHARED modules
if (!isNode)
    (<any>window)["require"] = function() {return {"IronLibsCommon" : window["IronLibsCommon"]}; };    //module import hack
import IronLibs = require('../common');
let __ = IronLibs.IronLibsCommon;


/**
 * CommonLib library dealing with local/session storage
 * (runs in browser)
 */
module IronLibsStore
{

    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function get(store : Storage, key : string) : any
    {
        let val = store[key];
        if (val === undefined)
            return undefined;

        return JSON.parse(val);
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     * @param value an object that will be stored SERIALIZED
     */
    function set(store : Storage, key : string, value : any) : void
    {
        if (value === undefined)
            remove(store, key);
        else
            store[key] = JSON.stringify(value);
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function remove(store : Storage, key : string) : void
    {
        store.removeItem(key);
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     */
    function getAllKeys(store : Storage, prefix : string) : string[]
    {
        let ret = [];
        __.ObjKeysToArray(store).forEach(function(key)
        {
            if (key.indexOf(prefix) == 0)
                ret.push(key.substr(prefix.length))
        });
        return ret;
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     */
    function clear(store : Storage, prefix : string) : void
    {
        __.ObjKeysToArray(store).forEach(function(key)
        {
            if (key.indexOf(prefix) == 0)
                store.removeItem(key);
        });
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} prefix MUST be a NOT-NULL string
     * @param fn
     */
    function each(store : Storage, prefix : string, fn : (key : string, value : any) => void) : void
    {
        if (!__.IsFunction(fn))
            fn = function() {};

        __.ObjKeysToArray(store).forEach(function(key)
        {
            if (key.indexOf(prefix) == 0)
                fn(key.substr(prefix.length), store[key]);
        });
    }


    /**
     *
     * @param store the local or session storage native object
     * @param {string} key MUST be a NOT-NULL string
     */
    function isPresent(store : Storage, key : string) : any
    {
        return get(store, key) !== undefined;
    }



    export module Local
    {
        let prefix = "App";

        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        export function GetPrefix() : string
        {
            return prefix;
        }


        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        export function SetPrefix(Prefix : string) : void
        {
            prefix = __.EnsureString(Prefix);
        }


        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        export function Get(key : string) : any
        {
            return get(localStorage, prefix + __.EnsureString(key));
        }


        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        export function Set(key : string, value : any) : void
        {
            set(localStorage, prefix + __.EnsureString(key), value);
        }

        export function Remove(key : string) : any
        {
            remove(localStorage, prefix + __.EnsureString(key));
        }

        export function GetAllKeys() : string[]
        {
            return getAllKeys(localStorage, prefix);
        }

        export function Clear() : void
        {
            clear(localStorage, prefix);
        }

        export function Each(fn : (key : string, value : any) => void) : void
        {
            each(localStorage, prefix, fn);
        }

        export function IsPresent(key : string) : boolean
        {
            return isPresent(localStorage, prefix + __.EnsureString(key));
        }
    }



    export module Session
    {
        let prefix = "App";

        /**
         * Returns the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         */
        export function GetPrefix() : string
        {
            return prefix;
        }


        /**
         * Updates the current keys prefix.
         * (this is automatically prepended to the KEY parameter in ALL functions.
         * Is useful in order to group keys by a certain logic (EX: CACHE))
         * TRICK: if set to "" disables the grouping mechanism, forcing to operate on ALL storage keys.
         */
        export function SetPrefix(Prefix : string) : void
        {
            prefix = __.EnsureString(Prefix);
        }


        /**
         * Returns the value stored with the passed key.
         * NULL is a valid return value.
         * If not found UNDEFINED will be returned.
         */
        export function Get(key : string) : any
        {
            return get(sessionStorage, prefix + __.EnsureString(key));
        }


        /**
         * Stores the value associated to the passed key.
         * The NULL value will be stored as a valid value.
         * The UNDEFINED value will NOT be stored as a valid value and the item will be removed (if present).
         */
        export function Set(key : string, value : any) : void
        {
            set(sessionStorage, prefix + __.EnsureString(key), value);
        }

        export function Remove(key : string) : any
        {
            remove(sessionStorage, prefix + __.EnsureString(key));
        }

        export function GetAllKeys() : string[]
        {
            return getAllKeys(sessionStorage, prefix);
        }

        export function Clear() : void
        {
            clear(sessionStorage, prefix);
        }

        export function Each(fn : (key : string, value : any) => void) : void
        {
            each(sessionStorage, prefix, fn);
        }

        export function IsPresent(key : string) : boolean
        {
            return isPresent(sessionStorage, prefix + __.EnsureString(key));
        }
    }
}


export = IronLibsStore;
if (!isNode)
    IronLibs.IronLibsCommon.Store = IronLibsStore;



