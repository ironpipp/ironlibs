/**
 * Created by Ironpipp on 12/12/2015.
 */

let window;
let isNode = window == null;

//import server-client SHARED modules
if (!isNode)
    window["require"] = function() {return window["EventEmitter"]; };  //MODULES HACK
let EE3 = require('eventemitter3');

class EventObject extends EE3
{
}

//this object is used to attach all GLOBAL events
let globalEvents = new EventObject();

//these dictionaries are meant to be used to dynamically retrieve (at runtime) the available class names
//which can emit or react to events
let registeredListenersTypes : { [id : string] : EventsManager.IEventListener } = {};
let registeredNotifiersTypes : { [id : string] : EventsManager.IEventNotifier } = {};


module EventsManager
{

    export interface IEventParameterDescriptor
    {
        Name : string;
        Type? : PARAM_TYPE;//events.PARAM_TYPE;
        Required : boolean;
    }

    export interface IEventDescriptor
    {
        EventName : string;
        Parameters : IEventParameterDescriptor[];
    }

    export interface IEventListener
    {
        GetListenableEventsNames : () => IEventDescriptor[];
    }

    export interface IEventNotifier
    {
    }


    export interface IEventListenerAndNotifier extends IEventListener, IEventNotifier
    {
    }

    export enum PARAM_TYPE
    {
        STRING,
        NUMBER,
        DATE,
        BOOLEAN,
        OBJECT
    }

    export abstract class EventListener implements EventsManager.IEventListener
    {
        /**
         * A class which implements EventListener can REACT to some GLOBAL events
         *
         * @param listenerTypeName The name of the implementing class. It's meant to be used to dynamically
         * retrieve (at runtime) the available class names which can react to GLOBAL events.
         * WARNING: equal listenerTypeName should listen to the same ListenableEventsNames
         */
        constructor(protected listenerTypeName : string)
        {
            if (typeof listenerTypeName != "string")
                throw new Error("Need to pass a valid listenerTypeName");

            registeredListenersTypes[listenerTypeName] = this;          //adds the entry to the dictionary
        }

        protected ListenToEvent(eventName : string, clbk : (...params : any[]) => void) : void
        {
            globalEvents.on(eventName, clbk);
        }

        protected StopListeningToEvent(eventName : string, clbk : (...params : any[]) => void) : void
        {
            globalEvents.removeListener(eventName, clbk);
        }

        public abstract GetListenableEventsNames() : EventsManager.IEventDescriptor[];
    }



    export class EventNotifier implements EventsManager.IEventNotifier
    {
        /**
         * A class which implements EventNotifier can FIRE some GLOBAL events
         *
         * @param notifierTypeName The name of the implementing class. It's meant to be used to dynamically
         * retrieve (at runtime) the available class names which can fire GLOBAL events
         */
        constructor(protected notifierTypeName : string)
        {
            if (typeof notifierTypeName != "string")
                throw new Error("Need to pass a valid notifierTypeName");

            registeredNotifiersTypes[notifierTypeName] = this;          //adds the entry to the dictionary
        }


        protected NotifyEvent(eventName : string, ...params : any[]) : void
        {
            if (arguments.length == 1)
                globalEvents.emit(eventName);
            else if (arguments.length == 2)
                globalEvents.emit(eventName, arguments[1]);
            else if (arguments.length == 3)
                globalEvents.emit(eventName, arguments[1], arguments[2]);
            else if (arguments.length == 4)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3]);
            else if (arguments.length == 5)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4]);
            else if (arguments.length == 6)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            else if (arguments.length == 7)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
            else if (arguments.length == 8)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]);
            else if (arguments.length == 9)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8]);
        }
    }



    export abstract class EventListenerAndNotifier implements EventsManager.IEventListenerAndNotifier
    {
        constructor(listenerAndNotifierTypeName : string)
        {
            if (typeof listenerAndNotifierTypeName != "string")
                throw new Error("Need to pass a valid listenerAndNotifierTypeName");

            if (registeredNotifiersTypes[listenerAndNotifierTypeName] == null)
                registeredNotifiersTypes[listenerAndNotifierTypeName] = this;          //adds the entry to the dictionary
            if (registeredListenersTypes[listenerAndNotifierTypeName] == null)
                registeredListenersTypes[listenerAndNotifierTypeName] = this;          //adds the entry to the dictionary
        }

        protected ListenToEvent(eventName : string, clbk : (...params : any[]) => void) : void
        {
            globalEvents.on(eventName, clbk);
        }

        protected StopListeningToEvent(eventName : string, clbk : (...params : any[]) => void) : void
        {
            globalEvents.removeListener(eventName, clbk);
        }

        public abstract GetListenableEventsNames() : EventsManager.IEventDescriptor[];


        protected NotifyEvent(eventName : string, ...params : any[]) : void
        {
            if (arguments.length == 1)
                globalEvents.emit(eventName);
            else if (arguments.length == 2)
                globalEvents.emit(eventName, arguments[1]);
            else if (arguments.length == 3)
                globalEvents.emit(eventName, arguments[1], arguments[2]);
            else if (arguments.length == 4)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3]);
            else if (arguments.length == 5)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4]);
            else if (arguments.length == 6)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            else if (arguments.length == 7)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
            else if (arguments.length == 8)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7]);
            else if (arguments.length == 9)
                globalEvents.emit(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6], arguments[7], arguments[8]);
        }
    }

}


export = EventsManager;